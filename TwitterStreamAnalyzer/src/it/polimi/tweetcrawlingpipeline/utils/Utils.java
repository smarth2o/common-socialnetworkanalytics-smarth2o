package it.polimi.tweetcrawlingpipeline.utils;

import it.polimi.tweetcrawlingpipeline.dataset.StringList;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Array;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils {
	
	/**
	 * Reads the lines as double values
	 * @param file
	 * @return features
	 */
	public static double[] readNumericContent(File file) {
		List<String> lines = readFileLines(file);
		double[] features = new double[lines.size()];
		
		for (int i = 0; i < lines.size(); i++) {
			String line = lines.get(i);
			features[i] = Double.parseDouble(line);
		}
		
		return features;
	}
	
	/**
	 * Reads the lines of a text file and returns a list of lines
	 * @param file
	 * @return lines
	 */
	public static List<String> readFileLines(File file) {
		BufferedReader br = null;
		List<String> lines = new ArrayList<String>();
		 
		try {
			String sCurrentLine;
			br = new BufferedReader(new FileReader(file));
 
			while ((sCurrentLine = br.readLine()) != null)
				lines.add(sCurrentLine);
			
			return lines;
 
		} 
		catch (IOException e) {
			e.printStackTrace();
			return null;
		} 
		finally {
			try {
				if (br != null)
					br.close();
			} 
			catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}
	
	/**
	 * Find position of maximum element in the array
	 * @param array
	 * @return index
	 */
	public static int findMaxElementPosition(double[] array) {
		double largest = array[0];
		int index = 0;
		
		for (int i = 1; i < array.length; i++)
			if (array[i] > largest) {
				largest = array[i];
				index = i;
			}
		
		return index;
	}
	
	/**
	 * Remove the URLs from the text
	 * @param text
	 * @return text
	 */
	public static String removeURLs(String text) {
		text = removeSubstring(text, Config.URLpattern);	
		
		return text;
	}
	
	/**
	 * Removes hashtags from a text
	 * @param text
	 * @return
	 */
	public static String removeHashtags(String text) {
		text = removeSubstring(text, Config.hashtagPattern);	
		
		return text;
	}
	
	/**
	 * Remove retweets from a text
	 * @param text
	 * @return
	 */
	public static String removeRetweets(String text) {
		text = removeSubstring(text, Config.retweetPattern);	
		
		return text;
	}
	
	/**
	 * Removes stopwords from a text
	 * @param tweet
	 * @param stopwords
	 * @return
	 */
	public static List<String> removeStopwords(String tweet, List<String>stopwords){
		tweet = tweet.trim();
		String[] keywords = divideInWords(tweet);
		ArrayList<String> keywordList = new ArrayList<String>();
		
		for(int i = 0; i < keywords.length; i++)
			if (!stopwords.contains(keywords[i]))
				keywordList.add(keywords[i]);

		return keywordList;
	}

	
	/**
	 * Removes stopwords or bad words from a text usign a db collection
	 * @param words
	 * @param collectionName
	 * @return
	 */	


public static void removeEmptySpace(ArrayList<String>keywordSet){
	Collection<String>emptyCollection = new ArrayList<String>();
	emptyCollection.add("");
	keywordSet.removeAll(emptyCollection);
}


	
	/**
	 * Remove the user mentions from the text (in case of tweet)
	 * @param text
	 * @return text
	 */
	public static String removeUserMentions(String text) {
		return removeSubstring(text, Config.mentionPattern);
	}
	
	/**
	 * Remove the substrings that answer to the given pattern
	 * @param text
	 * @param patternString
	 * @return text
	 */
	private static String removeSubstring(String text, String patternString) {
		Pattern pattern = Pattern.compile(patternString,Pattern.CASE_INSENSITIVE);
		Matcher m = pattern.matcher(text);
		
	    if (m.find()) {
	        do {
	            String substring = m.group().replace(" ", "");
	            text = text.replace(substring,"");
	        } while(m.find());
	    }
		
		return text;
	}
	
	/**
	 * Divides a text in words
	 * @param text
	 * @return words
	 */
	private static String[] divideInWords(String text) {
		return text.replaceAll("[^a-zA-Z ]", "").toLowerCase().split("\\s+");
	}
	
	/**
	 * Find a string in a set of strings
	 * @param strings
	 * @param string
	 * @return index
	 */
	public static int findPosition(String[] strings, String string) {
		int index = -1;
		for (int i = 0; i < strings.length; i++)
			if (strings[i].equals(string)){
				index = i;
				break;
			}
		
		return index;
	}
	
	/**
	 * Gets the stopword file according to the language
	 * @param language
	 * @return stopwordsFile
	 */
	public static String getStopwordsListFile(String language) {
		switch(language) {
			case Config.english:
				return Config.stopwordsFolder + Config.englishStopwordsFile;
			default:
				return null;
		}
	}
	
	/**
	 * Returns the stemmer name given the specified language
	 * @param language
	 * @return stemmerName
	 */
	public static String getStemmerName(String language) {
		switch(language) {
			case Config.english:
				return Config.englishStemmer;
			case Config.italian:
				return Config.italianStemmer;
			default:
				return null;
		}
	}

	/**
	 * Populate the stopword list
	 * @return list
	 */
	public static List<String> populateStopwordsList(){
		String fileName = getStopwordsListFile(Config.english);
		StringList stopwords = new StringList();
		
		stopwords = TextFileReader.readWordListFromFile(fileName);
		return stopwords.getWords();
	}
	
	public static List<String> findHashtags(String text) {
		Pattern pattern = Pattern.compile(Config.hashtagPattern, Pattern.CASE_INSENSITIVE);
		Matcher m = pattern.matcher(text);
		
		List<String> result = new ArrayList<String>();
		
	    if (m.find()) {
	        do {
	            result.add(m.group().replace(" ", "").replace("\n", ""));
	        } while(m.find());
	    }
	    
	    return result;
	}
	
	/**
	 * Return the concatenation of two arrays
	 * @param A
	 * @param B
	 * @return concatenation
	 */
	public static <T> T[] concatenate (T[] A, T[] B) {
		int aLen = A.length;
		int bLen = B.length;

		@SuppressWarnings("unchecked")
		T[] C = (T[]) Array.newInstance(A.getClass().getComponentType(), aLen+bLen);
		System.arraycopy(A, 0, C, 0, aLen);
		System.arraycopy(B, 0, C, aLen, bLen);

		return C;
	}
	
	/**
	 * Gets the date from a Twitter format
	 * @param date
	 * @return
	 */
	public static Date getTwitterDate(String date){
		 final String TWITTER="EEE MMM dd HH:mm:ss ZZZZZ yyyy";
		 SimpleDateFormat sf = new SimpleDateFormat(TWITTER,Locale.ENGLISH);
		 sf.setLenient(true);
		 try {
			 return sf.parse(date);
		 } 
		 catch (ParseException e) {
			 e.printStackTrace();
			 return null;
		 }
	}
	
	public static ArrayList<String> transformIntoWords(String tweet){
		tweet=tweet.trim();
		
		String[] keywords=divideInWords(tweet);
		ArrayList<String> keywordList=new ArrayList<String>();
		for(int i=0;i<keywords.length;i++){
			
				keywordList.add(keywords[i]);
			
		}
		
		return keywordList;
	}
	
	public static long formatTime(long time) {
		Calendar date = Calendar.getInstance();
		date.setTimeInMillis(time);
		date.set(Calendar.MILLISECOND, 0);
		date.set(Calendar.SECOND,0);
		date.set(Calendar.MINUTE,0);
		date.set(Calendar.HOUR,0);
		time = date.getTimeInMillis();

		return time;
	}
	
	public static Date getPreviousDay(long date){
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(date);
		calendar.set(Calendar.HOUR, -22);
		Date oneDayAgo=calendar.getTime();
		return oneDayAgo;
	}
}
