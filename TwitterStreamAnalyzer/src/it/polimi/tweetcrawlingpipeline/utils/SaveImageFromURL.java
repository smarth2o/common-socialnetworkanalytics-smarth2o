package it.polimi.tweetcrawlingpipeline.utils;
import it.polimi.tweetcrawlingpipeline.utils.Config;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class SaveImageFromURL {
	
	/**
	 * Saves the specified image
	 * @param imageUrl
	 * @return
	 * @throws IOException
	 */
	public static String saveImage(String imageUrl) throws IOException {
		
		int lastSlash = imageUrl.lastIndexOf("/");
		int lastDot = imageUrl.lastIndexOf(".");
		String extension = imageUrl.substring(lastDot+1);
		
		if (extension.equals("jpg") || extension.equals("png") || extension.equals("jpeg")) {
			String imageNameString=imageUrl.substring(lastSlash+1, lastDot);

			String destinationFile=Config.imagePathFolder+imageNameString+"."+extension;
			URL url = new URL(imageUrl);
			
			HttpURLConnection httpConnection =  ( HttpURLConnection )  url.openConnection (); 
			httpConnection.setRequestMethod ("GET"); 
			httpConnection.connect(); 

			int code = httpConnection.getResponseCode();

			if ((code == HttpURLConnection.HTTP_OK)){
				InputStream is = httpConnection.getInputStream();
				OutputStream os = new FileOutputStream(destinationFile);

				byte[] b = new byte[2048];
				int length;

				while ((length = is.read(b)) != -1) {
					os.write(b, 0, length);
				}

				is.close();
				os.close();
				
				httpConnection.disconnect();
				return destinationFile;
			}
			else {
				httpConnection.disconnect();
				return null;
			}
			
		}
		else
			return null;
	}
	
	
	
}
