package it.polimi.tweetcrawlingpipeline.utils;

public class Config {
	public static final String language = "lang";
	public static final String english = "en";
	public static final String italian = "it";
	
	/************************** Database **************************/
	public static final String database = "classify_twitter";
	public static final String user = "twitterUser";
	public static final String password = "c3wMk1v";
	
	public static final String userCollection = "User";
	public static final String tweetCollection = "Tweet";
	public static final String urlCollection = "URL";
	public static final String mediaCollection = "Media";
	
	public static final String rawTweets = "rawTweets";
	
	public static final String collectionStatistics = "collectionStatistics";
	public static final String keywordsCollection = "keywordsCollection";
	public static final String hashtagsCollection = "hashtagsCollection";
	
	public static final String stopwordsCollection ="Stopwords";
	public static final String badWordsCollection ="BadWords";
	
	public static final String tweetCount5Min = "tweetCount5Min";
	public static final String tweetCount10Sec = "tweetCount10Sec";
	
	public static final String keywordsAndHashtagsIndex = "keywordsAndHashtagsIndex";
	
	public static final String additionalUserCollection="additionalUserCollection";
	public static final String pastUserScoreCollection="pastUserScoreCollection";
	
	public static final String negativeTweetsCollection = "negativeTweets";
	
	public static final String dynamicTweetLocations = "tweetLocations";
	public static final int	   dynamicLocationStoreStepInMin = 2;
	public static final String hystoricalTweetLocations = "hystoricalTweetLocations";
	public static final int    hystoricalLocationStoreStepInMin = 15;
	
	/************************** Image Path Folder **************************/
	public static final String imagePathFolder="Tweet_images/";
	
	/************************** Classifier **************************/
	public static final String URLpattern = "(@)?(href=')?(HREF=')?(HREF=\")?(href=\")?(http://)?(https://)?" +
			"[a-zA-Z_0-9\\-]+(\\.\\w[a-zA-Z_0-9\\-]+)+(/[#&\\n\\-=?\\+\\%/\\.\\w]+)?";
	public static final String mentionPattern 	= "^@\\w+:*|\\s@\\w+:*";
	public static final String hashtagPattern 	= "^#\\w+|\\s+#\\w+";
	public static final String retweetPattern 	= "^RT ";
	
	public static final String stopwordsFolder 		= "stopwords/";
	public static final String englishStopwordsFile = "en.txt";
	public static final String badWordsFile			= "BadWordsFile.txt";
	public static final String bannedWordsFile		= "BannedWordsFile.txt";
	public static final String taxonomyFile			= "taxonomy/cookingKeywords.txt";
	
	public static final String englishStemmer = "porter";
	public static final String italianStemmer = "italian";
	
	public static final String trainingSetRelation 	= "tweets";
	public static final String textClassSeparator	= ";";
	
	public static final String CSVKeyRelevance 	= "relevance";
	public static final String CSVKeyText 		= "tweet_content";
	
	public static final String annotatedInstancesFile 	= "docs/trainingSet.csv";
	public static final String textModelFile		= "model/text-model.mod";
	public static final String ARFFFile			= "./docs/trainingSet.arff";
	
	public static final String CSVTextImagefile	= "docs/annotatedTextImage.csv";
	
	public static final String dictionaryFileName	= "docs/dictionary.txt";
	public static final String IDFfileName			= "docs/IDF.txt";
	
	/************************** Image Classifier **************************/
	public static final String trainingImagesFolder	= "image-data/images/";
	public static final String imageVocabularyFile = "image-data/image-vocabulary.txt";
	public static final String imageModelFile		= "model/image-model.mod";
	
	public static final String trainingSetImageRelation = "images";
	public static final String annotatedImagesFile	= "image-data/image-annotations.csv";

	/************************** Keywords and Hashtags file **************************/
	public static final String keywordsFile="relevantFiles/relevantKeywords.txt";
	public static final String hashtagsFile="relevantFiles/relevantHashtags.txt";
	
	/************************** Crawler **************************/
	public static final String twitterDatabaseStream	= "twitterDatabaseStream";
	public static final String CSVcrawler				= "CSVcrawler";
		
	public static final String tweetText = "text";
	public static final String tweetUserObjectName = "user";
	public static final String tweetUserScreenName = "screen_name";
	
	/************************** Language profiles **************************/
	public static final String languageProfileDirectory = "lang-profiles/";
	
	public static final int topK=1000;
	public static final long Day=24*60*60*1000;
	public static final double activityConstant=1/1000000;

}
