package it.polimi.tweetcrawlingpipeline.crawler.crawlers;

import it.polimi.tweetcrawlingpipeline.pipeline.TweetCrawlingPipeline;
import it.polimi.tweetcrawlingpipeline.utils.Config;

public abstract class GenericCrawler {
	
	protected TweetCrawlingPipeline pipeline;
	
	public GenericCrawler(TweetCrawlingPipeline pipeline) {
		this.pipeline = pipeline;
	}
		
	public static GenericCrawler getFactory(String crawlerType, TweetCrawlingPipeline pipeline) {		
		switch (crawlerType) {
			case Config.twitterDatabaseStream:
				return new DatabaseCrawler(pipeline);
			case Config.CSVcrawler:
				return new CSVCrawler(pipeline);
			default:
				return null;
		}
	}
	
	/**
	 * Crawls the tweets
	 */
	public abstract void crawl();

}
