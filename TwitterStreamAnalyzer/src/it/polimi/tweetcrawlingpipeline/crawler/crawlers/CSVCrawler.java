package it.polimi.tweetcrawlingpipeline.crawler.crawlers;

import it.polimi.tweetcrawlingpipeline.classifier.utils.TextProcessor;
import it.polimi.tweetcrawlingpipeline.crawler.analyzers.TweetAnalyzer;
import it.polimi.tweetcrawlingpipeline.pipeline.TweetCrawlingPipeline;
import it.polimi.tweetcrawlingpipeline.utils.Config;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.json.JSONException;
import org.json.JSONObject;

public class CSVCrawler extends GenericCrawler {
	
	/**
	 * Constructor.
	 * @param tweetRepository
	 * @param classifier
	 * @param description
	 */
	public CSVCrawler(TweetCrawlingPipeline pipeline) {
		super(pipeline);
	}

	@Override
	public void crawl() {
		try {
			BufferedReader br = new BufferedReader(new FileReader(Config.CSVTextImagefile));
			String line = br.readLine();
			line = line.replace("\"", "");
			
			String[] separated = line.split(Config.textClassSeparator);
			
			TextProcessor textProcessor = new TextProcessor();
			
			while ((line = br.readLine()) != null) {
				separated = line.split(Config.textClassSeparator);
				
				if (separated.length < 2)
					System.err.println("ERRORE");
				
				String text = separated[0];
				String imageURL = separated[1];
				
				String content = "{\"text\":\"" + text + "\",\"entities\": {\"media\":[{\"media_url_https\": \"" + imageURL + "\"}]}}";
				JSONObject object = new JSONObject(content);
				
		    	TweetAnalyzer.getObject().analyzeTweet(object.toString(), textProcessor, pipeline.getTweetRepository(), 
						pipeline.getTextClassifier(), pipeline.getImageClassifier(),
						pipeline.getNonAppropriateContentFilter(), 
						pipeline.getBannedWordsFilter(), pipeline.getDescription(),
						pipeline.getBeliefAnalyzer(),0);
			}
			
			br.close();
		} 
		catch (FileNotFoundException e) {
			e.printStackTrace();
		} 
		catch (IOException e) {
			e.printStackTrace();
		} 
		catch (JSONException e) {
			e.printStackTrace();
		}
	}

}