package it.polimi.tweetcrawlingpipeline.crawler.crawlers;

import it.polimi.tweetcrawlingpipeline.classifier.utils.TextProcessor;
import it.polimi.tweetcrawlingpipeline.crawler.analyzers.TweetAnalyzer;
import it.polimi.tweetcrawlingpipeline.pipeline.TweetCrawlingPipeline;

import java.util.Calendar;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.json.JSONException;
import org.json.JSONObject;

public class DatabaseCrawler extends GenericCrawler {
	
	private static int 	NUM_THREADS = 4;
	private static int	WAITING_PERIOD = 5*60*1000;
	private static long START_COUNT_DATE;
	
	/**
	 * Constructor.
	 * @param tweetRepository
	 * @param classifier
	 * @param description
	 */
	public DatabaseCrawler(TweetCrawlingPipeline pipeline) {
		super(pipeline);
		
		Calendar calendar = Calendar.getInstance();
		calendar.set(2014, 9, 2, 10, 30, 00);
		START_COUNT_DATE = calendar.getTimeInMillis();
	}

	@Override
	public void crawl() {
		final ExecutorService executor = Executors.newFixedThreadPool(NUM_THREADS);
	    
		Runnable tweetAnalyzer = defineRunnable();
		while (true) {
		    for (int i = 0; i < NUM_THREADS; i++)
		    	executor.execute(tweetAnalyzer);
		    
		    try {
				Thread.sleep(WAITING_PERIOD);
			} 
		    catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Defines the tweet analysis procedure followed by a single thread
	 * @return runnable
	 */
	private Runnable defineRunnable() {
		return new Runnable() {

			@Override
			public void run() {
				TextProcessor textProcessor = new TextProcessor();
				
			    while (true) {
			    	JSONObject json = null;
			    	
			    	synchronized(this) {
			    		json = pipeline.getTweetRepository().getRawTweetFromDB();
			    		
			    		if (json == null)
			    			break;
			    		
						try {
							pipeline.getTweetRepository().delete(json.getString("id_str"));
						}
						catch (JSONException e) {
							e.printStackTrace();
						}
			    	}

			    	TweetAnalyzer.getObject().analyzeTweet(json.toString(), textProcessor, pipeline.getTweetRepository(), 
			    								pipeline.getTextClassifier(), pipeline.getImageClassifier(),
			    								pipeline.getNonAppropriateContentFilter(), 
			    								pipeline.getBannedWordsFilter(), pipeline.getDescription(),
			    								pipeline.getBeliefAnalyzer(), START_COUNT_DATE);
			    }
			    
			    System.out.println("Exiting... (ID: " + Thread.currentThread().getId() + ")");
			}
	    	
	    };
	}

}