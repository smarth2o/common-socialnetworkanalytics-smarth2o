package it.polimi.tweetcrawlingpipeline.crawler.analyzers;

import it.polimi.tweetcrawlingpipeline.classifier.objects.DatasetDescription;
import it.polimi.tweetcrawlingpipeline.classifier.objects.samples.SVMSample;
import it.polimi.tweetcrawlingpipeline.classifiers.analyzers.BeliefAnalyzer;

public abstract class ClassifierOpinionAggregator {
	
	public static ClassifierOpinionAggregator opinionAggregator;
	
	public abstract int aggregateLabel(int textLabel, int imageLabel, BeliefAnalyzer<SVMSample> beliefAnalyzer, DatasetDescription description);	
}