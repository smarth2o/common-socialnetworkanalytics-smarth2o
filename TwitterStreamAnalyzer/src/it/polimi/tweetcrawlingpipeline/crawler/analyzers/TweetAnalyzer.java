package it.polimi.tweetcrawlingpipeline.crawler.analyzers;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import it.polimi.tweetcrawlingpipeline.classifier.classificationresults.ClassificationResult;
import it.polimi.tweetcrawlingpipeline.classifier.classificationresults.ImageClassificationResult;
import it.polimi.tweetcrawlingpipeline.classifier.classificationresults.TextClassificationResult;
import it.polimi.tweetcrawlingpipeline.classifier.classifiers.GenericClassifier;
import it.polimi.tweetcrawlingpipeline.classifier.objects.DatasetDescription;
import it.polimi.tweetcrawlingpipeline.classifier.objects.samples.ImageSample;
import it.polimi.tweetcrawlingpipeline.classifier.objects.samples.SVMSample;
import it.polimi.tweetcrawlingpipeline.classifier.objects.samples.TextSample;
import it.polimi.tweetcrawlingpipeline.classifier.utils.TextProcessor;
import it.polimi.tweetcrawlingpipeline.classifiers.analyzers.BeliefAnalyzer;
import it.polimi.tweetcrawlingpipeline.filters.Filter;
import it.polimi.tweetcrawlingpipeline.mongodb.TweetRepository;
import it.polimi.tweetcrawlingpipeline.mongodb.objects.TweetChunk;
import it.polimi.tweetcrawlingpipeline.utils.Config;
import it.polimi.tweetcrawlingpipeline.utils.SaveImageFromURL;
import it.polimi.tweetcrawlingpipeline.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import com.cybozu.labs.langdetect.Detector;
import com.cybozu.labs.langdetect.DetectorFactory;
import com.cybozu.labs.langdetect.LangDetectException;

public class TweetAnalyzer {
	
	public static TweetAnalyzer tweetAnalyzer;
	private TweetAnalyzer() {}
	
	public static TweetAnalyzer getObject() {
		if (tweetAnalyzer == null)
			tweetAnalyzer = new TweetAnalyzer();
		
		return tweetAnalyzer;
	}

	/**
	 * Analyzes a single tweet to understand whether to keep it or not (i.e., whether it is relevant)
	 * @param JSONstring
	 * @param textProcessor
	 * @param tweetRepository
	 * @param classifier
	 * @param nonAppropriateContentFilter
	 * @param bannedWordsFilter
	 * @param description
	 */
	public void analyzeTweet(String JSONstring, TextProcessor textProcessor, TweetRepository tweetRepository, 
									GenericClassifier<SVMSample> textClassifier, GenericClassifier<SVMSample> visualClassifier,
									Filter nonAppropriateContentFilter, Filter bannedWordsFilter, DatasetDescription description,
									BeliefAnalyzer<SVMSample> beliefAnalyzer, long startCountDate) {		
		try {
			JSONObject rawJSON = new JSONObject(JSONstring);
			String tweet = rawJSON.getString(Config.tweetText);
			
			List<String> numberedWords = textProcessor.subdivideDocumentInNumberedWords(tweet);
			if (!nonAppropriateContentFilter.isAppropriate(numberedWords) || !bannedWordsFilter.isAppropriate(numberedWords))
				return;
			
			try {
				String rawWords = textProcessor.extractRawWords(tweet);
				if (!rawWords.equals("")){
					Detector detector = DetectorFactory.create();
					detector.append(rawWords.replace("RT",""));
					int numWords = rawWords.replaceAll("[^a-zA-Z0-9 ]","").replaceAll("RT", "").replaceAll("( )+", " ").split(" ").length;
								
					if (numWords > 5 && !detector.detect().equals(Config.english)) {
						System.out.println("[LOG" + new Date(System.currentTimeMillis())+ "] Non-English tweet.");
						return;
					}
				}
				else
					return;
			}
			catch (LangDetectException e) {}
			
			classifyObject(rawJSON, textProcessor, nonAppropriateContentFilter, bannedWordsFilter, textClassifier, visualClassifier, description, 
							tweetRepository, beliefAnalyzer, startCountDate);
													
		} 
		catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	private void classifyObject(JSONObject rawJSON, TextProcessor textProcessor, Filter nonAppropriateContentFilter, Filter bannedWordsFilter, 
				GenericClassifier<SVMSample> textClassifier, GenericClassifier<SVMSample> visualClassifier, DatasetDescription description, 
				TweetRepository tweetRepository, BeliefAnalyzer<SVMSample> beliefAnalyzer, long startCountDate) {
		
		
		ClassificationResult textClassificationResult = analyzeText(rawJSON, textProcessor, nonAppropriateContentFilter, bannedWordsFilter, textClassifier, description, tweetRepository);
		ClassificationResult imageClassificationResult = analyzeImage(rawJSON, visualClassifier);
		
		if (textClassificationResult == null && imageClassificationResult == null)
			return;
		
		int aggregatedLabel = 1;
		if (textClassificationResult == null && imageClassificationResult != null)
			aggregatedLabel = imageClassificationResult.getClassId();
		else if (textClassificationResult != null && imageClassificationResult == null)
			aggregatedLabel = textClassificationResult.getClassId();
		else if (textClassificationResult != null && imageClassificationResult != null)
			aggregatedLabel = BayesianClassifierOpinionAggregator.getObject().aggregateLabel(textClassificationResult.getClassId(), imageClassificationResult.getClassId(), beliefAnalyzer, description);
		
		long currentDate = System.currentTimeMillis();
		if (aggregatedLabel == description.getPositiveClassId()) {
			System.out.println("[LOG" + new Date(System.currentTimeMillis())+ "] Positive tweet.");
			tweetRepository.saveTweet(rawJSON);
			//tweetRepository.updateCurrentUserScore();
			
			if (currentDate >= startCountDate)
				tweetRepository.updateTweetCount(currentDate);
			
			if (textClassificationResult != null)
				updateDatabase(tweetRepository, ((TextClassificationResult)textClassificationResult).getKeywords(), 
												((TextClassificationResult)textClassificationResult).getHashtags(), true, currentDate);
		}
		else {
			System.out.println("[LOG" + new Date(System.currentTimeMillis())+ "] Negative tweet.");
			if (textClassificationResult != null)
				updateDatabase(tweetRepository, ((TextClassificationResult)textClassificationResult).getKeywords(), 
												((TextClassificationResult)textClassificationResult).getHashtags(), false, currentDate);
		}
	}
	
	private ClassificationResult analyzeText(JSONObject rawJSON, TextProcessor textProcessor, Filter nonAppropriateContentFilter, Filter bannedWordsFilter, 
			GenericClassifier<SVMSample> classifier, DatasetDescription description, TweetRepository tweetRepository) {
		
		try {
			String tweet = rawJSON.getString(Config.tweetText);
				
			List<String> words = textProcessor.subdivideDocumentInWords(tweet);
			List<String> stemmedWords = textProcessor.stem(words);					
			List<String> hashtags = Utils.findHashtags(tweet.toLowerCase());
			
			SVMSample sample = new TextSample(stemmedWords);
			int classId = classifier.classify(sample);
			return new TextClassificationResult(classId, hashtags, textProcessor.filterTaxonomy(textProcessor.removeStopwords(words)));	
		} 
		catch (JSONException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	private ClassificationResult analyzeImage(JSONObject rawJSON, GenericClassifier<SVMSample> classifier) {
		try {
			TweetChunk entities = new TweetChunk(rawJSON.getJSONObject("entities"));
			
			if (entities != null && entities.has("media")) {
				TweetChunk media = new TweetChunk(entities.getSubArray("media").getJSONObject(0));
				
				if (media != null) {
					try {
						String diskPath = SaveImageFromURL.saveImage(media.getFieldValue("media_url_https"));
						if (diskPath != null) {
							SVMSample sample = new ImageSample(diskPath);
							int classId = classifier.classify(sample);
							
							File imageFile = new File(diskPath);
							imageFile.delete();
							
							return new ImageClassificationResult(classId);
						}
						else
							return null;
					} 
					catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		} 
		catch (JSONException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	private synchronized void updateDatabase(TweetRepository tweetRepository, List<String> keywords, List<String> hashtags, boolean isPositive, long currentDate){
		tweetRepository.saveTweetStatisticsInDatabase(keywords, hashtags, isPositive);
		if (isPositive)
			tweetRepository.indexTerms(keywords, hashtags, currentDate);
	}
}
