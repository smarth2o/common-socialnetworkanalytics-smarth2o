package it.polimi.tweetcrawlingpipeline.crawler.analyzers;

import it.polimi.tweetcrawlingpipeline.classifier.objects.DatasetDescription;
import it.polimi.tweetcrawlingpipeline.classifier.objects.samples.SVMSample;
import it.polimi.tweetcrawlingpipeline.classifiers.analyzers.BeliefAnalyzer;

public class BayesianClassifierOpinionAggregator extends ClassifierOpinionAggregator {
	
	private BayesianClassifierOpinionAggregator(){};
	
	public static ClassifierOpinionAggregator getObject() {
		if (opinionAggregator == null)
			opinionAggregator = new BayesianClassifierOpinionAggregator();
		
		return opinionAggregator;
	}
	
	@Override
	public int aggregateLabel(int textLabel, int imageLabel, BeliefAnalyzer<SVMSample> beliefAnalyzer, DatasetDescription description) {
		double eta = computeEta(textLabel, imageLabel, description, beliefAnalyzer);
		double beliefPositive = computeBelief(description.getPositiveClassId(), textLabel, imageLabel, beliefAnalyzer, eta);
		double beliefNegative = computeBelief(description.getNegativeClassId(), textLabel, imageLabel, beliefAnalyzer, eta);
		
		if (beliefPositive > beliefNegative)
			return description.getPositiveClassId();
		else
			return description.getNegativeClassId();
	}
	
	private double computeBelief(int trueLabel, int textLabel, int imageLabel, BeliefAnalyzer<SVMSample> beliefAnalyzer, double eta) {
		double textCorrectProb = beliefAnalyzer.getProbability(trueLabel, textLabel, 0);
		double imageCorrectProb = beliefAnalyzer.getProbability(trueLabel, imageLabel, 1);
		
		double belief = eta * textCorrectProb * imageCorrectProb;
		return belief;
	}
	
	private double computeEta(int textLabel, int imageLabel, DatasetDescription description, BeliefAnalyzer<SVMSample> beliefAnalyzer) {
		double prob = 0;
		
		for (int i = 0; i < description.getNumClasses(); i++) {
			int trueLabel = i;
			prob += beliefAnalyzer.getProbability(trueLabel, textLabel, 0) *
					beliefAnalyzer.getProbability(trueLabel, imageLabel, 1);
		}
		
		double eta = 1 / prob;
		return eta;
	}
}
