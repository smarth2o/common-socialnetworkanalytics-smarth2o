package it.polimi.tweetcrawlingpipeline.filters;

import it.polimi.tweetcrawlingpipeline.utils.Config;
import it.polimi.tweetcrawlingpipeline.utils.Utils;

import java.io.File;

public class NonAppropriateContentFilter extends Filter {
	
	public NonAppropriateContentFilter() {
		bannedWords = Utils.readFileLines(new File(Config.stopwordsFolder + Config.badWordsFile));
		
		normalizeList();
	}
}