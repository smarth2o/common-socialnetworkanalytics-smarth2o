package it.polimi.tweetcrawlingpipeline.filters;

import java.util.List;

public abstract class Filter {
	
	protected List<String> bannedWords;
	
	public boolean isAppropriate(List<String> words) {
		boolean appropriate = true;
		for (String word:words)
			if (bannedWords.contains(word)) {
				appropriate = false;
				break;
			}
		
		return appropriate;
	}
	
	protected void normalizeList() {
		for (int i = 0; i < bannedWords.size(); i++)
			bannedWords.set(i, bannedWords.get(i).toLowerCase());
	}

}
