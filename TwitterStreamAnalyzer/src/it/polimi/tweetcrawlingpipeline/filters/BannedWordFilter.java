package it.polimi.tweetcrawlingpipeline.filters;

import it.polimi.tweetcrawlingpipeline.utils.Config;
import it.polimi.tweetcrawlingpipeline.utils.Utils;

import java.io.File;

public class BannedWordFilter extends Filter {
	
	public BannedWordFilter() {
		bannedWords = Utils.readFileLines(new File(Config.stopwordsFolder + Config.bannedWordsFile));
		
		normalizeList();
	}
}
