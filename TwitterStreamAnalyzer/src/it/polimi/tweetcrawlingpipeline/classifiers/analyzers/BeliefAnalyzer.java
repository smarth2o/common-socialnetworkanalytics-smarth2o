package it.polimi.tweetcrawlingpipeline.classifiers.analyzers;

import it.polimi.tweetcrawlingpipeline.classifier.classifiers.GenericClassifier;
import it.polimi.tweetcrawlingpipeline.classifier.objects.DatasetDescription;
import it.polimi.tweetcrawlingpipeline.classifier.objects.samples.Sample;

import java.util.HashMap;
import java.util.List;

public class BeliefAnalyzer<T extends Sample> {
	
	private HashMap<ProbabilityIndex, Double> probabilities;

	public BeliefAnalyzer(List<GenericClassifier<T>> classifiers, DatasetDescription description) {
		probabilities = new HashMap<ProbabilityIndex, Double>();
		
		for (int i = 0; i < classifiers.size(); i++) {
			GenericClassifier<T> classifier = classifiers.get(i);
			int[][] confusionMatrix = classifier.getConfusionMatrix();
			
			updateHashmap(confusionMatrix, description, i);
		}
	}
	
	public void updateHashmap(int[][] confusionMatrix, DatasetDescription description, int classifierIndex) {
		for (int i = 0; i < description.getNumClasses(); i++)
			for (int j = 0; j < description.getNumClasses(); j++) {
				int trueLabel = i;
				int classifiedLabel = j;
				ProbabilityIndex key = new ProbabilityIndex(trueLabel, classifiedLabel, classifierIndex);
				
				double Nij = confusionMatrix[i][j];
				double Nj = 0;
				for (int k = 0; k < description.getNumClasses(); k++)
					Nj += confusionMatrix[k][j];
				
				double value = Nij / Nj;
				probabilities.put(key, new Double(value));
			}
	}
	
	public double getProbability(int trueLabel, int classifiedLabel, int classifierIndex) {
		ProbabilityIndex key = new ProbabilityIndex(trueLabel, classifiedLabel, classifierIndex);
		
		return probabilities.get(key);
	}
}
