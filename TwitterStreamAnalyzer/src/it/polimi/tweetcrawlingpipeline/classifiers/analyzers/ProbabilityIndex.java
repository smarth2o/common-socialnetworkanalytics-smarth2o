package it.polimi.tweetcrawlingpipeline.classifiers.analyzers;

import org.apache.commons.lang3.builder.EqualsBuilder;

public class ProbabilityIndex {
	
	private int trueLabel;
	private int classifiedLabel;
	private int classifierIndex;
	
	public ProbabilityIndex(int trueLabel, int classifiedLabel, int classifierIndex){
		this.trueLabel = trueLabel;
		this.classifiedLabel = classifiedLabel;
		this.classifierIndex = classifierIndex;
	}
	
	int getTrueLabel() {
		return trueLabel;
	}
	
	int getClassifiedLabe() {
		return classifiedLabel;
	}
	
	int getClassifierIndex() {
		return classifierIndex;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof ProbabilityIndex))
			return false;
		if (obj == this)
			return true;

		ProbabilityIndex rhs = (ProbabilityIndex) obj;
        return new EqualsBuilder().
            append(trueLabel, rhs.trueLabel).
            append(classifiedLabel, rhs.classifiedLabel).
            append(classifierIndex, rhs.classifierIndex).
            isEquals();
    }

    @Override
    public int hashCode() {
    	return trueLabel | (classifiedLabel << 1) | (classifierIndex << 2);
    }

}
