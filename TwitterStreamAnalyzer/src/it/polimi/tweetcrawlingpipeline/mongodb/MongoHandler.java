package it.polimi.tweetcrawlingpipeline.mongodb;

import it.polimi.tweetcrawlingpipeline.utils.Config;

import java.net.UnknownHostException;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Arrays;

import com.mongodb.AggregationOutput;
import com.mongodb.BasicDBObject;
import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;

public class MongoHandler {

	private static DB db;
	
	/**
	 * Constructor: connect to the database
	 */
	public MongoHandler() {	
		try {
			System.out.println("Connecting to Mongo DB..");
			MongoClient mongo = new MongoClient();
			//MongoCredential credential = MongoCredential.createMongoCRCredential(Config.user, Config.database, Config.password.toCharArray());
			//MongoClient mongo = new MongoClient(new ServerAddress(), Arrays.asList(credential));
			db = mongo.getDB(Config.database);
			System.out.println("Connected.");
		} 
		catch (UnknownHostException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Initialize the connection with the database
	 * @param collectionNames
	 */
	public void initializeCollections(List<String> collectionNames){
		DBObject options = BasicDBObjectBuilder.start().add("capped", false).add("size", 2000000000l).get();
		
		for (int i = 0; i < collectionNames.size(); i++)
			if (!db.collectionExists(collectionNames.get(i)))
				db.createCollection(collectionNames.get(i), options);
			else
				db.getCollection(collectionNames.get(i));
	}
	
	/**
	 * Retrieve the reference to a database collection
	 * @param collectionName
	 * @return collection
	 */
	public DBCollection getCollection(String collectionName) {
		return db.getCollection(collectionName);
	}
	
	/**
	 * Retrieve the tweet count for a specific collection
	 * @param collectionName
	 * @return count
	 */
	public long getCollectionCount(String collectionName){
		DBCollection items = getCollection(collectionName);
		return items.count();
	}

	/**
	 * Returns the set of objects contained in a collection.
	 * @param collectionName
	 * @return objects
	 */
	public DBCursor getCollectionObjects(String collectionName){
		DBCollection items = getCollection(collectionName);
		DBCursor cursor = items.find();

		return cursor;
	}

	/**
	 * Return the object answering to the specified query
	 * @param collection
	 * @param field
	 * @param value
	 * @return object
	 */
	public DBCursor retrieveObjectByValue(DBCollection collection, String field, String value){
		BasicDBObject query = new BasicDBObject(field, value);
		DBCursor cursor = collection.find(query);
		
		return cursor;
	}
	
	public DBCursor retrieveObjectByValue(DBCollection collection, String field, long value){
		BasicDBObject query = new BasicDBObject(field, value);
		DBCursor cursor = collection.find(query);
		
		return cursor;
	}
	
	/**
	 * Return the object answering to the specified query
	 * @param collectionName
	 * @param queryObject
	 * @return object
	 */
	public DBCursor retrieveObjectByQueryObject(String collectionName, DBObject queryObject) {
		return retrieveObjectByQueryObject(getCollection(collectionName), queryObject);
	}
	
	/**
	 * Return the object answering to the specified query
	 * @param collection
	 * @param queryObject
	 * @return object
	 */
	public DBCursor retrieveObjectByQueryObject(DBCollection collection, DBObject queryObject) {
		DBCursor cursor = collection.find(queryObject);
		
		return cursor;
	}
	
	/**
	 * Return the object answering to the specified query
	 * @param collection
	 * @param field
	 * @param value
	 * @return object
	 */
	public DBCursor retrieveObjectByLongValue(DBCollection collection, String field, long value){
		BasicDBObject query = new BasicDBObject(field, value);
		DBCursor cursor = collection.find(query);
		
		return cursor;
	}
	
	/**
	 * Check whether the object is in the database
	 * @param collectionName
	 * @param field
	 * @param value
	 * @return status
	 */
	public boolean isObjectInDatabase(String collectionName, String field, String value){
		DBCollection collection = getCollection(collectionName);
		
		BasicDBObject tweetQueryIdObject = new BasicDBObject();
		try {
			tweetQueryIdObject.put(field, value);
			if (collection.find(tweetQueryIdObject).hasNext())
				return true;
			else
				return false;
		
		} 
		catch (NumberFormatException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * Check whether the objects is in the database
	 * @param collectionName
	 * @param object
	 * @return status
	 */
	public boolean isObjectInDatabase(String collectionName, DBObject object) {
		DBCollection collection = getCollection(collectionName);
		return collection.find(object).hasNext();
	}
	
	
	
	/**
	 * Update a specified object
	 * @param collectionName
	 * @param object
	 * @param chunk
	 */
	public void updateObject(String collectionName, DBObject updateObj, DBObject query) {
		DBCollection collection = getCollection(collectionName);
		collection.update(query, updateObj);
	}
	
	/**
	 * Insert a new object
	 * @param collectionName
	 * @param object
	 */
	public void insert(String collectionName, DBObject object) {
		DBCollection collection = getCollection(collectionName);
		collection.insert(object);
	}
	
	/**
	 * Insert a new object
	 * @param collectionName
	 * @param object
	 */
	public void insert(String collectionName, List<DBObject> objects) {
		DBCollection collection = getCollection(collectionName);
		collection.insert(objects);
	}
	
	/**
	 * Get the first object of a collection
	 * @param collectionName
	 * @return object
	 */
	public DBObject getFirstObject(String collectionName) {
		return getCollection(collectionName).findOne();
	}
	
	/**
	 * Delete the specified object(s)
	 * @param collectionName
	 * @param field
	 * @param value
	 */
	public void delete(String collectionName, String field, String value) {
		getCollection(collectionName).remove(retrieveObjectByValue(getCollection(collectionName), field, value).next());
	}
	
	public void delete(String collectionName, String field, long value) {
		getCollection(collectionName).remove(retrieveObjectByValue(getCollection(collectionName), field, value).next());
	}
	
	public int computeCountQuery(
			long objectValue, String objectKey, String collectionName){
		BasicDBObject query=new BasicDBObject(objectKey,objectValue);

		int count=retrieveObjectByQueryObject(collectionName, query).count();
		return count;
	}
	
	public int computeDistinctAggregation(long value, String matchFieldKey,String groupFieldValue){
		int count=0;
		DBObject match=new BasicDBObject("$match",new BasicDBObject(matchFieldKey,value));
		DBObject groupFields = new BasicDBObject("_id",groupFieldValue);

		groupFields.put("count",new BasicDBObject("$sum",1));
		DBObject group=new BasicDBObject("$group",groupFields);
		
		DBObject fields=new BasicDBObject("count", 1);
		
		DBObject project=new BasicDBObject("$project",fields);
		List<DBObject> pipeline = Arrays.asList(match, group,project);
		
		DBCollection coll = getCollection(Config.tweetCollection);
		AggregationOutput output = coll.aggregate(pipeline);
		System.out.println("output "+output);
		Iterable<DBObject> mappedResults=output.results();
		
		for (DBObject object:mappedResults) {
			count=(int)object.get("count");
		}
		return count;
	}
	
	public Iterable<DBObject> findDistinctElementsByDate(Date from, Date to, String collectionName, String value){
		DBObject match = new BasicDBObject("$match",new BasicDBObject("creation_date",new BasicDBObject("$gte",from).append("$lt", to)));
		DBObject groupFields = new BasicDBObject("_id",value);

		DBObject group=new BasicDBObject("$group",groupFields);
		
		DBObject fields=new BasicDBObject("_id", 1);
		
		DBObject project=new BasicDBObject("$project",fields);
		List<DBObject> pipeline = Arrays.asList(match, group,project);
		
		DBCollection coll = getCollection(collectionName);
		AggregationOutput output = coll.aggregate(pipeline);
		
		Iterable<DBObject> mappedResults=output.results();
		return mappedResults;
		
	}
	
	public DBCursor getLastObject(String collectionName) {
		
		DBCollection collection=getCollection(collectionName);
		
		DBCursor queryCursor=collection.find();
		int collectionCount=(int) (queryCursor.count());
		
		DBCursor cursor = queryCursor.skip(collectionCount-1);
		
		
		return cursor;
	}
	
	public AggregationOutput aggregate(String collectionName, DBObject match, DBObject unwind, DBObject group) {
		return getCollection(collectionName).aggregate(match, unwind, group);
	}
	
	public int computeDistinctAggregationRetweetPerUser(long userId){
		int count=0;
		DBObject match=new BasicDBObject("$match",new BasicDBObject("user_id",userId).append("original_id",new BasicDBObject("$exists",true)));
		DBObject groupFields = new BasicDBObject("_id","$original_id");

		groupFields.put("count",new BasicDBObject("$sum",1));
		DBObject group=new BasicDBObject("$group",groupFields);
		
		DBObject fields=new BasicDBObject("count", 1);

		DBObject project=new BasicDBObject("$project",fields);
		List<DBObject> pipeline = Arrays.asList(match, group,project);

		DBCollection coll = getCollection(Config.tweetCollection);
		AggregationOutput output = coll.aggregate(pipeline);
		
		Iterable<DBObject> mappedResults=output.results();

		for (Iterator<DBObject> iterator = mappedResults.iterator(); iterator
				.hasNext();) {
			count++;
		}
		return count;
	}
	
	public int computeDistinctAggregationRetweetByOthers(long userId){
		int count=0;
		String authorid=String.valueOf(userId);
		DBObject match=new BasicDBObject("$match",new BasicDBObject("original_author_id",authorid));
		DBObject groupFields = new BasicDBObject("_id","$user_id");

		groupFields.put("count",new BasicDBObject("$sum",1));
		DBObject group=new BasicDBObject("$group",groupFields);
		
		DBObject fields=new BasicDBObject("count", 1);

		DBObject project=new BasicDBObject("$project",fields);
		List<DBObject> pipeline = Arrays.asList(match, group,project);

		DBCollection coll = getCollection(Config.tweetCollection);
		AggregationOutput output = coll.aggregate(pipeline);
		
		Iterable<DBObject> mappedResults=output.results();

		for (Iterator<DBObject> iterator = mappedResults.iterator(); iterator
				.hasNext();) {
			count++;
		}
		return count;
	}
	
	public int computeDistinctAggregationUserMentions(long userId){
		int count=0;
		DBObject match=new BasicDBObject("$match",new BasicDBObject("user_mentions",userId));
		DBObject groupFields = new BasicDBObject("_id","$user_id");

		groupFields.put("count",new BasicDBObject("$sum",1));
		DBObject group=new BasicDBObject("$group",groupFields);
		
		DBObject fields=new BasicDBObject("count", 1);

		DBObject project=new BasicDBObject("$project",fields);
		List<DBObject> pipeline = Arrays.asList(match, group,project);

		DBCollection coll = getCollection(Config.tweetCollection);
		AggregationOutput output = coll.aggregate(pipeline);
		
		Iterable<DBObject> mappedResults=output.results();

		for (Iterator<DBObject> iterator = mappedResults.iterator(); iterator
				.hasNext();) {
			count++;
		}
		return count;
	}
	
	public int computeRetweetNumberByOthers(long userId){
		String authorid=String.valueOf(userId);
		BasicDBObject query=new BasicDBObject("original_author_id",authorid);

		int count=retrieveObjectByQueryObject(Config.tweetCollection, query).count();
		return count;
	}
	
}

