package it.polimi.tweetcrawlingpipeline.mongodb;

import it.polimi.tweetcrawlingpipeline.mongodb.objects.TweetChunk;
import it.polimi.tweetcrawlingpipeline.utils.Config;
import it.polimi.tweetcrawlingpipeline.utils.SaveImageFromURL;
import it.polimi.tweetcrawlingpipeline.utils.Utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.mongodb.AggregationOutput;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.util.JSON;

public class TweetRepository {

	private MongoHandler mongoHandler;

	/**
	 * Constructor.
	 */
	public TweetRepository() {
		List<String> collectionNames = new ArrayList<String>();
		collectionNames.add(Config.tweetCollection);
		collectionNames.add(Config.userCollection);
		collectionNames.add(Config.urlCollection);
		collectionNames.add(Config.mediaCollection);

		collectionNames.add(Config.keywordsCollection);
		collectionNames.add(Config.hashtagsCollection);
		collectionNames.add(Config.collectionStatistics);

		collectionNames.add(Config.tweetCount5Min);
		collectionNames.add(Config.tweetCount10Sec);
		collectionNames.add(Config.keywordsAndHashtagsIndex);
		collectionNames.add(Config.pastUserScoreCollection);
		
		collectionNames.add(Config.negativeTweetsCollection);

		mongoHandler = new MongoHandler();
		mongoHandler.initializeCollections(collectionNames);
	}

	/**
	 * Returns a raw, unprocessed tweet from the database
	 * 
	 * @return rawTweet
	 */
	public JSONObject getRawTweetFromDB() {
		try {
			DBObject tweet = mongoHandler.getFirstObject(Config.rawTweets);
			if (tweet == null)
				return null;
			else
				return new JSONObject(tweet.toString());
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Returns the number of unprocessed tweets from the database
	 * 
	 * @return count
	 */
	public long getRawTweetCount() {
		return mongoHandler.getCollectionCount(Config.rawTweets);
	}

	public void updateTweetCount(long currentDate) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(currentDate);
		int year = calendar.get(Calendar.YEAR);
		int month = calendar.get(Calendar.MONTH);
		int day = calendar.get(Calendar.DAY_OF_MONTH);
		int hour = calendar.get(Calendar.HOUR_OF_DAY);
		int minute = calendar.get(Calendar.MINUTE);
		int seconds = calendar.get(Calendar.SECOND);

		Calendar tupleTime5Min = Calendar.getInstance();
		int tupleMin = minute - (minute % 5);
		tupleTime5Min.set(year, month, day, hour, tupleMin, 0);
		tupleTime5Min.set(Calendar.MILLISECOND, 0);
		updateTweetCount(tupleTime5Min, Config.tweetCount5Min);

		Calendar tupleTime10Sec = Calendar.getInstance();
		int tupleSec = seconds - (seconds % 10);
		tupleTime10Sec.set(year, month, day, hour, minute, tupleSec);
		tupleTime10Sec.set(Calendar.MILLISECOND, 0);
		updateTweetCount(tupleTime10Sec, Config.tweetCount10Sec);
	}

	private void updateTweetCount(Calendar calendar, String collectionName) {
		long time = calendar.getTimeInMillis();

		BasicDBObject object = new BasicDBObject();
		object.append("time", time);
		if (mongoHandler.isObjectInDatabase(collectionName, object)) {
			BasicDBObject updateTweetCount = new BasicDBObject().append("$inc",
					new BasicDBObject().append("count", 1));
			mongoHandler.updateObject(collectionName, updateTweetCount,
					new BasicDBObject().append("time", time));
		} else {
			object.append("count", 1);
			mongoHandler.insert(collectionName, object);
		}
	}

	public void indexTerms(List<String> keywords, List<String> hashtags,
			long currentDate) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(currentDate);
		int year = calendar.get(Calendar.YEAR);
		int month = calendar.get(Calendar.MONTH);
		int day = calendar.get(Calendar.DAY_OF_MONTH);
		int hour = calendar.get(Calendar.HOUR_OF_DAY);
		int minute = calendar.get(Calendar.MINUTE);

		Calendar binTime = Calendar.getInstance();
		int tupleMin = minute - (minute % 10);
		binTime.set(year, month, day, hour, tupleMin, 0);
		binTime.set(Calendar.MILLISECOND, 0);

		BasicDBObject queryObject = new BasicDBObject();
		queryObject.append("time", binTime.getTimeInMillis());
		DBCursor result = mongoHandler.retrieveObjectByQueryObject(
				Config.keywordsAndHashtagsIndex, queryObject);
		if (!result.hasNext())
			createNewTermIndexObject(keywords, hashtags,
					binTime.getTimeInMillis());
		else
			updateTermIndexObject(keywords, hashtags, binTime.getTimeInMillis());
	}

	private void updateTermIndexObject(List<String> keywords,
			List<String> hashtags, long currentTime) {
		updateTermList(currentTime, keywords, "keywords");
		updateTermList(currentTime, hashtags, "hashtags");
	}

	private void updateTermList(long currentTime, List<String> termList,
			String type) {
		List<String> nonDupTerms = new ArrayList<String>(
				new LinkedHashSet<String>(termList));
		for (String term : nonDupTerms) {
			if (term.matches("[0-9]+") || term.length() <= 2)
				continue;

			// Existing term: update
			BasicDBObject queryObject = new BasicDBObject();
			queryObject.append("time", currentTime);
			queryObject.append(type + ".term", term);

			BasicDBObject updateObject = new BasicDBObject();
			updateObject.append("$inc", new BasicDBObject(type + ".$.count",
					Collections.frequency(termList, term)));
			mongoHandler.updateObject(Config.keywordsAndHashtagsIndex,
					updateObject, queryObject);

			// Non-existing term: add
			queryObject = new BasicDBObject();
			queryObject.append("time", currentTime);
			queryObject.append(type + ".term", new BasicDBObject("$ne", term));
			updateObject = new BasicDBObject();
			updateObject.append(
					"$addToSet",
					new BasicDBObject(type, new BasicDBObject("term", term)
							.append("count",
									Collections.frequency(termList, term))));
			mongoHandler.updateObject(Config.keywordsAndHashtagsIndex,
					updateObject, queryObject);
		}
	}

	private void createNewTermIndexObject(List<String> keywords,
			List<String> hashtags, long currentTime) {
		// Remove non-significant terms from previous bin
		if (mongoHandler.getCollection(Config.keywordsAndHashtagsIndex).count() >= 1) {
			BasicDBObject query = new BasicDBObject();
			DBObject previousObject = mongoHandler
					.retrieveObjectByQueryObject(
							Config.keywordsAndHashtagsIndex, query)
					.sort(new BasicDBObject("time", -1)).limit(1).next();
			long binTime = (long) previousObject.get("time");
			removeNonRelevantTerms(previousObject, "keywords", binTime);
			removeNonRelevantTerms(previousObject, "hashtags", binTime);
		}

		BasicDBObject object = new BasicDBObject();
		object.append("time", currentTime);
		object.append("keywords", buildTermList(keywords));
		object.append("hashtags", buildTermList(hashtags));
		mongoHandler.insert(Config.keywordsAndHashtagsIndex, object);
	}

	private void removeNonRelevantTerms(DBObject object, String type, long currentTime) {
		BasicDBList list = (BasicDBList) object.get(type);

        BasicDBObject unwind = new BasicDBObject("$unwind", "$" + type);
        BasicDBObject match = new BasicDBObject("$match", new BasicDBObject("time", currentTime));
        BasicDBObject group = new BasicDBObject("$group", new BasicDBObject(
                        "_id", "$time").append("count", new BasicDBObject("$max", "$"
                        + type + ".count")));
        AggregationOutput aggregation = mongoHandler.aggregate(
                        Config.keywordsAndHashtagsIndex, match, unwind, group);
        if (aggregation.results().iterator().hasNext()){
        	int max = (int) aggregation.results().iterator().next().get("count");

        	for (int i = 0; i < list.size(); i++) {
        		BasicDBObject termObject = (BasicDBObject) list.get(i);
        		if (termObject.getInt("count") < max * 0.2) {
        			BasicDBObject query = new BasicDBObject("time", currentTime);
        			BasicDBObject updateObject = new BasicDBObject("$pull",
                                                new BasicDBObject(type, new BasicDBObject("term",
                                                                termObject.getString("term"))));
        			mongoHandler.updateObject(Config.keywordsAndHashtagsIndex, updateObject, query);
        		}
        	}
        }

	}

	private BasicDBList buildTermList(List<String> terms) {
		BasicDBList termList = new BasicDBList();

		List<String> nonDupTerms = new ArrayList<String>(
				new LinkedHashSet<String>(terms));
		for (String term : nonDupTerms) {
			BasicDBObject termObject = new BasicDBObject();
			termObject.append("term", term);
			termObject.append("count", Collections.frequency(terms, term));

			termList.add(termObject);
		}

		return termList;
	}

	/**
	 * Deletes an unprocessed tweet from the database
	 * 
	 * @param id
	 */
	public void delete(String id) {
		mongoHandler.delete(Config.rawTweets, "id_str", id);
	}

	/**
	 * Stores a tweet in the collections
	 * 
	 * @param tweet
	 */
	public void saveTweet(JSONObject tweet) {
		if (!isTweetInDB(tweet)) {
			writeTweet(new TweetChunk(tweet));
			writeUser(new TweetChunk(tweet));
			writeExtraContent(new TweetChunk(tweet), "media",
					Config.mediaCollection);
			writeExtraContent(new TweetChunk(tweet), "urls",
					Config.urlCollection);
		}
	}

	/**
	 * Checks whether the tweet is in the database
	 * 
	 * @param tweet
	 * @return status
	 */
	public boolean isTweetInDB(JSONObject tweet) {
		try {
			return mongoHandler.isObjectInDatabase(Config.tweetCollection,
					"id", tweet.getString("id_str"));
		} catch (JSONException e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Writes the tweet content in the database
	 * 
	 * @param tweet
	 */
	private void writeTweet(TweetChunk tweet) {
		DBObject tweetObject = buildTweetObject(tweet);
		mongoHandler.insert(Config.tweetCollection, tweetObject);
	}

	/**
	 * Writes the user description in the database
	 * 
	 * @param user
	 */
	private void writeUser(TweetChunk user) {
		BasicDBObject userQueryIdObject = new BasicDBObject();
		long userId = Long.parseLong(new TweetChunk(user.getSubObject("user"))
				.getFieldValue("id_str"));
		userQueryIdObject.put("id", userId);

		if (!mongoHandler.isObjectInDatabase(Config.userCollection,
				userQueryIdObject)) {
			mongoHandler.insert(Config.userCollection,
					buildUserObject(user, userId));
		} else
			updateUserObject(user);
	}

	/**
	 * Returns an extra (media) object from the tweet
	 * 
	 * @param entity
	 * @param tag
	 * @return object
	 */
	private DBObject buildExtraObject(TweetChunk entity, String tag) {
		DBObject object = null;
		if (tag.equals("urls"))
			object = buildURLObject(entity);
		else if (tag.equals("media"))
			object = buildMediaObject(entity);
		else
			System.err.println("Wrong tag: " + tag);
		return object;
	}

	/**
	 * Analyzes an extra content collection from the tweet
	 * 
	 * @param tweet
	 * @param tag
	 * @param collection
	 */
	private void writeExtraContent(TweetChunk tweet, String tag,
			String collection) {
		if (tweet.getSubObject("entities").has(tag)) {
			JSONArray entities = new TweetChunk(tweet.getSubObject("entities"))
					.getSubArray(tag);
			for (int i = 0; i < entities.length(); i++)
				try {
					DBObject object = buildExtraObject(
							new TweetChunk(entities.getJSONObject(i)), tag);

					if (object != null)
						mongoHandler.insert(collection, object);
				} catch (JSONException e) {
					e.printStackTrace();
				}
		}
	}

	/**
	 * Builds the URL object
	 * 
	 * @param urlEntity
	 * @return URL
	 */
	private DBObject buildURLObject(TweetChunk urlEntity) {
		BasicDBObject queryIdObject = new BasicDBObject();
		queryIdObject.put("url", urlEntity.getFieldValue("url"));

		if (!mongoHandler.isObjectInDatabase(Config.urlCollection,
				queryIdObject)) {
			BasicDBObject urlObject = new BasicDBObject();
			urlObject.put("url", urlEntity.getFieldValue("url"));
			urlObject
					.put("display_url", urlEntity.getFieldValue("display_url"));
			urlObject.put("expanded_url",
					urlEntity.getFieldValue("expanded_url"));
			return urlObject;
		} else
			return null;
	}

	/**
	 * Builds the media object
	 * 
	 * @param mediaEntity
	 * @return media
	 */
	private DBObject buildMediaObject(TweetChunk mediaEntity) {
		BasicDBObject queryIdObject = new BasicDBObject();

		long mediaId = Long.parseLong(mediaEntity.getFieldValue("id_str"));
		queryIdObject.put("id", mediaId);

		if (!mongoHandler.isObjectInDatabase(Config.mediaCollection,
				queryIdObject)) {
			BasicDBObject mediaObject = new BasicDBObject();
			mediaObject.put("id", mediaId);
			mediaObject.put("display_url", mediaEntity.getFieldValue("display_url"));
			mediaObject.put("expanded_url", mediaEntity.getFieldValue("expanded_url"));
			mediaObject.put("media_url", mediaEntity.getFieldValue("media_url"));
			mediaObject.put("media_url_https", mediaEntity.getFieldValue("media_url_https"));
			mediaObject.put("type", mediaEntity.getFieldValue("type"));
			mediaObject.put("url", mediaEntity.getFieldValue("url"));

			try {
				String diskPath = SaveImageFromURL.saveImage(mediaEntity.getFieldValue("media_url_https"));
				mediaObject.put("media_path", diskPath);
			} catch (IOException e) {
				e.printStackTrace();
			}

			return mediaObject;
		} 
		else
			return null;
	}

	/**
	 * Builds the user object
	 * 
	 * @param tweet
	 * @param userId
	 * @return user
	 */
	private DBObject buildUserObject(TweetChunk tweet, long userId) {
		BasicDBObject userObject = new BasicDBObject();

		TweetChunk user = new TweetChunk(tweet.getSubObject("user"));
		userObject.put("id", userId);
		userObject.put("contributors_enabled",user.getBooleanFieldValue("contributors_enabled"));
		userObject.put("created_at", user.getFieldValue("created_at"));
		userObject.put("description", user.getFieldValue("description"));
		userObject.put("favourites_count",user.getIntFieldValue("favourites_count"));
		userObject.put("follow_request_sent",user.getFieldValue("follow_request_sent"));
		userObject.put("followers_count",user.getIntFieldValue("followers_count"));
		userObject.put("friends_count", user.getIntFieldValue("friends_count"));
		userObject.put("geo_enabled", user.getBooleanFieldValue("geo_enabled"));
		userObject.put("is_translator",user.getBooleanFieldValue("is_translator"));
		userObject.put("lang", user.getFieldValue("lang"));
		userObject.put("listed_count", user.getIntFieldValue("listed_count"));
		userObject.put("location", user.getFieldValue("location"));
		userObject.put("name", user.getFieldValue("name"));
		userObject.put("profile_image_url",user.getFieldValue("profile_image_url"));
		userObject.put("profile_image_url_https",user.getFieldValue("profile_image_url_https"));
		userObject.put("is_protected", user.getBooleanFieldValue("protected"));
		userObject.put("screen_name", user.getFieldValue("screen_name"));
		userObject.put("statuses_count",user.getIntFieldValue("statuses_count"));
		userObject.put("time_zone", user.getFieldValue("time_zone"));
		userObject.put("url", user.getFieldValue("url"));
		userObject.put("utc_offset", user.getFieldValue("utc_offset"));
		userObject.put("verified", user.getBooleanFieldValue("verified"));
		
		Coordinates coordinates = computeCoordinates(user.getFieldValue("location"));
		if (coordinates != null) {
			userObject.append("latitude", coordinates.getLatitude());
			userObject.append("longitude", coordinates.getLongitude());
		} 
		else {
			userObject.append("latitude", -1);
			userObject.append("longitude", -1);
		}

		return userObject;
	}
	
	private Coordinates computeCoordinates(String location) {
		String response = "";

		try {
			String urlString = "http://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer/find?f=pjson&text=" + location;
			urlString = urlString.replace(" ", "%20");
			URL url = new URL(urlString);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setDoOutput(true);

			InputStream is = connection.getInputStream();
			InputStreamReader isr = new InputStreamReader(is);
			BufferedReader br = new BufferedReader(isr);

			String nextLine = br.readLine();
			while (nextLine != null) {
				response += nextLine;
				nextLine = br.readLine();
			}

			connection.disconnect();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		if (!response.equals("")) {
			try {
				JSONObject jsonResponse = new JSONObject(response);
				JSONArray locations = jsonResponse.getJSONArray("locations");
				if (locations.length() > 0) {
					JSONObject locationObject = locations.getJSONObject(0);
					JSONObject features = (JSONObject) locationObject.get("feature");
					JSONObject point = features.getJSONObject("geometry");
					double latitude = point.getDouble("y");
					double longitude = point.getDouble("x");

					Coordinates coordinates = new Coordinates();
					coordinates.setLatitude(latitude);
					coordinates.setLongitude(longitude);

					return coordinates;
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	private void updateUserObject(TweetChunk tweet) {

		TweetChunk user = new TweetChunk(tweet.getSubObject("user"));
		long userId = Long.parseLong(user.getFieldValue("id_str"));
		BasicDBObject updateProfileImage = new BasicDBObject();
		updateProfileImage.append(
				"$set",
				new BasicDBObject().append("profile_image_url",
						user.getFieldValue("profile_image_url")).append(
						"profile_image_url_https",
						user.getFieldValue("profile_image_url_https")));
		mongoHandler.updateObject(Config.userCollection, updateProfileImage,
				new BasicDBObject().append("id", userId));

	}

	/**
	 * Builds the tweet object
	 * 
	 * @param tweet
	 * @return tweet
	 */
	private DBObject buildTweetObject(TweetChunk tweet) {
		BasicDBObject databaseTweetObject = new BasicDBObject();
		addGeneralInfo(tweet, databaseTweetObject);
		addRetweet(tweet, databaseTweetObject);

		TweetChunk entities = new TweetChunk(tweet.getSubObject("entities"));
		addLongSubField(entities, databaseTweetObject, "media", "id_str", "media_ids");
		addSubField(entities, databaseTweetObject, "urls", "url", "url");
		addSubField(entities, databaseTweetObject, "hashtags", "text", "hashtag");
		addLongSubField(entities, databaseTweetObject, "user_mentions", "id_str", "user_mentions");
		ArrayList<String> keywords = new ArrayList<String>();

		processDemoKeywords(tweet.getFieldValue("text"), keywords);
		ArrayList<String> hashtags = new ArrayList<String>(extractSubFieldValues(entities, "hashtags", "text"));
		processDemoHashtags(hashtags);

		addProcessedSubField(databaseTweetObject, "processedKeywords", keywords);
		addProcessedSubField(databaseTweetObject, "processedHashtags", hashtags);

		databaseTweetObject.put("user_id", Long.parseLong(new TweetChunk(tweet.getSubObject("user")).getFieldValue("id_str")));
		return databaseTweetObject;
	}

	/**
	 * Extract arrays from the tweet
	 * 
	 * @param tweet
	 * @param subFieldName
	 * @param valueName
	 * @return arrays
	 */
	private List<String> extractSubFieldValues(TweetChunk tweet,
			String subFieldName, String valueName) {
		List<String> subField = new ArrayList<String>();
		if (tweet.has(subFieldName)) {
			JSONArray subFieldEntities = tweet.getSubArray(subFieldName);

			for (int i = 0; i < subFieldEntities.length(); i++) {
				try {
					subField.add(subFieldEntities.getJSONObject(i).getString(
							valueName));
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}

		return subField;
	}

	/**
	 * Adds a subfield to a tweet
	 * 
	 * @param tweet
	 * @param databaseTweetObject
	 * @param subFieldName
	 * @param valueName
	 * @param databaseFieldName
	 */
	private void addSubField(TweetChunk tweet, DBObject databaseTweetObject, String subFieldName, String valueName, String databaseFieldName) {
		databaseTweetObject.put(databaseFieldName,
				extractSubFieldValues(tweet, subFieldName, valueName));
	}

	private void addProcessedSubField(DBObject databaseTweetObject,
			String databaseFieldName, ArrayList<String> words) {
		databaseTweetObject.put(databaseFieldName, words);
	}

	private void addLongSubField(TweetChunk tweet, DBObject databaseTweetObject, String subFieldName, String valueName, String databaseFieldName) {
		List<String> values = extractSubFieldValues(tweet, subFieldName,
				valueName);
		List<Long> longValues = new ArrayList<Long>();

		for (String value : values)
			longValues.add(Long.parseLong(value));

		databaseTweetObject.put(databaseFieldName, longValues);
	}

	/**
	 * Adds general information to a tweet
	 * 
	 * @param tweet
	 * @param databaseTweetObject
	 */
	private void addGeneralInfo(TweetChunk tweet, DBObject databaseTweetObject) {
		databaseTweetObject.put("text", tweet.getFieldValue("text"));
		databaseTweetObject.put("creation_date", Utils.getTwitterDate(tweet.getFieldValue("created_at")));
		databaseTweetObject.put("contributors", tweet.getJSON().optJSONArray("contributors"));
		databaseTweetObject.put("favorite_count", tweet.getIntFieldValue("favorite_count"));
		databaseTweetObject.put("favorited", tweet.getBooleanFieldValue("favorited"));
		databaseTweetObject.put("insertion_date", new Date());
		databaseTweetObject.put("id", tweet.getLongFieldValue("id_str"));

		try {
			double latitude = Double.NaN;
			double longitude = Double.NaN;
			if (!tweet.isNull("geo")) {
				JSONObject geoData = (JSONObject) tweet.getJSON().get("geo");
				JSONArray coordinates = geoData.getJSONArray("coordinates");
				latitude = coordinates.getDouble(0);
				longitude = coordinates.getDouble(1);
			} 
			else {
				if (!tweet.isNull("coordinates")) {
					JSONObject geoData = (JSONObject) tweet.getJSON().get(
							"coordinates");
					JSONArray coordinates = geoData.getJSONArray("coordinates");
					latitude = coordinates.getDouble(1); // swapped WRT "geo" tag
					longitude = coordinates.getDouble(0);
				}
			}
			if (Double.isNaN(latitude)) {
				databaseTweetObject.put("latitude", null);
				databaseTweetObject.put("longitude", null);
			}
			else {
				databaseTweetObject.put("latitude", latitude);
				databaseTweetObject.put("longitude", longitude);
				storeLocation(latitude, longitude, tweet.getFieldValue("text"), tweet);
			}
		} 
		catch (JSONException e) {
			e.printStackTrace();
		}

		databaseTweetObject.put("in_reply_to_screen_name", tweet.optString("in_reply_to_screen_name"));
		databaseTweetObject.put("in_reply_to_status_id", tweet.optString("in_reply_to_status_id"));
		databaseTweetObject.put("in_reply_to_user_id", tweet.optString("in_reply_to_user_id"));
		databaseTweetObject.put("lang", tweet.getFieldValue("lang"));
		if (!tweet.isNull("place"))
			databaseTweetObject.put("place", (new TweetChunk(tweet.getOptionalSubObject("place"))).getFieldValue("country_code"));
		else
			databaseTweetObject.put("place", null);

		databaseTweetObject.put("retweet_count", tweet.getIntFieldValue("retweet_count"));
		databaseTweetObject.put("retweeted", tweet.getBooleanFieldValue("retweeted"));
		databaseTweetObject.put("truncated", tweet.getBooleanFieldValue("truncated"));
	}
	
	private void storeLocation(double latitude, double longitude, String text, TweetChunk tweet) {				
		BasicDBObject newLocation = new BasicDBObject();
		newLocation.append("latitude", latitude);
		newLocation.append("longitude", longitude);
		newLocation.append("text", text);
		
		TweetChunk user = new TweetChunk(tweet.getSubObject("user"));
		newLocation.append("author", user.getFieldValue("screen_name"));
		
		storeLocationInDynamicCollection(newLocation);
		storeLocationInHistoricalLocation(newLocation);
	}
	
	private void storeLocationInDynamicCollection(BasicDBObject newLocation) {
		long time = System.currentTimeMillis();
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(time);
		calendar.set(Calendar.MILLISECOND, 0);
		calendar.set(Calendar.MINUTE, calendar.get(Calendar.MINUTE) - calendar.get(Calendar.MINUTE) % Config.dynamicLocationStoreStepInMin);
		calendar.set(Calendar.SECOND, 0);
		time = calendar.getTimeInMillis();
		
		storeTweetLocation(newLocation, Config.dynamicTweetLocations, time);
	}
	
	private void storeLocationInHistoricalLocation(BasicDBObject newLocation) {
		long time = System.currentTimeMillis();
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(time);
		calendar.set(Calendar.MILLISECOND, 0);
		calendar.set(Calendar.MINUTE, calendar.get(Calendar.MINUTE) - calendar.get(Calendar.MINUTE) % Config.hystoricalLocationStoreStepInMin);
		calendar.set(Calendar.SECOND, 0);
		time = calendar.getTimeInMillis();
		
		storeTweetLocation(newLocation, Config.hystoricalTweetLocations, time);
	}
	
	private void storeTweetLocation(BasicDBObject newLocation, String collection, long time) {
		if (mongoHandler.isObjectInDatabase(collection, new BasicDBObject("time",time)))
			mongoHandler.updateObject(collection, new BasicDBObject("$push", new BasicDBObject("locations", newLocation)), 
																	new BasicDBObject("time",time));
		else {
			DBCursor existingTuples = mongoHandler.getCollectionObjects(collection);
			while (existingTuples.hasNext()) {
				DBObject object = existingTuples.next();
				mongoHandler.delete(collection, "time", (long)object.get("time"));
			}
			
			BasicDBObject dbObject = new BasicDBObject("time",time);
			List<BasicDBObject> locations = new ArrayList<BasicDBObject>();
			locations.add(newLocation);
			dbObject.append("locations", locations);
			mongoHandler.insert(collection, dbObject);
		}
	}

	/**
	 * Adds a retweet status to the tweet
	 * 
	 * @param tweet
	 * @param databaseTweetObject
	 */
	private void addRetweet(TweetChunk tweet, DBObject databaseTweetObject) {
		if (tweet.has("retweeted_status")) {
			TweetChunk retweet = new TweetChunk(
					tweet.getSubObject("retweeted_status"));
			databaseTweetObject.put("original_author_id", (new TweetChunk(
					retweet.getSubObject("user"))).getFieldValue("id_str"));
			databaseTweetObject.put("original_content",
					retweet.getFieldValue("text"));
			databaseTweetObject.put("original_id",
					retweet.getFieldValue("id_str"));

			updateOriginalRetweetCount(retweet);
		}
	}

	/**
	 * Updates the original tweet object (from a retweet)
	 * 
	 * @param retweet
	 */
	private void updateOriginalRetweetCount(TweetChunk retweet) {
		BasicDBObject tweetQueryIdObject = new BasicDBObject();
		long tweet_id = Long.parseLong(retweet.getFieldValue("id_str"));

		tweetQueryIdObject.put("id", tweet_id);

		if (mongoHandler.isObjectInDatabase(Config.tweetCollection,
				tweetQueryIdObject)) {
			BasicDBObject updateRetweetCount = new BasicDBObject().append(
					"$inc", new BasicDBObject().append("retweet_count", 1));
			mongoHandler.updateObject(Config.tweetCollection,
					updateRetweetCount,
					new BasicDBObject().append("id", tweet_id));
		}
	}

	private void saveStatistics(int numKeywords, int numHashtags,
			boolean isDocumentPositive) {
		BasicDBObject object = (BasicDBObject) mongoHandler
				.getFirstObject(Config.collectionStatistics);

		if (object == null) {
			object = new BasicDBObject();
			object.append("positive", 0);
			object.append("negative", 0);
			object.append("wordCount", 0);
			object.append("hashtagCount", 0);
			mongoHandler.insert(Config.collectionStatistics, object);
		}

		BasicDBObject newObject = new BasicDBObject();
		newObject
				.append("$set",
						new BasicDBObject()
								.append("positive",
										isDocumentPositive ? 1 + (int) object
												.get("positive") : (int) object
												.get("positive"))
								.append("negative",
										isDocumentPositive ? (int) object
												.get("negative")
												: 1 + (int) object
														.get("negative"))
								.append("wordCount",
										(int) object.get("wordCount")
												+ numKeywords)
								.append("hashtagCount",
										(int) object.get("hashtagCount")
												+ numHashtags));
		mongoHandler.updateObject(Config.collectionStatistics, newObject,
				object);
	}

	public void saveTweetStatisticsInDatabase(List<String> keywords,
			List<String> hashtags, boolean isDocumentPositive) {
		List<String> nonEmptyKeywords = new ArrayList<String>(keywords);
		List<String> nonEmptyHashtags = new ArrayList<String>(hashtags);
		nonEmptyKeywords.removeAll(Collections.singleton(""));
		nonEmptyHashtags.removeAll(Collections.singleton(""));

		saveStatistics(nonEmptyKeywords.size(), nonEmptyHashtags.size(),
				isDocumentPositive);

		saveWordsInDatabase(keywords, isDocumentPositive,
				Config.keywordsCollection);
		saveWordsInDatabase(hashtags, isDocumentPositive,
				Config.hashtagsCollection);
	}

	private void saveWordsInDatabase(List<String> words,
			boolean isDocumentPositive, String collection) {
		Map<String, Integer> wordFrequencies = computeFrequencies(words);

		DBCursor resultSet = mongoHandler.getCollectionObjects(collection);

		while (resultSet.hasNext()) {
			DBObject object = resultSet.next();
			String keyword = (String) object.get("keyword");

			if (wordFrequencies.containsKey(keyword)) {
				int frequency = wordFrequencies.get(keyword)
						+ (int) object.get("frequency");
				int numPositiveDocuments = isDocumentPositive ? 1 + (int) object
						.get("positiveDocuments") : (int) object
						.get("positiveDocuments");
				int numNegativeDocuments = isDocumentPositive ? (int) object
						.get("negativeDocuments") : 1 + (int) object
						.get("negativeDocuments");
				int numDocuments = 1 + (int) object.get("documents");

				BasicDBObject newObject = new BasicDBObject();
				newObject.append(
						"$set",
						new BasicDBObject()
								.append("frequency", frequency)
								.append("positiveDocuments",
										numPositiveDocuments)
								.append("negativeDocuments",
										numNegativeDocuments)
								.append("documents", numDocuments));
				mongoHandler.updateObject(collection, newObject, object);

				wordFrequencies.remove(keyword);
			}
		}

		List<DBObject> objects = new ArrayList<DBObject>();
		for (Entry<String, Integer> keyword : wordFrequencies.entrySet()) {
			DBObject object = new BasicDBObject();
			object.put("keyword", keyword.getKey());
			object.put("frequency", keyword.getValue());
			object.put("positiveDocuments", isDocumentPositive ? 1 : 0);
			object.put("negativeDocuments", isDocumentPositive ? 0 : 1);
			object.put("documents", 1);
			objects.add(object);
		}
		if (objects.size() > 0) {
			mongoHandler.insert(collection, objects);
		}
	}

	private Map<String, Integer> computeFrequencies(List<String> words) {
		Map<String, Integer> wordFrequencies = new HashMap<String, Integer>();

		for (String word : words) {
			if (word.isEmpty())
				continue;
			Integer frequency = wordFrequencies.get(word);
			if (frequency == null)
				wordFrequencies.put(word, 1);
			else
				wordFrequencies.put(word, 1 + frequency);
		}

		return wordFrequencies;
	}

	/**
	 * Queries a collection of words
	 * 
	 * @param collectionName
	 * @return words
	 */
	public ArrayList<String> QueryWordsCollections(String collectionName) {
		ArrayList<String> wordsList = new ArrayList<String>();

		DBCursor cursor = mongoHandler.getCollectionObjects(collectionName);

		while (cursor.hasNext()) {
			Object word = cursor.next().get("word");
			wordsList.add(word.toString());

		}

		return wordsList;
	}

	/**
	 * Removes unnecessary words from a list of words
	 * 
	 * @param words
	 * @param collectionName
	 */
	public void removeWords(ArrayList<String> words, String collectionName) {
		ArrayList<String> badWordsList = new ArrayList<String>();

		badWordsList = QueryWordsCollections(collectionName);
		Collection<String> badWordsCollection = badWordsList;
		words.removeAll(badWordsCollection);
	}

	/**
	 * Processes keywords for the demo and saves them in a database
	 * 
	 * @param tweet
	 * @param keywords
	 */
	public void processDemoKeywords(String tweet, ArrayList<String> keywords) {
		String filteredTweet = Utils.removeRetweets(tweet);
		filteredTweet = Utils.removeURLs(filteredTweet);
		filteredTweet = Utils.removeHashtags(filteredTweet);
		filteredTweet = Utils.removeUserMentions(filteredTweet);
		ArrayList<String> tweetKeywords = Utils
				.transformIntoWords(filteredTweet);
		Collection<String> tweetCollection = tweetKeywords;
		keywords.addAll(tweetCollection);

		removeWords(keywords, Config.stopwordsCollection);
		removeWords(keywords, Config.stopwordsCollection);
		Utils.removeEmptySpace(keywords);
	}

	/**
	 * Processes hashtags for the demo and saves them in a database
	 * 
	 * @param tweet
	 * @param hashtags
	 */
	public void processDemoHashtags(ArrayList<String> hashtagSet) {
		removeWords(hashtagSet, Config.badWordsCollection);
	}
	
	public void saveNegativeTweet(JSONObject negativeTweet) {
		mongoHandler.insert(Config.negativeTweetsCollection, (DBObject) JSON.parse(negativeTweet.toString()));
	}
}
