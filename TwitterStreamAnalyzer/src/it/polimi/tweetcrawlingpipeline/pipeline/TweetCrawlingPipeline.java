package it.polimi.tweetcrawlingpipeline.pipeline;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.opencv.core.Core;

import com.cybozu.labs.langdetect.DetectorFactory;
import com.cybozu.labs.langdetect.LangDetectException;

import weka.core.stemmers.SnowballStemmer;

import it.polimi.tweetcrawlingpipeline.classifier.classifiers.GenericClassifier;
import it.polimi.tweetcrawlingpipeline.classifier.classifiers.VisualSVMClassifier;
import it.polimi.tweetcrawlingpipeline.classifier.classifiers.factories.GenericClassifierFactory;
import it.polimi.tweetcrawlingpipeline.classifier.classifiers.factories.SVMClassifierFactory;
import it.polimi.tweetcrawlingpipeline.classifier.classifiers.factories.TextSVMClassifierFactory;
import it.polimi.tweetcrawlingpipeline.classifier.classifiers.factories.VisualSVMClassifierFactory;
import it.polimi.tweetcrawlingpipeline.classifier.objects.DatasetDescription;
import it.polimi.tweetcrawlingpipeline.classifier.objects.dictionaries.VisualVocabularyExtractor;
import it.polimi.tweetcrawlingpipeline.classifier.objects.samples.ImageSample;
import it.polimi.tweetcrawlingpipeline.classifier.objects.samples.SVMSample;
import it.polimi.tweetcrawlingpipeline.classifier.parameters.ClassifierParameterizer;
import it.polimi.tweetcrawlingpipeline.classifier.parameters.LearningCurvesBuilder;
import it.polimi.tweetcrawlingpipeline.classifier.utils.ModelSerializer;
import it.polimi.tweetcrawlingpipeline.classifiers.analyzers.BeliefAnalyzer;
import it.polimi.tweetcrawlingpipeline.crawler.crawlers.GenericCrawler;
import it.polimi.tweetcrawlingpipeline.filters.BannedWordFilter;
import it.polimi.tweetcrawlingpipeline.filters.Filter;
import it.polimi.tweetcrawlingpipeline.filters.NonAppropriateContentFilter;
import it.polimi.tweetcrawlingpipeline.mongodb.TweetRepository;
import it.polimi.tweetcrawlingpipeline.utils.Config;

public class TweetCrawlingPipeline {
	
	private static boolean TEST_TEXT = false;
	private static boolean TEST_IMAGE = false;
	private static boolean LEARNING_CURVES = false;
	
	private GenericClassifier<SVMSample> 	textClassifier;
	private GenericClassifier<SVMSample>	imageClassifier;
	private GenericCrawler 					crawler;
	private DatasetDescription 				description;
	private TweetRepository 				tweetRepository;
	private Filter							nonAppropriateContentFilter;
	private Filter							bannedWordsFilter;
	private BeliefAnalyzer<SVMSample>		beliefAnalyzer;
	
	/**
	 * @param description
	 * @param textClassifierFactory
	 * @param imageClassifierFactory
	 * @param crawlerType
	 */
	public TweetCrawlingPipeline(DatasetDescription description, GenericClassifierFactory<SVMSample> textClassifierFactory, 
			GenericClassifierFactory<SVMSample> imageClassifierFactory, String crawlerType) {
		this.description = description;
		
		File textModel = new File(Config.textModelFile);
		if (textModel.exists())
			this.textClassifier = initializeClassifier(Config.textModelFile, TEST_TEXT);
		else
			this.textClassifier = initializeClassifier(description, textClassifierFactory, Config.textModelFile, TEST_TEXT);
		
		File imageModel = new File(Config.imageModelFile);
		if (imageModel.exists()) {
			this.imageClassifier = initializeClassifier(Config.imageModelFile, TEST_IMAGE);
			((VisualSVMClassifier)imageClassifier).convertDictionary();
			((VisualSVMClassifier)imageClassifier).initialize();
		}
		else {
			extractVisualVocabulary(description);
			this.imageClassifier = initializeClassifier(description, imageClassifierFactory, Config.imageModelFile, TEST_IMAGE);
		}

		tweetRepository = new TweetRepository();
		nonAppropriateContentFilter = new NonAppropriateContentFilter();
		bannedWordsFilter = new BannedWordFilter();
		crawler = GenericCrawler.getFactory(crawlerType, this);
		
		List<GenericClassifier<SVMSample>> classifiersList = new ArrayList<GenericClassifier<SVMSample>>();
		classifiersList.add(textClassifier);
		classifiersList.add(imageClassifier);
		beliefAnalyzer = new BeliefAnalyzer<>(classifiersList, description);
	}
	
	 public TweetCrawlingPipeline(String language, String crawlerType){
		 crawler = GenericCrawler.getFactory(crawlerType, this);		 
	 }
	 
	 private void extractVisualVocabulary(DatasetDescription description) {
		 File vocabularyFile = new File(Config.imageVocabularyFile);
		 if (vocabularyFile.exists())
			 return;
		 
		 VisualVocabularyExtractor vocabularyExtractor = new VisualVocabularyExtractor(Config.imageVocabularyFile, Config.annotatedImagesFile, description);
		 vocabularyExtractor.extractVocabulary();
	 }
	 
	 /**
	 * Initializes a classifier by training it
	 * @param description
	 * @param language
	 * @return classifier
	 */
	 private GenericClassifier<SVMSample> initializeClassifier(DatasetDescription description, GenericClassifierFactory<SVMSample> classifierFactory, String model, boolean isTestRequired) {
		System.out.println("Initialize classifier and training set...");	
		GenericClassifier<SVMSample> classifier = classifierFactory.build(description);
		
		System.out.println("Train classifier...");
		classifier.train();
		
		testClassifier(isTestRequired, classifier);
		
		classifier.shrinkTrainingSet();
		ModelSerializer.serializeModel(classifier, model);
		
		return classifier;
	}
	
	/**
	 * Initializes a classifier by loading the already trained model
	 * @return classifier
	 */
	private GenericClassifier<SVMSample> initializeClassifier(String model, boolean isTestRequired) {
		System.out.println("Loading model...");
		GenericClassifier<SVMSample> classifier = ModelSerializer.deserializeModel(model);
		
		testClassifier(isTestRequired, classifier);
		
		classifier.initializeAttributes();
						
		return classifier;
	}
	
	/**
	 * Tests the classifier to compute the MSE (if required)
	 */
	private void testClassifier(boolean isTestRequired, GenericClassifier<SVMSample> classifier) {
		if (isTestRequired) {
			System.out.println("Test classifier...");
			classifier.test();
		}
	}
	
	/**
	 * Starts the Twitter crawling
	 */
	public void startCrawling() {
		crawler.crawl();
	}
	
	/**
	 * Returns the text classifier
	 * @return classifier
	 */
	public GenericClassifier<SVMSample> getTextClassifier() {
		return textClassifier;
	}
	
	/**
	 * Returns the visual classifier
	 * @return imageClassifier
	 */
	public GenericClassifier<SVMSample> getImageClassifier() {
		return imageClassifier;
	}
	
	public TweetRepository getTweetRepository() {
		return tweetRepository;
	}
	
	public Filter getNonAppropriateContentFilter() {
		return nonAppropriateContentFilter;
	}
	
	public Filter getBannedWordsFilter() {
		return bannedWordsFilter;
	}
	
	public BeliefAnalyzer<SVMSample> getBeliefAnalyzer() {
		return beliefAnalyzer;
	}
	
	/**
	 * Returns the dataset description
	 * @return description
	 */
	public DatasetDescription getDescription() {
		return description;
	}
	
	public void startStemmer() {
		SnowballStemmer stemmer = new SnowballStemmer();
		System.out.println("Test stemmer: from lighting to " + stemmer.stem("lighting"));
	}
	
	
	
	/**
	 * Main.
	 * @param args
	 */
	public static void main(String[] args) {		
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		try {
			DetectorFactory.loadProfile(Config.languageProfileDirectory);
		} 
		catch (LangDetectException e) {
			e.printStackTrace();
		}
		
		String language = Config.english;
		String crawlerType = Config.twitterDatabaseStream;
		
		double textClassifierCost = 15;
		double textClassifierGamma = 0.01;
		
		double imageClassifierCost = 1;
		double imageClassifierGamma = 1000;
		
		DatasetDescription description = new DatasetDescription();
		description.addClass("Y");
		description.addClass("N");
		description.setPositiveClassId(0);
		description.setNegativeClassId(1);
				
		GenericClassifierFactory<SVMSample> textClassifierFactory = new TextSVMClassifierFactory(language, Config.dictionaryFileName, Config.IDFfileName, textClassifierCost, textClassifierGamma);
		
		if (imageClassifierCost == -1 && imageClassifierGamma == -1) {
			System.out.println("[PARAMETERS] Visual classifier parameterization.");
			SVMClassifierFactory<ImageSample> factory = new VisualSVMClassifierFactory(Config.imageVocabularyFile);
			
			if (LEARNING_CURVES) {
				double testedCost = 1.0;
				double testedGamma = 1000.0;
				LearningCurvesBuilder<ImageSample,List<List<Double>>> learningCurvesBuilder = new LearningCurvesBuilder<ImageSample,List<List<Double>>>(factory, 
																											description, testedCost, testedGamma);
				learningCurvesBuilder.buildCurves();
			}
			ClassifierParameterizer<ImageSample,List<List<Double>>> parameterizer = new ClassifierParameterizer<ImageSample,List<List<Double>>>(
																							factory, description);
			parameterizer.computeParameters();
			imageClassifierCost = parameterizer.getCost();
			imageClassifierGamma = parameterizer.getGamma();
		}
		
		GenericClassifierFactory<SVMSample> imageClassifierFactory = new VisualSVMClassifierFactory(Config.imageVocabularyFile, imageClassifierCost, imageClassifierGamma);
		
		TweetCrawlingPipeline crawlingPipeline = new TweetCrawlingPipeline(description, textClassifierFactory, imageClassifierFactory, crawlerType);
		crawlingPipeline.startStemmer();
		crawlingPipeline.startCrawling();
		
	}

}
