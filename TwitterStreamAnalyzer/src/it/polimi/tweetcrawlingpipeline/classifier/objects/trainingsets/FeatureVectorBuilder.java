package it.polimi.tweetcrawlingpipeline.classifier.objects.trainingsets;

import java.io.Serializable;
import java.util.List;

import it.polimi.tweetcrawlingpipeline.classifier.objects.dictionaries.Dictionary;
import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;

public class FeatureVectorBuilder implements Serializable {
	

	private static final long 	serialVersionUID = 3953744632861988948L;
	
	private Instance 					emptyInstance;
	private Dictionary<List<String>>	dictionary;
	private double[]					IDF;
	private FastVector					attributes;
	private List<Double>				minimums;
	private List<Double>				ranges;
	
	public FeatureVectorBuilder(Dictionary<List<String>> dictionary, FastVector attributes, double[] IDF, List<Double> minimums, List<Double> ranges) {
		this.dictionary = dictionary;
		this.IDF = IDF;
		this.attributes = attributes;
		this.minimums = minimums;
		this.ranges = ranges;
		
		this.emptyInstance = new Instance(dictionary.getDictionaryLength() + 1);
		
		for (int i = 0; i < emptyInstance.numValues(); i++)
			emptyInstance.setValue((Attribute)attributes.elementAt(i), 0);
	}
	
	public Instance build(List<String> words) {
		FeatureVector featureVector = new FeatureVector.Builder(dictionary, attributes, IDF)
															.featureVector(emptyInstance)
															.components(words)
															.normalize(minimums, ranges)
															.build();
		
		return featureVector.getInstance();
	}

}
