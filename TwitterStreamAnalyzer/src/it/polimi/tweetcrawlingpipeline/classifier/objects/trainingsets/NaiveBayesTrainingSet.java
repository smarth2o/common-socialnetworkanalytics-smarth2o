package it.polimi.tweetcrawlingpipeline.classifier.objects.trainingsets;

import java.util.List;

import it.polimi.tweetcrawlingpipeline.classifier.objects.DatasetDescription;
import it.polimi.tweetcrawlingpipeline.classifier.objects.annotatedinstances.SampleSet;

import weka.core.FastVector;
import weka.core.Instance;

public class NaiveBayesTrainingSet extends TextualTrainingSet {
	
	private static final long serialVersionUID = -4477160757079620795L;
	
	public NaiveBayesTrainingSet(Instance instance, String relationName,
			FastVector attributes) {
		super(instance, relationName, attributes);
	}
	
	/**
	 * Constructor.
	 * @param trainingSetFile
	 */
	public NaiveBayesTrainingSet(SampleSet trainingInstances, String relationName, DatasetDescription description, FastVector attributes) {
		this.relationName = relationName;
		this.attributes = attributes;
		buildTrainingSet(trainingInstances, relationName, description, attributes);
	}

	@Override
	protected Instance computeInstance(List<String> words, String classId, FastVector attributes) {
		StringBuilder builder = new StringBuilder();
		for(String s: words) {
		    builder.append(s);
		}
		String filteredText = builder.toString();
		
		double[] newInst = new double[2];
		newInst[0] = (double)data.attribute(0).addStringValue(filteredText);
		newInst[1] = (double)data.attribute(1).indexOfValue(classId);
		
		return new Instance(1.0, newInst);
	}
}
