package it.polimi.tweetcrawlingpipeline.classifier.objects.trainingsets;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import it.polimi.tweetcrawlingpipeline.classifier.objects.DatasetDescription;
import it.polimi.tweetcrawlingpipeline.classifier.objects.annotatedinstances.SampleSet;
import it.polimi.tweetcrawlingpipeline.classifier.objects.dictionaries.Dictionary;
import it.polimi.tweetcrawlingpipeline.classifier.utils.InstanceNormalizer;

import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;

public class TextSVMTrainingSet extends TextualTrainingSet {

	private static final long 			serialVersionUID = 7997233591097884538L;
	private Dictionary<List<String>> 	dictionary;
	private double[]					IDF;
	
	private List<Double>				minimums;
	private List<Double>				ranges;
	
	public TextSVMTrainingSet(Instance instance, String relationName,
			FastVector attributes) {
		super(instance, relationName, attributes);
	}
	
	/**
	 * Constructor.
	 * @param dictionary
	 * @param IDF
	 */
	public TextSVMTrainingSet(Dictionary<List<String>> dictionary, double[] IDF) {
		this.dictionary = dictionary;
		this.IDF = IDF;
	}
	
	/**
	 * Constructor.
	 * @param trainingSetFile
	 * @param relationName
	 * @param description
	 * @param dictionary
	 * @param IDF
	 */
	public TextSVMTrainingSet(SampleSet trainingInstances, String relationName, DatasetDescription description, Dictionary<List<String>> dictionary, double[] IDF, List<Double> minimums, List<Double> ranges, FastVector attributes) {
		this(dictionary, IDF);
		this.relationName = relationName;
		this.attributes = attributes;
		
		System.out.println("Computing " + trainingInstances.getNumInstances() + " instances.");
		System.out.println("- Feature vectors computation");
		buildTrainingSet(trainingInstances, relationName, description, attributes);
		System.out.println("- Normalization");
		normalizeSet(minimums, ranges, dictionary.getDictionaryLength());
	}
	
	/**
	 * Normalize the set by columns
	 * @param minimums
	 * @param ranges
	 */
	private void normalizeSet(List<Double> minimums, List<Double> ranges, int numTerms) {
		if (minimums == null)  {
			this.minimums = new ArrayList<Double>(Collections.nCopies(numTerms, 1000.0));
			this.ranges = new ArrayList<Double>(Collections.nCopies(numTerms, 1000.0));
			computeFeatureVectorRanges(numTerms);
		}
		else {
			this.minimums = minimums;
			this.ranges = ranges;
		}
		
		for (int i = 0; i < data.numInstances(); i++)
			InstanceNormalizer.normalizeInstance(data.instance(i), this.minimums, this.ranges, numTerms);
	}
	
	/**
	 * Compute the feature vector ranges
	 * @param minimums
	 * @param ranges
	 */
	private void computeFeatureVectorRanges(int numTerms) {
		double[] maximums = new double[numTerms];
		
		Arrays.fill(maximums, -1);
		
		for (int i = 0; i < data.numInstances(); i++)
			for (int j = 0; j < numTerms; j++){
				if (data.instance(i).value(j) < minimums.get(j))
					minimums.set(j, data.instance(i).value(j));
				if (data.instance(i).value(j) > maximums[j])
					maximums[j] = data.instance(i).value(j);
			}
		
		for (int i = 0; i < numTerms; i++)
			ranges.set(i, maximums[i] - minimums.get(i));
	}
	
	@Override
	protected Instance computeInstance(List<String> words, String classId, FastVector attributes) {
		FeatureVector featureVector = new FeatureVector.Builder(dictionary, attributes, IDF)
															.featureVector()
															.components(words)
															.build();
		
		Instance newInstance = featureVector.getInstance();
		newInstance.setValue((Attribute)attributes.elementAt(featureVector.getLength()), classId);
		
		return newInstance;
	}
	
	public List<Double> getMinimums() {
		return minimums;
	}
	
	public List<Double> getRanges() {
		return ranges;
	}
	
}
