package it.polimi.tweetcrawlingpipeline.classifier.objects.trainingsets;

import java.util.List;

import org.opencv.core.Mat;

import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;
import it.polimi.tweetcrawlingpipeline.classifier.objects.DatasetDescription;
import it.polimi.tweetcrawlingpipeline.classifier.objects.annotatedinstances.SampleSet;
import it.polimi.tweetcrawlingpipeline.classifier.objects.dictionaries.Dictionary;
import it.polimi.tweetcrawlingpipeline.classifier.objects.dictionaries.VisualDictionary;
import it.polimi.tweetcrawlingpipeline.classifier.visualobjects.Histogram;
import it.polimi.tweetcrawlingpipeline.classifier.visualobjects.Image;
import it.polimi.tweetcrawlingpipeline.utils.Config;

public class VisualSVMTrainingSet extends TrainingSet {
	
	private int minDim = 480;
	private int maxDim = 640;
	private int numNeighbors = 3;
	
	private Dictionary<List<List<Double>>> dictionary;

	private static final long serialVersionUID = 1L;
	
	public VisualSVMTrainingSet(Instance instance, String relationName,
			FastVector attributes) {
		super(instance, relationName, attributes);
	}

	public VisualSVMTrainingSet(SampleSet trainingSet, String relation, DatasetDescription description, Dictionary<List<List<Double>>> dictionary, FastVector attributes) {
		this.dictionary = dictionary;
		this.data = createDataset(trainingSet, relation, description, attributes);
		this.relationName = relation;
		this.attributes = attributes;
	}

	@Override
	protected Instances createDataset(SampleSet trainingInstances, String relationName, DatasetDescription description, FastVector attributes) {
		Instances data = new Instances(relationName, attributes, 0);
		
		Mat matDictionary = ((VisualDictionary)dictionary).getMatDictionary();
		
		System.out.println("[DESCRIPTORS] Computing descriptors for " + trainingInstances.getNumInstances() + " images.");
		for (int i = 0; i < trainingInstances.getNumInstances(); i++) {
			printPercentage(i, trainingInstances.getNumInstances());
			
			String sample = trainingInstances.getInstance(i);
			String[] separated = sample.split(Config.textClassSeparator);
			String path = separated[0];
			String annotation = separated[1];
			
			Image image = new Image(Config.trainingImagesFolder + path);
			Histogram histogram = image.computeHistogram(matDictionary, minDim, maxDim, numNeighbors, attributes, annotation);
			
			data.add(histogram.getFeatureVector());
		}
		
		System.out.println("[DESCRIPTORS] Finished.");
		
		data.setClassIndex(data.numAttributes() - 1);
		
		return data;
	}
	
}
