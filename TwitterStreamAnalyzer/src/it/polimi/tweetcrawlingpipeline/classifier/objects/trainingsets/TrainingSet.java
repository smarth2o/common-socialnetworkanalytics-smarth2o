package it.polimi.tweetcrawlingpipeline.classifier.objects.trainingsets;

import it.polimi.tweetcrawlingpipeline.classifier.objects.DatasetDescription;
import it.polimi.tweetcrawlingpipeline.classifier.objects.annotatedinstances.SampleSet;

import java.io.Serializable;

import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;

public abstract class TrainingSet implements Serializable {

	private static final long serialVersionUID = 1L;
	protected Instances		  data;
	protected String		  relationName;
	protected FastVector	  attributes;
	
	public TrainingSet() {}
	
	public TrainingSet(Instance instance, String relationName, FastVector attributes) {
		this.data = new Instances(relationName, attributes, 0);
		this.data.setClassIndex(data.numAttributes() - 1);
		
		this.data.add(instance);
		this.relationName = relationName;
		this.attributes = attributes;
	}
	
	protected abstract Instances createDataset(SampleSet trainingInstances, String relationName, DatasetDescription description, FastVector attributes);

	/**
	 * Returns the training set instances
	 * @return trainingSetInstances
	 */
	public Instances getTrainingSetInstances() {
		return data;
	}
	
	public String getRelationName() {
		return relationName;
	}
	
	public FastVector getAttributes() {
		return attributes;
	}
	
	/**
	 * Returns the specified instance
	 * @param index
	 * @return instance
	 */
	public Instance getInstance(int index) {
		return data.instance(index);
	}
	
	protected void printPercentage(int index, int numInstances) {
		double[] percentages = {0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1};
		for (int i = 0; i < percentages.length; i++) {
			double percentage = Math.round(numInstances*percentages[i]);
			if (index == percentage) {
				System.out.println(" * Percentage: " + percentages[i]);
				return;
			}
		}
	}
}
