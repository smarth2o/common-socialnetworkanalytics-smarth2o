package it.polimi.tweetcrawlingpipeline.classifier.objects.trainingsets;

import java.io.File;
import java.io.IOException;
import java.util.List;

import it.polimi.tweetcrawlingpipeline.classifier.objects.DatasetDescription;
import it.polimi.tweetcrawlingpipeline.classifier.objects.annotatedinstances.SampleSet;
import it.polimi.tweetcrawlingpipeline.classifier.objects.annotatedinstances.TextualSampleSet;
import it.polimi.tweetcrawlingpipeline.classifier.utils.TextProcessor;
import it.polimi.tweetcrawlingpipeline.utils.Config;

import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ArffSaver;

public abstract class TextualTrainingSet extends TrainingSet {
	
	public TextualTrainingSet() {}

	public TextualTrainingSet(Instance instance, String relationName,
			FastVector attributes) {
		super(instance, relationName, attributes);
	}

	private static final long serialVersionUID = 3243457786900502398L;
	
	private static boolean	  SAVE_FILE = false;
	
	/**
	 * Saves the training set in an ARFF file
	 * @param trainingInstances
	 * @param relationName
	 * @param description
	 */
	protected void buildTrainingSet(SampleSet trainingInstances, String relationName, DatasetDescription description, FastVector attributes) {
		data = createDataset(trainingInstances, relationName, description, attributes);
		
		if (SAVE_FILE) 
			saveDatasetFile();
	}
	
	/**
	 * Converts a single document in an instance
	 * @param document
	 * @param relationName
	 * @param description
	 */
	protected void buildTrainingSet(List<String> words, String relationName, DatasetDescription description, FastVector attributes) {
		data = new Instances(relationName, attributes, 0);
		data.add(computeInstance(words, description.getClass(0), attributes));
	}
	
	/**
	 * Save dataset ARFF file
	 */
	private void saveDatasetFile() {
		try {
			ArffSaver saver = new ArffSaver();
			saver.setInstances(data);
			saver.setFile(new File(Config.ARFFFile));
			saver.writeBatch();
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Create the training set instances
	 * @param trainingSetFile
	 * @param relationName
	 * @param className
	 * @return trainingSet
	 * @throws IOException
	 */
	@Override
	protected Instances createDataset(SampleSet trainingInstances, String relationName, DatasetDescription description, FastVector attributes){
		Instances data = new Instances(relationName, attributes, 0);
		
		TextProcessor textProcessor = new TextProcessor();
		
		for (int i = 0; i < trainingInstances.getNumInstances(); i++) {
			printPercentage(i, trainingInstances.getNumInstances());
			String line = trainingInstances.getInstance(i);
			
			line = line.replace("\"", "");
			String[] separated = line.split(Config.textClassSeparator);
			if (separated.length == 0)
				continue;
			
			List<String> words = textProcessor.subdivideDocumentInWords(separated[((TextualSampleSet)trainingInstances).getTextIndex()]);
			words = textProcessor.stem(words);
			String classId = separated[((TextualSampleSet)trainingInstances).getClassIndex()];
			
			data.add(computeInstance(words,classId, attributes));
		}
		
		data.setClassIndex(data.numAttributes() - 1);
		
		return data;
	}
	
	/**
	 * Computes a new instance for the training set
	 * @param filteredText
	 * @param classId
	 * @return instance
	 */
	protected abstract Instance computeInstance(List<String> words, String classId, FastVector attributes);
}
