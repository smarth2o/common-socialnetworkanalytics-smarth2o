package it.polimi.tweetcrawlingpipeline.classifier.objects.trainingsets;

import it.polimi.tweetcrawlingpipeline.classifier.objects.dictionaries.Dictionary;
import it.polimi.tweetcrawlingpipeline.classifier.objects.dictionaries.TextDictionary;
import it.polimi.tweetcrawlingpipeline.classifier.utils.InstanceNormalizer;

import java.io.Serializable;
import java.util.List;

import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;

public class FeatureVector implements Serializable {

	private static final long serialVersionUID = -8561116473530333279L;
	private Instance featureVector;
	
	public static class Builder {
		private Instance 					featureVector;
		private Dictionary<List<String>> 	dictionary;
		private FastVector 					attributes;
		private double[]					IDF;
		
		public Builder(Dictionary<List<String>> dictionary, FastVector attributes, double[] IDF) {
			this.dictionary = dictionary;
			this.attributes = attributes;
			this.IDF = IDF;
		}
		
		public Builder featureVector() {
			featureVector = new Instance(dictionary.getDictionaryLength() + 1);
			for (int i = 0; i < featureVector.numValues(); i++)
				featureVector.setValue((Attribute)attributes.elementAt(i), 0);
			
			return this;
		}
		
		public Builder featureVector(Instance emptyInstance) {
			featureVector = (Instance) emptyInstance.copy();
			return this;
		}
		
		public Builder components(List<String> words) {
			for (String word:words) 
				if (!word.isEmpty()) {
					int index = ((TextDictionary)dictionary).findTerm(word);
					if (index != -1)
						featureVector.setValue((Attribute)attributes.elementAt(index),featureVector.value(index)+IDF[index]);
				}
			
			return this;
		}
		
		public Builder normalize(List<Double> minimums, List<Double> ranges) {
			InstanceNormalizer.normalizeInstance(featureVector, minimums, ranges, dictionary.getDictionaryLength());			
			return this;
		}
		
		public FeatureVector build() {
			return new FeatureVector(this.featureVector);
		}
	}
	
	public FeatureVector(Instance featureVector) {
		this.featureVector = featureVector;
	}
	
	/**
	 * Returns the number of features
	 * @return numFeatures
	 */
	public int getLength() {
		return featureVector.numValues()-1;
	}
	
	public Instance getInstance() {
		return featureVector;
	}
}
