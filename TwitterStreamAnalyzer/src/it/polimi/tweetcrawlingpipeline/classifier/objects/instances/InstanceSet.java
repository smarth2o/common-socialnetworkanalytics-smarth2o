package it.polimi.tweetcrawlingpipeline.classifier.objects.instances;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class InstanceSet implements Serializable {

	private static final long serialVersionUID = -3998696816889558828L;
	
	private List<String> instances;
	private int textIndex;
	private int classIndex;
	
	public InstanceSet(int textIndex, int classIndex) {
		this.instances = new ArrayList<String>();
		this.textIndex = textIndex;
		this.classIndex = classIndex;
	}
	
	/**
	 * Add new instance
	 * @param instance
	 */
	public void add(String instance) {
		instances.add(instance);
	}
	
	/**
	 * Returns the number of instances in the set
	 * @return numInstances
	 */
	public int getNumInstances() {
		return instances.size();
	}
	
	/**
	 * Returns the specified instance
	 * @param index
	 * @return instance
	 */
	public String getInstance(int index) {
		return instances.get(index);
	}
	
	/**
	 * Returns the text index in the CSV file (indicating the document content)
	 * @return textIndex
	 */
	public int getTextIndex() {
		return textIndex;
	}
	
	/**
	 * Returns the class index in the CSV file (indicating the document label)
	 * @return classIndex
	 */
	public int getClassIndex() {
		return classIndex;
	}
}
