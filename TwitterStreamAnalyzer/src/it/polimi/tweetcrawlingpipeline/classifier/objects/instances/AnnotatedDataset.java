package it.polimi.tweetcrawlingpipeline.classifier.objects.instances;


public class AnnotatedDataset {
	protected InstanceSet	trainingInstances;
	protected InstanceSet	crossValidationInstances;
	protected InstanceSet	testInstances;
	
	public AnnotatedDataset(InstanceSet trainingInstances, InstanceSet crossValidationInstances, InstanceSet testInstances) {
		this.trainingInstances = trainingInstances;
		this.crossValidationInstances = crossValidationInstances;
		this.testInstances = testInstances;
	}
	
	public InstanceSet getTrainingInstances() {
		return trainingInstances;
	}
	
	public InstanceSet getCrossValidationInstances() {
		return crossValidationInstances;
	}
	
	public InstanceSet getTestInstances() {
		return testInstances;
	}
}
