package it.polimi.tweetcrawlingpipeline.classifier.objects.annotatedinstances;

public class TextualSampleSet extends SampleSet {

	private static final long serialVersionUID = -3998696816889558828L;

	private int textIndex;
	private int classIndex;
	
	public TextualSampleSet(int textIndex, int classIndex) {
		super();
		this.textIndex = textIndex;
		this.classIndex = classIndex;
	}
	
	/**
	 * Returns the text index in the CSV file (indicating the document content)
	 * @return textIndex
	 */
	public int getTextIndex() {
		return textIndex;
	}
	
	/**
	 * Returns the class index in the CSV file (indicating the document label)
	 * @return classIndex
	 */
	public int getClassIndex() {
		return classIndex;
	}
}
