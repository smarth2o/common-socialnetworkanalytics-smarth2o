package it.polimi.tweetcrawlingpipeline.classifier.objects.annotatedinstances;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SampleSet implements Serializable {

	protected List<String> instances;
	
	private static final long serialVersionUID = 1L;
	
	public SampleSet() {
		this.instances = new ArrayList<String>();
	}
	
	/**
	 * Add new instance
	 * @param instance
	 */
	public void add(String instance) {
		instances.add(instance);
	}
	
	/**
	 * Returns the number of instances in the set
	 * @return numInstances
	 */
	public int getNumInstances() {
		return instances.size();
	}
	
	/**
	 * Returns the specified instance
	 * @param index
	 * @return instance
	 */
	public String getInstance(int index) {
		return instances.get(index);
	}

}
