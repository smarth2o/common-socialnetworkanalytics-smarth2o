package it.polimi.tweetcrawlingpipeline.classifier.objects.annotateddatasets;

import it.polimi.tweetcrawlingpipeline.classifier.objects.annotatedinstances.SampleSet;
import it.polimi.tweetcrawlingpipeline.classifier.objects.annotatedinstances.TextualSampleSet;
import it.polimi.tweetcrawlingpipeline.utils.Config;
import it.polimi.tweetcrawlingpipeline.utils.Utils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class TextualAnnotatedDataset extends AnnotatedDataset {
	
	public TextualAnnotatedDataset() {
		List<String> lines = new ArrayList<String>();
		
		try {
			BufferedReader br = new BufferedReader(new FileReader(Config.annotatedInstancesFile));
			String line = br.readLine();
			line = line.replace("\"", "");
			
			String[] separated = line.split(Config.textClassSeparator);
			int classIndex = Utils.findPosition(separated, Config.CSVKeyRelevance);
			int textIndex = Utils.findPosition(separated, Config.CSVKeyText);
			
			while ((line = br.readLine()) != null) 
				lines.add(line);
			
			br.close();
			
			int N = lines.size();
			int trainingSetIdx = (int) (N * 0.6);
			int CVSetIdx = (int) (N * 0.8);
			
			trainingInstances = new TextualSampleSet(textIndex, classIndex);
			crossValidationInstances = new TextualSampleSet(textIndex, classIndex);
			testInstances = new TextualSampleSet(textIndex, classIndex);
			
			addInstancesToSet(trainingInstances, lines, 0, trainingSetIdx);
			addInstancesToSet(crossValidationInstances, lines, trainingSetIdx+1, CVSetIdx);
			addInstancesToSet(testInstances, lines, CVSetIdx+1, lines.size());
			
			System.out.println("Number of samples:");
			System.out.println("- Training set: " + trainingInstances.getNumInstances());
			System.out.println("- CV set: " + crossValidationInstances.getNumInstances());
			System.out.println("- Test set: " + testInstances.getNumInstances());			
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Adds new instances to either training, CV or test set
	 * @param set
	 * @param instances
	 * @param start
	 * @param end
	 */
	private void addInstancesToSet(SampleSet set, List<String> instances, int start, int end) {
		for (int i = start; i < end; i++)
			set.add(instances.get(i));
	}
}
