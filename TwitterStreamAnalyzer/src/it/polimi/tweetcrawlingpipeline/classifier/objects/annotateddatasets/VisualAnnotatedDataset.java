package it.polimi.tweetcrawlingpipeline.classifier.objects.annotateddatasets;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import it.polimi.tweetcrawlingpipeline.classifier.objects.annotatedinstances.SampleSet;
import it.polimi.tweetcrawlingpipeline.utils.Config;

public class VisualAnnotatedDataset extends AnnotatedDataset {
	
	static final String[] EXTENSIONS = new String[]{
		"gif", "png", "bmp", "jpeg", "jpg"
	};
	
	public VisualAnnotatedDataset() {		
		trainingInstances = new SampleSet();
		testInstances = new SampleSet();
		crossValidationInstances = new SampleSet();
		
		List<String> lines = new ArrayList<String>();
		
		try {
			BufferedReader br = new BufferedReader(new FileReader(Config.annotatedImagesFile));
			String line = br.readLine();
			
			while ((line = br.readLine()) != null) 
				lines.add(line);
			br.close();
		} 
		catch (FileNotFoundException e) {
			e.printStackTrace();
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
				
		int maxNumTrainingSamples = (int) Math.floor(lines.size()*0.6);
		int maxNumCVSamples = (int) Math.floor(lines.size()*0.8);
		Collections.shuffle(lines, new Random(2));
		
		for (int i = 0; i < maxNumTrainingSamples; i++)
			trainingInstances.add(lines.get(i));
		
		for (int i = maxNumTrainingSamples; i < maxNumCVSamples; i++)
			crossValidationInstances.add(lines.get(i));
		
		for (int i = maxNumCVSamples; i < lines.size(); i++)
			testInstances.add(lines.get(i));
		
		System.out.println("Population:");
		System.out.println("- Training set: " + trainingInstances.getNumInstances());
		System.out.println("- Test set: " + testInstances.getNumInstances());
	}
}
