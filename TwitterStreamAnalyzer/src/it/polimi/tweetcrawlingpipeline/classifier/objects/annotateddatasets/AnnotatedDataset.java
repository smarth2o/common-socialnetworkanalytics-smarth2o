package it.polimi.tweetcrawlingpipeline.classifier.objects.annotateddatasets;

import it.polimi.tweetcrawlingpipeline.classifier.objects.annotatedinstances.SampleSet;

public class AnnotatedDataset {
	protected SampleSet	trainingInstances;
	protected SampleSet	crossValidationInstances;
	protected SampleSet	testInstances;
	
	public SampleSet getTrainingInstances() {
		return trainingInstances;
	}
	
	public SampleSet getCrossValidationInstances() {
		return crossValidationInstances;
	}
	
	public SampleSet getTestInstances() {
		return testInstances;
	}
}
