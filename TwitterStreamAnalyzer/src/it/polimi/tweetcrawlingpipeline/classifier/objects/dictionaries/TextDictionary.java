package it.polimi.tweetcrawlingpipeline.classifier.objects.dictionaries;

import it.polimi.tweetcrawlingpipeline.utils.Utils;

import java.io.File;
import java.util.List;

public class TextDictionary extends Dictionary<List<String>> {

	private static final long serialVersionUID = 6864983436604038250L;
	
	/**
	 * Constructor
	 * @param file
	 */
	public TextDictionary(File file) {
		dictionary = Utils.readFileLines(file);
	}
	
	/**
	 * Constructor
	 * @param fileName
	 */
	public TextDictionary(String fileName) {
		this(new File(fileName));
	}
	
	@Override
	public int getDictionaryLength() {
		return dictionary.size();
	}
	
	/**
	 * Returns the specified term
	 * @param index
	 * @return term
	 */
	public String getTerm(int index) {
		return dictionary.get(index);
	}
	
	/**
	 * Finds the term position in the dictionary
	 * @param term
	 * @return index
	 */
	public int findTerm(String term) {		
		for (int i = 0; i < dictionary.size(); i++)
			if (dictionary.get(i).equals(term)) {
				return i;
			}
		
		return -1;
	}
}
