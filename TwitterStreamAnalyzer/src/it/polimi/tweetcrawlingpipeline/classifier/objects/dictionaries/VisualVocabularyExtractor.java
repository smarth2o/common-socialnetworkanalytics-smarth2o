package it.polimi.tweetcrawlingpipeline.classifier.objects.dictionaries;

import it.polimi.tweetcrawlingpipeline.classifier.objects.DatasetDescription;
import it.polimi.tweetcrawlingpipeline.classifier.visualobjects.Image;
import it.polimi.tweetcrawlingpipeline.utils.Config;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.TermCriteria;

public class VisualVocabularyExtractor {
	
	private String 	dictionaryFileName;
	private String	annotationFileName;
	private DatasetDescription description;
	private int		numSamplesForEachClass = 50;
	private int		numWords = 5000;
	
	private int minDim = 480;
	private int maxDim = 640;
	
	public VisualVocabularyExtractor(String dictionaryFileName, String annotationFileName, DatasetDescription description) {
		this.dictionaryFileName = dictionaryFileName;
		this.annotationFileName = annotationFileName;
		this.description = description;
	}
	
	public void extractVocabulary() {
		List<String> samples = extractPositiveAndNegativeSamples();
		System.out.println("[VISUAL VOCABULARY] Extracting descriptors...");
		Mat candidates = extractCandidates(samples);
		System.out.println("[VISUAL VOCABULARY] Number of candidates: " + candidates.height());
		
		System.out.println("[VISUAL VOCABULARY] Clustering...");
		Mat centers = new Mat();
		Mat labels = new Mat();
		TermCriteria termCriteria = new TermCriteria(TermCriteria.COUNT,100,1);
		Core.kmeans(candidates, numWords, labels, termCriteria, 1, Core.KMEANS_PP_CENTERS, centers);
		System.out.println("[VISUAL VOCABULARY] Number of features: " + centers.width());
		System.out.println("[VISUAL VOCABULARY] Number of centers: " + centers.height());
		
		saveVocabularyOnFile(centers);
	}
	
	private void saveVocabularyOnFile(Mat centers) {
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(new File(dictionaryFileName)));
			for (int i = 0; i < centers.height(); i++) {
				for (int j = 0; j < centers.width(); j++) {
					double[] value = centers.get(i, j);
					writer.write(new Double(value[0]).toString() + " ");
				}
				writer.write("\n");
			}
			
			writer.close();
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private List<String> extractPositiveAndNegativeSamples() {
		List<String> positiveSamples = new ArrayList<String>();
		List<String> negativeSamples = new ArrayList<String>();
		
		try {
			BufferedReader br = new BufferedReader(new FileReader(annotationFileName));
			String line = br.readLine();
			
			while ((line = br.readLine()) != null) {
				String[] separated = line.split(Config.textClassSeparator);
				if (separated[1].equals(description.getPositiveClass()) && positiveSamples.size() < numSamplesForEachClass)
					positiveSamples.add(separated[0]);
				else if (negativeSamples.size() < numSamplesForEachClass)
					negativeSamples.add(separated[0]);
				
				if (positiveSamples.size() == numSamplesForEachClass && negativeSamples.size() == numSamplesForEachClass)
					break;
			}
			br.close();
		} 
		catch (FileNotFoundException e) {
			e.printStackTrace();
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
		
		List<String> samples = positiveSamples;
		samples.addAll(negativeSamples);
		
		return samples;
	}
	
	private Mat extractCandidates(List<String> samples) {
		Mat descriptors = new Mat();
		
		for (int i = 0; i < samples.size(); i++) {
			Image image = new Image(Config.trainingImagesFolder + samples.get(i));
			Mat sampleDescriptors = image.computeDescriptors(minDim, maxDim);
			
			descriptors.push_back(sampleDescriptors);
		}
		
		return descriptors;
	}

}
