package it.polimi.tweetcrawlingpipeline.classifier.objects.dictionaries;

import java.io.Serializable;

public abstract class Dictionary<T> implements Serializable {

	private static final long serialVersionUID = 1L;
	protected T dictionary;
	
	/**
	 * Returns the dictionary
	 * @return dictionary
	 */
	public T getDictionary() {
		return dictionary;
	}
	
	/**
	 * Returns the dictionary length
	 * @return dictionaryLength
	 */
	public abstract int getDictionaryLength();

}
