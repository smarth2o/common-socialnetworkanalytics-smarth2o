package it.polimi.tweetcrawlingpipeline.classifier.objects.dictionaries;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.opencv.core.CvType;
import org.opencv.core.Mat;

public class VisualDictionary extends Dictionary<List<List<Double>>> {

	private static final long 	serialVersionUID = 1L;
	private Mat 				dictionaryInMat;

	public VisualDictionary(String path) {
		dictionary = new ArrayList<List<Double>>();
		
		try {
			BufferedReader reader = new BufferedReader(new FileReader(path));
			
			String line = null;
			while ((line = reader.readLine()) != null)
				dictionary.add(computeDescriptor(line));
			
			reader.close();
			
			System.out.println("[VISUAL VOCABULARY] Loaded " + dictionary.size() 
							+ " descriptors (num features: " + dictionary.get(0).size() + ").");
		} 
		catch (FileNotFoundException e) {
			e.printStackTrace();
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
		
		convertDictionary();
	}
	
	private List<Double> computeDescriptor(String line) {
		List<Double> descriptor = new ArrayList<Double>();
		String[] descriptorValues = line.split(" ");
		
		for (int i = 0; i < descriptorValues.length; i++)
			descriptor.add(Double.parseDouble(descriptorValues[i]));
		
		return descriptor;
	}

	@Override
	public int getDictionaryLength() {
		return dictionary.size();
	}
	
	public List<Double> getDescriptor(int i) {
		return dictionary.get(i);
	}
	
	public void convertDictionary() {
		dictionaryInMat = new Mat();
		
		for (int i = 0; i < dictionary.size(); i++)
			dictionaryInMat.push_back(computeMatDescriptor(getDescriptor(i)));
	}
	
	private Mat computeMatDescriptor(List<Double> list) {
		Mat descriptor = new Mat(1, 128, CvType.CV_32F);
		for (int i = 0; i < list.size(); i++)
			descriptor.put(0, i, list.get(i));
		return descriptor;
	}
	
	public Mat getMatDictionary() {
		return dictionaryInMat;
	}
}
