package it.polimi.tweetcrawlingpipeline.classifier.objects.samples;

import it.polimi.tweetcrawlingpipeline.classifier.visualobjects.Image;

public class ImageSample extends SVMSample{
	protected Image sample;	
	
	public ImageSample(String fileName){
		this.sample = new Image(fileName);
	}
	
	public Image getSample() {
		return sample;
	}
}
