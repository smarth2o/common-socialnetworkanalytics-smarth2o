package it.polimi.tweetcrawlingpipeline.classifier.objects.samples;

import java.util.List;

public class TextSample extends SVMSample{
	protected List<String> sample;
	
	public TextSample(List<String> sample){
		this.sample = sample;
	}
	
	public List<String> getSample() {
		return sample;
	}
}
