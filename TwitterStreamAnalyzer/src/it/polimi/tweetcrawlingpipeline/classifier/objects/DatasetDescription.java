package it.polimi.tweetcrawlingpipeline.classifier.objects;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class DatasetDescription implements Serializable {

	private static final long serialVersionUID = -3768404404123989483L;
	
	private List<String> classes;
	private int			 positiveClassId;
	private int			 negativeClassId;
	
	/**
	 * Constructor
	 */
	public DatasetDescription() {
		this.classes = new ArrayList<String>();
	}
	
	/**
	 * Constructor
	 * @param classes
	 */
	public DatasetDescription(List<String> classes) {
		this.classes = classes;
	}

	/**
	 * Return the dataset class labels
	 * @return classes
	 */
	public List<String> getClasses() {
		return classes;
	}
	
	/**
	 * Returns the number of classes the text can be classified into
	 * @return numClasses
	 */
	public int getNumClasses() {
		return classes.size();
	}
	
	/**
	 * Returns a specific class
	 * @param index
	 * @return class
	 */
	public String getClass(int index) {
		return classes.get(index);
	}

	/**
	 * Sets the set of classes
	 * @param classes
	 */
	public void setClasses(List<String> classes) {
		this.classes = classes;
	}
	
	/**
	 * Adds a new class to the description
	 * @param className
	 */
	public void addClass(String className) {
		classes.add(className);
	}

	/**
	 * Returns the positive class index
	 * @return positiveClassId
	 */
	public int getPositiveClassId() {
		return positiveClassId;
	}
	
	public int getNegativeClassId() {
		return negativeClassId;
	}

	/**
	 * Sets the positive class index
	 * @param positiveClassId
	 */
	public void setPositiveClassId(int positiveClassId) {
		this.positiveClassId = positiveClassId;
	}
	
	public void setNegativeClassId(int negativeClassId) {
		this.negativeClassId = negativeClassId;
	}
	
	public String getPositiveClass() {
		return classes.get(0);
	}
	
}
