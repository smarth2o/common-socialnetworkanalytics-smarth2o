package it.polimi.tweetcrawlingpipeline.classifier.visualobjects;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.opencv.core.Mat;
import org.opencv.core.MatOfDMatch;
import org.opencv.core.MatOfKeyPoint;
import org.opencv.core.Size;
import org.opencv.features2d.DMatch;
import org.opencv.features2d.DescriptorExtractor;
import org.opencv.features2d.DescriptorMatcher;
import org.opencv.features2d.FeatureDetector;
import org.opencv.features2d.Features2d;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;

import weka.core.FastVector;

public class Image {

	private Mat image;
	private MatOfKeyPoint keypoints;
	private Mat descriptors;
	
	public Image() {
		image = new Mat();
		keypoints = new MatOfKeyPoint();
		descriptors = new Mat();
	}
	
	public Image(String fileName) {
		this();
		String path = new File(fileName).getAbsolutePath();
		image = Highgui.imread(path);
	}
	
	public Image(Mat otherImage) {
		this();
		image = new Mat(otherImage.width(), otherImage.height(), otherImage.type());
		otherImage.copyTo(image);
	}
	
	public Image(Mat otherImage, double blurringDegree) {
		this();
		image = new Mat(otherImage.width(), image.height(), image.type());
		Imgproc.GaussianBlur(otherImage, image, new Size(3,3), blurringDegree);
	}
	
	public Histogram computeHistogram(Mat vocabulary, int minDim, int maxDim, int numNeighbors, FastVector attributes, String annotation) {
		normalizeImage(minDim, maxDim);
		extractDescriptors();

		return extractHistogram(vocabulary, numNeighbors, attributes, annotation);
	}
	
	public Mat computeDescriptors(int minDim, int maxDim) {
		normalizeImage(minDim, maxDim);
		extractDescriptors();
		return descriptors;
	}
	
	private void normalizeImage(int minDim, int maxDim) {
		convertToGray(Imgproc.COLOR_RGB2GRAY);
		resize(minDim, maxDim);
	}
	
	private void convertToGray(int conversion) {
		if (image.channels() == 3)
			Imgproc.cvtColor(image, image, conversion);
	}
	
	private void resize(int minDim, int maxDim) {
		if ((Math.max(image.width(), image.height()) == image.width() && image.width() <= maxDim) ||
			(Math.max(image.width(), image.height()) == image.height() && image.height() <= maxDim))
			return;
		
		double ratio = ((double)image.width())/image.height();
		Size size = null;
		if (ratio > 1)
			size = new Size(maxDim, maxDim/ratio);
		else 
			size = new Size(minDim*ratio, minDim);
		Imgproc.resize(image, image, size);
	}
	
	private MatOfKeyPoint detectKeypoints(int type) {
		FeatureDetector featureDetector = FeatureDetector.create(type);
		MatOfKeyPoint keypoints = new MatOfKeyPoint();
		featureDetector.detect(image, keypoints);
		
		return keypoints;
	}
	
	private Mat computeDescriptors(int type, MatOfKeyPoint keypoints) {
		DescriptorExtractor extractor = DescriptorExtractor.create(type);
		Mat descriptors = new Mat();
		extractor.compute(image, keypoints, descriptors);
		
		return descriptors;
	}
	
	private void extractDescriptors() {
		keypoints = detectKeypoints(FeatureDetector.SIFT);
		descriptors = computeDescriptors(DescriptorExtractor.SIFT, keypoints);
	}
	
	private Histogram extractHistogram(Mat vocabulary, int numNeighbors, FastVector attributes, String annotation) {
		List<Double> matchingTerms = new ArrayList<Double>(Collections.nCopies(vocabulary.height(), 0.0));
		int total = 0;
		
		DescriptorMatcher matcher = DescriptorMatcher.create(DescriptorMatcher.BRUTEFORCE);
		
		List<MatOfDMatch> multipleMatches = new ArrayList<MatOfDMatch>();
		matcher.knnMatch(descriptors, vocabulary, multipleMatches, numNeighbors);
		
		for (MatOfDMatch multipleMatch:multipleMatches) {
			List<DMatch> matches = multipleMatch.toList();
			for (DMatch match:matches) {
				matchingTerms.set(match.trainIdx, matchingTerms.get(match.trainIdx) + 1.0);
				total++;
			}
		}
		
		return new Histogram(matchingTerms, total, attributes, annotation);
	}
	
	public void drawKeypoints() {
		Features2d.drawKeypoints(image, keypoints, image);
	}
	
	public void save(String path) {
		Highgui.imwrite(path,image);
	}

}
