package it.polimi.tweetcrawlingpipeline.classifier.visualobjects;

import java.util.List;

import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;

public class Histogram {
	
	private Instance featureVector;

	public Histogram(List<Double> matchingTerms, int total, FastVector attributes, String annotation) {
		featureVector = new Instance(matchingTerms.size() + 1);
		
		for (int i = 0; i < matchingTerms.size();i++)
			featureVector.setValue((Attribute)attributes.elementAt(i), matchingTerms.get(i)/total);
		
		featureVector.setValue((Attribute)attributes.elementAt(matchingTerms.size()), annotation);
	}	
	
	public Instance getFeatureVector(){
		return featureVector;
	}

}
