package it.polimi.tweetcrawlingpipeline.classifier.classificationresults;

import java.util.List;

public class TextClassificationResult extends ClassificationResult {
	private List<String> hashtags;
	private List<String> words;
	
	public TextClassificationResult(int classId, List<String> hashtags, List<String> words) {
		super(classId);
		this.hashtags = hashtags;
		this.words = words;
	}
	
	public List<String> getHashtags() {
		return hashtags;
	}
	
	public List<String> getKeywords() {
		return words;
	}

}
