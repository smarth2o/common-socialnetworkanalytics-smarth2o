package it.polimi.tweetcrawlingpipeline.classifier.classificationresults;

public class ImageClassificationResult extends ClassificationResult {

	public ImageClassificationResult(int classId) {
		super(classId);
	}
}
