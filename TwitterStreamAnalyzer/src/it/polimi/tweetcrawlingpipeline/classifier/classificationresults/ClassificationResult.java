package it.polimi.tweetcrawlingpipeline.classifier.classificationresults;

public abstract class ClassificationResult {
	private int classId;
	
	public ClassificationResult(int classId) {
		this.classId = classId;
	}
	
	public int getClassId() {
		return classId;
	}
}
