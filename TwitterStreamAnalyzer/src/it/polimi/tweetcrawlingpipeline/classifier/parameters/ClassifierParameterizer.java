package it.polimi.tweetcrawlingpipeline.classifier.parameters;

import it.polimi.tweetcrawlingpipeline.classifier.classifiers.SVMClassifier;
import it.polimi.tweetcrawlingpipeline.classifier.classifiers.factories.SVMClassifierFactory;
import it.polimi.tweetcrawlingpipeline.classifier.objects.DatasetDescription;
import it.polimi.tweetcrawlingpipeline.classifier.objects.samples.SVMSample;

public class ClassifierParameterizer<T extends SVMSample, E> {
	
	private SVMClassifier<T, E> classifier;
	private double bestCost;
	private double bestGamma;
	private double bestIncorrectRate;
	
	@SuppressWarnings("unchecked")
	public ClassifierParameterizer(SVMClassifierFactory<T> factory, DatasetDescription description) {
		this.classifier = (SVMClassifier<T, E>) factory.build(description);
		this.bestCost = -1.0;
		this.bestGamma = -1.0;
		this.bestIncorrectRate = Double.POSITIVE_INFINITY;
	}
	
	public void computeParameters() {
		double[] costs = {1};
		double[] gammas = {200,300,700,800};
		
		for (int c = 0; c < costs.length; c++)
			for (int g = 0; g < gammas.length; g++) {
				classifier.setCost(costs[c]);
				classifier.setGamma(gammas[g]);
				
				classifier.train();
				double trainError = classifier.trainingSetTest();
				double cvError = classifier.crossValidationSetTest();
				System.out.println("[ROUND] Cost: " + costs[c] + "; Gamma: " + gammas[g] + "; Training: " + trainError + "; CV: " + cvError);
				if (cvError < bestIncorrectRate) {
					bestGamma = gammas[g];
					bestCost = costs[c];
				}
			}
	}
	
	public double getCost() {
		return bestCost;
	}
	
	public double getGamma() {
		return bestGamma;
	}
}
