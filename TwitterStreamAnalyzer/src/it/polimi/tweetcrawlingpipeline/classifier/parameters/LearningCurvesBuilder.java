package it.polimi.tweetcrawlingpipeline.classifier.parameters;

import it.polimi.tweetcrawlingpipeline.classifier.classifiers.SVMClassifier;
import it.polimi.tweetcrawlingpipeline.classifier.classifiers.factories.SVMClassifierFactory;
import it.polimi.tweetcrawlingpipeline.classifier.objects.DatasetDescription;
import it.polimi.tweetcrawlingpipeline.classifier.objects.samples.SVMSample;

public class LearningCurvesBuilder<T extends SVMSample, E> {
	
	private SVMClassifier<T, E> classifier;

	@SuppressWarnings("unchecked")
	public LearningCurvesBuilder(SVMClassifierFactory<T> factory, DatasetDescription description, double cost, double gamma) {
		this.classifier = (SVMClassifier<T, E>) factory.build(description);
		this.classifier.setCost(cost);
		this.classifier.setGamma(gamma);
	}
	
	public void buildCurves() {
		double[] percentages = {0.2, 0.4, 0.6, 0.8};
		LearningCurve learningCurve = new LearningCurve();
		
		for (int i = 0; i < percentages.length; i++) {
			System.out.println("[LEARNING CURVE] Percentage: " + percentages[i]);
			
			classifier.train(percentages[i]);
			double trainError = classifier.trainingSetTest(percentages[i]);
			double cvError = classifier.crossValidationSetTest(percentages[i]);
			
			learningCurve.addPoint(percentages[i], trainError, cvError);
		}
		
		learningCurve.print();
	}
}
