package it.polimi.tweetcrawlingpipeline.classifier.parameters;

import java.util.ArrayList;
import java.util.List;

public class LearningCurve {

	private List<Double> points;
	private List<Double> trainErrors;
	private List<Double> cvErrors;
	
	public LearningCurve() {
		points = new ArrayList<Double>();
		trainErrors = new ArrayList<Double>();
		cvErrors = new ArrayList<Double>();
	}
	
	public void addPoint(double point, double trainError, double cvError) {
		points.add(point);
		trainErrors.add(trainError);
		cvErrors.add(cvError);
	}	
	
	public void print() {
		for (int i = 0; i < points.size(); i++) 
			System.out.println((points.get(i)*100) + "%: Train error = " + trainErrors.get(i) + "; CV error = " + cvErrors.get(i));
	}
}
