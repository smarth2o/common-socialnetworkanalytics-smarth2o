package it.polimi.tweetcrawlingpipeline.classifier.utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import it.polimi.tweetcrawlingpipeline.classifier.classifiers.GenericClassifier;
import it.polimi.tweetcrawlingpipeline.classifier.objects.samples.Sample;

public class ModelSerializer {
	/**
	 * Serializes a model in the specified file
	 * @param classifier
	 * @param filePath
	 */
	public static <T extends Sample> void serializeModel(GenericClassifier<T> classifier, String filePath) {
		try {
			ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(filePath));
			oos.writeObject(classifier);
			oos.flush();
			oos.close();
		} 
		catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Deserializes the model from the specified file
	 * @param filePath
	 * @return classifier
	 */
	public static <T extends Sample> GenericClassifier<T> deserializeModel(String filePath) {
		try {
			ObjectInputStream ois = new ObjectInputStream(new FileInputStream(filePath));
			@SuppressWarnings("unchecked")
			GenericClassifier<T> classifier = (GenericClassifier<T>) ois.readObject();
			ois.close();
			
			return classifier;
		} 
		catch (FileNotFoundException e) {
			e.printStackTrace();
			return null;
		} 
		catch (IOException e) {
			e.printStackTrace();
			return null;
		} 
		catch (ClassNotFoundException e) {
			e.printStackTrace();
			return null;
		}
	}
}
