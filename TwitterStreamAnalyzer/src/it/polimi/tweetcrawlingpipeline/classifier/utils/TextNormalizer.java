package it.polimi.tweetcrawlingpipeline.classifier.utils;

import it.polimi.tweetcrawlingpipeline.utils.Utils;

public class TextNormalizer {
	/**
	 * Normalizes a tweet text removing URLs, user mentions, hashes
	 * @param text
	 * @return filteredText
	 */
	public static String normalizeText(String text) {
		String filteredText = Utils.removeURLs(text);
		filteredText = Utils.removeUserMentions(filteredText);
		filteredText = filteredText.replace("#", "");
		filteredText = filteredText.toLowerCase();
		
		return filteredText;
	}
}
