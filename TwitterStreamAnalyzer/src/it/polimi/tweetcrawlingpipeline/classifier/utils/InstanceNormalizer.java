package it.polimi.tweetcrawlingpipeline.classifier.utils;

import java.util.List;

import weka.core.Instance;

public class InstanceNormalizer {
	
	/**
	 * Normalizes an instance
	 * @param instance
	 * @param minimums
	 * @param ranges
	 * @param numTerms
	 */
	public static void normalizeInstance(Instance instance, List<Double> minimums, List<Double> ranges, int numTerms) {
		for (int j = 0; j < numTerms; j++)
			if (ranges.get(j) == 0)
				instance.setValue(j, instance.value(j) - minimums.get(j));
			else
				instance.setValue(j, (instance.value(j) - minimums.get(j))/(ranges.get(j)-minimums.get(j)));
	}
}
