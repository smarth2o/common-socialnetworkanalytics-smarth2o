package it.polimi.tweetcrawlingpipeline.classifier.utils;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import weka.core.stemmers.SnowballStemmer;
import it.polimi.tweetcrawlingpipeline.utils.Config;
import it.polimi.tweetcrawlingpipeline.utils.Utils;

public class TextProcessor implements Serializable {
	
	private static final long serialVersionUID = -7374621015538799209L;
	private SnowballStemmer stemmer;
	private List<String> stopwords;
	private List<String> taxonomy;
	
	public TextProcessor() {
		stemmer = new SnowballStemmer();
		System.out.println("Test stemmer: from lighting to " + stemmer.stem("lighting"));
		stopwords = Utils.readFileLines(new File(Config.stopwordsFolder + Config.englishStopwordsFile));
		taxonomy = Utils.readFileLines(new File(Config.taxonomyFile));
	}
	
	public List<String> removeStopwords(List<String> keywords) {
		keywords.removeAll(stopwords);
		return keywords;
	}
	
	public List<String> filterTaxonomy(List<String> keywords) {		
		Iterator<String> iter = keywords.iterator();
		while(iter.hasNext()) {
		    String keyword = iter.next();
		    if (!taxonomy.contains(keyword))
		    	iter.remove();
		}
		
		return keywords;
	}
	
	/**
	 * Normalizes a tweet text removing URLs, user mentions, hashes
	 * @param text
	 * @return filteredText
	 */
	private String normalizeText(String text) {
		String filteredText = Utils.removeURLs(text);
		filteredText = Utils.removeUserMentions(filteredText);
		filteredText = filteredText.replace("#", "");
		filteredText = filteredText.toLowerCase();
		
		return filteredText;
	}
	
	/**
	 * Divides a text in words
	 * @param text
	 * @return words
	 */
	private String[] divideInWords(String text) {
		return text.replaceAll("[^a-zA-Z ]", " ").split("\\s+");
	}
	
	private String[] divideInNumberedWords(String text) {
		return text.replaceAll("[^a-zA-Z0-9 ]", " ").split("\\s+");
	}
	
	/**
	 * Subdivides text in stemmed words
	 * @param text
	 * @return stemmedWords
	 */
	public List<String> subdivideDocumentInWords(String text) {
		text = normalizeText(text);
		String[] words = divideInWords(text);
		
		return new ArrayList<String>(Arrays.asList(words));	
	}
	
	public List<String> subdivideDocumentInNumberedWords(String text) {
		text = normalizeText(text);
		String[] words = divideInNumberedWords(text);
		
		return Arrays.asList(words);	
	}
	
	/**
	 * Stems a set of words
	 * @param words
	 * @return stemmedWords
	 */
	public List<String> stem(List<String> words) {
		List<String> stemmedWords = new ArrayList<String>();
		for (int i = 0; i < words.size(); i++) 
			if (!words.get(i).isEmpty()) 
				stemmedWords.add(stemmer.stem(words.get(i)));
		
		return stemmedWords;
	}
	
	public String stem(String word){
		return stemmer.stem(word);
	}
	
	public String extractRawWords(String text) {
		String rawWords = Utils.removeHashtags(text);
		rawWords = Utils.removeUserMentions(rawWords);
		rawWords = Utils.removeURLs(rawWords);
		
		return rawWords;
	}
}
