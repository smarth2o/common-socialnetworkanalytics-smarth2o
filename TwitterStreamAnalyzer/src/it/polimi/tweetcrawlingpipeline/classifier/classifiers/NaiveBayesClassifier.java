package it.polimi.tweetcrawlingpipeline.classifier.classifiers;

import java.util.List;

import weka.classifiers.Classifier;
import weka.classifiers.bayes.NaiveBayes;
import weka.classifiers.meta.FilteredClassifier;
import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.stemmers.SnowballStemmer;
import weka.filters.unsupervised.attribute.StringToWordVector;
import it.polimi.tweetcrawlingpipeline.classifier.objects.DatasetDescription;
import it.polimi.tweetcrawlingpipeline.classifier.objects.annotateddatasets.TextualAnnotatedDataset;
import it.polimi.tweetcrawlingpipeline.classifier.objects.samples.TextSample;
import it.polimi.tweetcrawlingpipeline.classifier.objects.trainingsets.TextualTrainingSet;
import it.polimi.tweetcrawlingpipeline.classifier.objects.trainingsets.NaiveBayesTrainingSet;
import it.polimi.tweetcrawlingpipeline.utils.Config;
import it.polimi.tweetcrawlingpipeline.utils.Utils;

public class NaiveBayesClassifier extends GenericClassifier<TextSample> {
	
	private static final long serialVersionUID = -5456171442479838910L;
	private FilteredClassifier classifier;
	
	/**
	 * Constructor.
	 * @param trainingSet
	 */
	public NaiveBayesClassifier(DatasetDescription description, String language) {
		TextualAnnotatedDataset annotatedDataset = new TextualAnnotatedDataset();

		initializeAttributes();
		this.trainingSet = new NaiveBayesTrainingSet(annotatedDataset.getTrainingInstances(), Config.trainingSetRelation, description, attributes);
		
		StringToWordVector filter = new StringToWordVector(); 
		filter.setAttributeIndices("first"); 
		
		try {
			String[] options = new String[3];
			options[0] = "-S";
			options[1] = "-stopwords";
			options[2] = "\"" + Utils.getStopwordsListFile(language) + "\"";
			filter.setOptions(options);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		
		SnowballStemmer stemmer = new SnowballStemmer();
		stemmer.setStemmer(Utils.getStemmerName(language));
		filter.setStemmer(stemmer);
		
		this.classifier = new FilteredClassifier(); 
		this.classifier.setFilter(filter); 
		this.classifier.setClassifier(new NaiveBayes());
	}

	@Override
	public int classify(TextSample sample) {	
		List<String> words = sample.getSample();
		
		StringBuilder text = new StringBuilder();
		for (String word:words)
			text.append(word + " ");
		
		Instance instance = new Instance(2);
		instance.setValue((Attribute)trainingSet.getTrainingSetInstances().attribute(0), text.toString());   
		instance.setDataset(trainingSet.getTrainingSetInstances());
		
		try {
			double[] fDistribution = classifier.distributionForInstance(instance);			
			return Utils.findMaxElementPosition(fDistribution);
		} 
		catch (Exception e) {
			e.printStackTrace();
			return -1;
		}
	}

	@Override
	protected Classifier getClassifier() {
		return classifier;
	}
	
	/**
	 * Test the classifier
	 */
	@Override
	public void test() {
		TextualTrainingSet testSet = new NaiveBayesTrainingSet(new TextualAnnotatedDataset().getTestInstances(), Config.trainingSetRelation, description, attributes);
		super.test(testSet);
	}

	@Override
	public void initializeAttributes() {
		attributes = new FastVector(2);
		attributes.addElement(new Attribute("text", (FastVector) null));
		
	    attributes.addElement(addClass(description));

	}

	@Override
	public void shrinkTrainingSet() {
		Instance trainingSetInstance = trainingSet.getInstance(0);
		this.trainingSet = new NaiveBayesTrainingSet(trainingSetInstance, trainingSet.getRelationName(), trainingSet.getAttributes());
	}

	@Override
	public int classify(Instance instance) {
		try {
			double[] fDistribution = classifier.distributionForInstance(instance);			
			return Utils.findMaxElementPosition(fDistribution);
		} 
		catch (Exception e) {
			e.printStackTrace();
			return -1;
		}
	}
}
