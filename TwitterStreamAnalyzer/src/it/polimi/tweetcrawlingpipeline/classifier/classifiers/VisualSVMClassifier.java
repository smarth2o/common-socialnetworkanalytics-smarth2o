package it.polimi.tweetcrawlingpipeline.classifier.classifiers;

import java.util.List;

import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;
import it.polimi.tweetcrawlingpipeline.classifier.objects.DatasetDescription;
import it.polimi.tweetcrawlingpipeline.classifier.objects.annotateddatasets.VisualAnnotatedDataset;
import it.polimi.tweetcrawlingpipeline.classifier.objects.dictionaries.VisualDictionary;
import it.polimi.tweetcrawlingpipeline.classifier.objects.samples.SVMSample;
import it.polimi.tweetcrawlingpipeline.classifier.objects.samples.ImageSample;
import it.polimi.tweetcrawlingpipeline.classifier.objects.trainingsets.VisualSVMTrainingSet;
import it.polimi.tweetcrawlingpipeline.classifier.visualobjects.Histogram;
import it.polimi.tweetcrawlingpipeline.classifier.visualobjects.Image;
import it.polimi.tweetcrawlingpipeline.utils.Config;

public class VisualSVMClassifier extends SVMClassifier<SVMSample,List<List<Double>>> {

	private static final long 	serialVersionUID = 1L;
	private int 				minDim = 480;
	private int					maxDim = 640;
	private int 				numNeighbors = 3;
	
	public VisualSVMClassifier(DatasetDescription description, String dictionaryFileName, double cost, double gamma) {
		super(description, cost, gamma);
		
		setDictionary(dictionaryFileName);
		initializeAttributes();
		
		VisualAnnotatedDataset annotatedDataset = new VisualAnnotatedDataset();
		
		this.trainingSet = new VisualSVMTrainingSet(annotatedDataset.getTrainingInstances(), Config.trainingSetImageRelation, description, dictionary, attributes);
		this.crossValidationSet = new VisualSVMTrainingSet(annotatedDataset.getCrossValidationInstances(), Config.trainingSetImageRelation, description, dictionary, attributes);
	}

	@Override
	public int classify(SVMSample sample) {
		Image image = ((ImageSample)sample).getSample();
		Histogram histogram = image.computeHistogram(((VisualDictionary)dictionary).getMatDictionary(), 
												minDim, maxDim, numNeighbors, attributes, "Y");
		Instance imageInstance = histogram.getFeatureVector();
		imageInstance.setDataset(trainingSet.getTrainingSetInstances());
		
		return classify(imageInstance);
	}

	@Override
	public void test() {
		System.out.println("TEST: training set");
		super.test(trainingSet);
		
		System.out.println("TEST: test set");
		VisualSVMTrainingSet testSet = new VisualSVMTrainingSet(new VisualAnnotatedDataset().getTestInstances(), Config.trainingSetRelation, description, dictionary, attributes);
		super.test(testSet);
	}

	@Override
	public void initializeAttributes() {
		attributes = new FastVector(dictionary.getDictionaryLength() + 1);
		for (int i = 0; i < dictionary.getDictionaryLength(); i++) 
			attributes.addElement(new Attribute("VW-" + new Integer(i).toString()));

	    attributes.addElement(addClass(description));
	}
	
	public void setDictionary(String dictionaryFileName) {
		this.dictionary = new VisualDictionary(dictionaryFileName);
	}
	
	@Override
	public void shrinkTrainingSet() {
		Instance trainingSetInstance = trainingSet.getInstance(0);
		this.trainingSet = new VisualSVMTrainingSet(trainingSetInstance, trainingSet.getRelationName(), trainingSet.getAttributes());
	}
	
	public void convertDictionary() {
		((VisualDictionary)dictionary).convertDictionary();
	}
	
	public void initialize() {
		minDim = 480;
		maxDim = 640;
		numNeighbors = 3;
		new Instances(trainingSet.getRelationName(), attributes, 0);
	}

}
