package it.polimi.tweetcrawlingpipeline.classifier.classifiers;

import it.polimi.tweetcrawlingpipeline.classifier.objects.DatasetDescription;
import it.polimi.tweetcrawlingpipeline.classifier.objects.annotateddatasets.TextualAnnotatedDataset;
import it.polimi.tweetcrawlingpipeline.classifier.objects.dictionaries.TextDictionary;
import it.polimi.tweetcrawlingpipeline.classifier.objects.samples.SVMSample;
import it.polimi.tweetcrawlingpipeline.classifier.objects.samples.TextSample;
import it.polimi.tweetcrawlingpipeline.classifier.objects.trainingsets.FeatureVectorBuilder;
import it.polimi.tweetcrawlingpipeline.classifier.objects.trainingsets.TextSVMTrainingSet;
import it.polimi.tweetcrawlingpipeline.classifier.objects.trainingsets.TextualTrainingSet;
import it.polimi.tweetcrawlingpipeline.utils.Config;
import it.polimi.tweetcrawlingpipeline.utils.Utils;

import java.io.File;
import java.util.List;

import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;

public class TextSVMClassifier extends SVMClassifier<TextSample,List<String>> {

	private static final long serialVersionUID = -2470732985275980235L;
	private FeatureVectorBuilder	featureVectorBuilder;
	protected double[]			IDF;

	/**
	 * Constructor.
	 * @param description
	 * @param language
	 * @param dictionaryFileName
	 * @param IDFfileName
	 * @param cost
	 */
	public TextSVMClassifier(DatasetDescription description, String language, String dictionaryFileName, String IDFfileName, double cost, double gamma) {
		super(description, cost, gamma);
		this.dictionary = new TextDictionary(dictionaryFileName);
		this.IDF = Utils.readNumericContent(new File(IDFfileName));
		
		TextualAnnotatedDataset annotatedDataset = new TextualAnnotatedDataset();
		
		System.out.println(" ");
		System.out.println("Compute training set feature vectors...");
		
		initializeAttributes();
		this.trainingSet = new TextSVMTrainingSet(annotatedDataset.getTrainingInstances(), Config.trainingSetRelation, description, dictionary, IDF, null, null, attributes);
		this.crossValidationSet = new TextSVMTrainingSet(annotatedDataset.getCrossValidationInstances(), Config.trainingSetRelation, description, dictionary, IDF, null, null, attributes);
		
	    this.featureVectorBuilder = new FeatureVectorBuilder(dictionary, attributes, IDF, ((TextSVMTrainingSet)trainingSet).getMinimums(), ((TextSVMTrainingSet)trainingSet).getRanges());
	}
	
	@Override
	public int classify(SVMSample sample) {
		List<String> words = ((TextSample)sample).getSample();
		Instance tweetInstance = featureVectorBuilder.build(words);
		tweetInstance.setDataset(trainingSet.getTrainingSetInstances());
		
		return classify(tweetInstance);
	}
	
	@Override
	public void initializeAttributes() {	
		attributes = new FastVector(dictionary.getDictionaryLength() + 1);
		for (int i = 0; i < dictionary.getDictionaryLength(); i++) 
			attributes.addElement(new Attribute(((TextDictionary)dictionary).getTerm(i)));

	    attributes.addElement(addClass(description));
	}
	
	@Override
	public void test() {
		System.out.println("TEST: training set");
		super.test(trainingSet);
		
		System.out.println("TEST: test set");
		TextualTrainingSet testSet = new TextSVMTrainingSet(new TextualAnnotatedDataset().getTestInstances(), Config.trainingSetRelation, description, dictionary, IDF, ((TextSVMTrainingSet)trainingSet).getMinimums(), ((TextSVMTrainingSet)trainingSet).getRanges(), attributes);
		super.test(testSet);
	}
	
	@Override
	public void shrinkTrainingSet() {
		Instance trainingSetInstance = trainingSet.getInstance(0);
		this.trainingSet = new TextSVMTrainingSet(trainingSetInstance, trainingSet.getRelationName(), trainingSet.getAttributes());
	}
}
