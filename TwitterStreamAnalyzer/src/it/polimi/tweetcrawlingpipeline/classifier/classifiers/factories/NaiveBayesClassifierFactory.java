package it.polimi.tweetcrawlingpipeline.classifier.classifiers.factories;

import it.polimi.tweetcrawlingpipeline.classifier.classifiers.GenericClassifier;
import it.polimi.tweetcrawlingpipeline.classifier.classifiers.NaiveBayesClassifier;
import it.polimi.tweetcrawlingpipeline.classifier.objects.DatasetDescription;
import it.polimi.tweetcrawlingpipeline.classifier.objects.samples.TextSample;

public class NaiveBayesClassifierFactory extends GenericClassifierFactory<TextSample> {
	
	private String language;
	
	public NaiveBayesClassifierFactory(String language) {
		this.language = language;
	}
	
	@Override
	public GenericClassifier<TextSample> build(DatasetDescription description) {
		return new NaiveBayesClassifier(description, language);
	}

}
