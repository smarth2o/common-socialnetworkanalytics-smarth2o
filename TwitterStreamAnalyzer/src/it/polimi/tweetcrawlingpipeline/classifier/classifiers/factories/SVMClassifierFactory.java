package it.polimi.tweetcrawlingpipeline.classifier.classifiers.factories;

import it.polimi.tweetcrawlingpipeline.classifier.objects.samples.SVMSample;

public abstract class SVMClassifierFactory<T extends SVMSample> extends GenericClassifierFactory<SVMSample> {
	
	protected String dictionaryFileName;
	protected double cost;
	protected double gamma;
	
}
