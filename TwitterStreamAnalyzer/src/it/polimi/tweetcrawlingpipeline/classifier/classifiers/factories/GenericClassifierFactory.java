package it.polimi.tweetcrawlingpipeline.classifier.classifiers.factories;

import it.polimi.tweetcrawlingpipeline.classifier.classifiers.GenericClassifier;
import it.polimi.tweetcrawlingpipeline.classifier.objects.DatasetDescription;
import it.polimi.tweetcrawlingpipeline.classifier.objects.samples.Sample;

public abstract class GenericClassifierFactory<T extends Sample> {
	public abstract GenericClassifier<T> build(DatasetDescription description);
}
