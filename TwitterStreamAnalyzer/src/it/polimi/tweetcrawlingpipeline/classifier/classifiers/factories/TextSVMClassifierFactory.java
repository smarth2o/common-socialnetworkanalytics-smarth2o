package it.polimi.tweetcrawlingpipeline.classifier.classifiers.factories;

import it.polimi.tweetcrawlingpipeline.classifier.classifiers.GenericClassifier;
import it.polimi.tweetcrawlingpipeline.classifier.classifiers.TextSVMClassifier;
import it.polimi.tweetcrawlingpipeline.classifier.objects.DatasetDescription;
import it.polimi.tweetcrawlingpipeline.classifier.objects.samples.SVMSample;
import it.polimi.tweetcrawlingpipeline.classifier.objects.samples.TextSample;

public class TextSVMClassifierFactory extends SVMClassifierFactory<TextSample> {
	
	private String IDFfileName;
	private String language;
	
	/**
	 * Constructor.
	 * @param dictionaryFileName
	 * @param IDFfileName
	 */
	public TextSVMClassifierFactory(String language, String dictionaryFileName, String IDFfileName, double cost, double gamma) {
		this.language = language;
		this.dictionaryFileName = dictionaryFileName;
		this.IDFfileName = IDFfileName;
		this.cost = cost;
		this.gamma = gamma;
	}
	
	@Override
	public GenericClassifier<SVMSample> build(DatasetDescription description) {
		return new TextSVMClassifier(description, language, dictionaryFileName, IDFfileName, cost, gamma);
	}
}
