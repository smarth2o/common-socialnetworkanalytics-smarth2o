package it.polimi.tweetcrawlingpipeline.classifier.classifiers.factories;

import it.polimi.tweetcrawlingpipeline.classifier.classifiers.GenericClassifier;
import it.polimi.tweetcrawlingpipeline.classifier.classifiers.VisualSVMClassifier;
import it.polimi.tweetcrawlingpipeline.classifier.objects.DatasetDescription;
import it.polimi.tweetcrawlingpipeline.classifier.objects.samples.ImageSample;
import it.polimi.tweetcrawlingpipeline.classifier.objects.samples.SVMSample;

public class VisualSVMClassifierFactory extends SVMClassifierFactory<ImageSample> {
	
	/**
	 * Constructor.
	 * @param dictionaryFileName
	 * @param IDFfileName
	 */
	public VisualSVMClassifierFactory(String dictionaryFileName, double cost, double gamma) {
		this.dictionaryFileName = dictionaryFileName;
		this.cost = cost;
		this.gamma = gamma;
	}
	
	public VisualSVMClassifierFactory(String dictionaryFileName) {
		this.dictionaryFileName = dictionaryFileName;
		this.cost = -1;
		this.gamma = -1;
	}
	
	@Override
	public GenericClassifier<SVMSample> build(DatasetDescription description) {
		return new VisualSVMClassifier(description, dictionaryFileName, cost, gamma);
	}
}
