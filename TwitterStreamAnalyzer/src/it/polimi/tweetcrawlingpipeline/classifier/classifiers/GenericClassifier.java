package it.polimi.tweetcrawlingpipeline.classifier.classifiers;

import it.polimi.tweetcrawlingpipeline.classifier.objects.DatasetDescription;
import it.polimi.tweetcrawlingpipeline.classifier.objects.samples.Sample;
import it.polimi.tweetcrawlingpipeline.classifier.objects.trainingsets.TrainingSet;

import java.io.Serializable;

import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;

public abstract class GenericClassifier<T extends Sample> implements Serializable {

	private static final long 		serialVersionUID = -7039094928143041489L;
	protected TrainingSet 			trainingSet;
	protected TrainingSet			crossValidationSet;
	protected DatasetDescription 	description;
	
	protected FastVector			attributes;

	/**
	 * Classifies a new instance
	 * @param text
	 * @return classIndex
	 */
	public abstract int classify(T sample);
	
	public abstract int classify(Instance instance);
	
	/**
	 * Trains the classifier given the training set
	 */
	public void train() {
		try {
			getClassifier().buildClassifier(trainingSet.getTrainingSetInstances());
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void train(double percentage) {
		Instances subsetInstances = extractSubsetInstances(trainingSet.getTrainingSetInstances(), percentage);
		
		try {
			getClassifier().buildClassifier(subsetInstances);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private Instances extractSubsetInstances(Instances instances, double percentage) {
		int trainingSetSize = instances.numInstances();
		
		int maxNumTrainingInstances = (int) Math.floor(trainingSetSize*percentage);
		Instances subsetInstances = new Instances(instances, 0, maxNumTrainingInstances);
		
		return subsetInstances;
	}
	
	/**
	 * Tests the classifier
	 */
	public abstract void test();
	
	/**
	 * Tests the classifier after training
	 */
	public double test(TrainingSet testSet) {
		return computePerformance(testSet.getTrainingSetInstances());
	}
	
	public double test(TrainingSet testSet, double percentage) {		
		return computePerformance(extractSubsetInstances(trainingSet.getTrainingSetInstances(), percentage));
	}
	
	public double crossValidationSetTest() {
		return computePerformance(crossValidationSet.getTrainingSetInstances());
	}
	
	public double crossValidationSetTest(double percentage) {		
		return computePerformance(extractSubsetInstances(crossValidationSet.getTrainingSetInstances(), percentage));
	}
	
	public double trainingSetTest() {
		return computePerformance(trainingSet.getTrainingSetInstances());
	}
	
	public double trainingSetTest(double percentage) {		
		return computePerformance(extractSubsetInstances(trainingSet.getTrainingSetInstances(), percentage));
	}
	
	public double computePerformance(Instances instances) {
		try {
			Evaluation eTest = new Evaluation(trainingSet.getTrainingSetInstances());
			eTest.evaluateModel(getClassifier(), instances);
			
			System.out.println(eTest.toSummaryString());
			
			return (eTest.incorrect()/(eTest.incorrect()+eTest.correct()));
		} catch (Exception e) {
			e.printStackTrace();
			return -1.0;
		}
	}
	
	protected abstract Classifier getClassifier();
	
	public abstract void initializeAttributes();
	
	/**
	 * Adds the class to the feature vector
	 * @param description
	 * @return attribute
	 */
	protected Attribute addClass(DatasetDescription description) {
		FastVector classes = new FastVector();
	    for (int i = 0; i < description.getNumClasses(); i++)
	    	classes.addElement(description.getClass(i));
	    return new Attribute("instanceLabel",classes);
	}
	
	
	/**
	 * The training set is needed to classify instances
	 * However, it does not stick in memory, due to the large dimensions
	 * Thus, we shrink it to just one instance, so that it still keep the formatting
	 * while reducing the size
	 */
	public abstract void shrinkTrainingSet();
	
	public int[][] getConfusionMatrix() {
		int[][] confusionMatrix = new int[description.getNumClasses()][description.getNumClasses()];
		Instances subsetInstances = extractSubsetInstances(crossValidationSet.getTrainingSetInstances(), 0.2);
		
		for (int i = 0; i < description.getNumClasses(); i++)
			for (int j = 0; j < description.getNumClasses(); j++)
				confusionMatrix[i][j] = 0;
		
		for (int i = 0; i < subsetInstances.numInstances(); i++) {
			Instance instance = subsetInstances.instance(i);
			int classifiedLabel = this.classify(instance);
			int trueLabel = (int)instance.value(instance.classIndex());
			
			confusionMatrix[trueLabel][classifiedLabel]++;
		}
		
		return confusionMatrix;
	}
}
