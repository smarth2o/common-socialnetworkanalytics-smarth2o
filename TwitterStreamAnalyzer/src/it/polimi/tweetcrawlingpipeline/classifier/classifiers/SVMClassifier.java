package it.polimi.tweetcrawlingpipeline.classifier.classifiers;

import weka.classifiers.Classifier;
import weka.classifiers.functions.LibSVM;
import weka.core.Instance;

import it.polimi.tweetcrawlingpipeline.classifier.objects.DatasetDescription;
import it.polimi.tweetcrawlingpipeline.classifier.objects.dictionaries.Dictionary;
import it.polimi.tweetcrawlingpipeline.classifier.objects.samples.SVMSample;

public abstract class SVMClassifier<T extends SVMSample, E> extends GenericClassifier<SVMSample> {
	
	protected Dictionary<E> 	dictionary;
	protected LibSVM 			classifier;
	private boolean				ESTIMATE_PROBABILITIES = false;

	private static final long 	serialVersionUID = 2089289189295772867L;
	
	public SVMClassifier(DatasetDescription description, double cost, double gamma) {
		this.description = description;
		
	    this.classifier = new LibSVM();
	    this.classifier.setCost(cost);
	    this.classifier.setGamma(gamma);
	    
	    if (ESTIMATE_PROBABILITIES)
	    	this.classifier.setProbabilityEstimates(true);
	}
	
	public void setCost(double cost) {
		classifier.setCost(cost);
	}
	
	public void setGamma(double gamma) {
		classifier.setGamma(gamma);
	}

	@Override
	protected Classifier getClassifier() {
		return classifier;
	}
	
	@Override
	public int classify(Instance instance) {
		try {
			double classId = classifier.classifyInstance(instance);
			return (int) classId;
		} 
		catch (Exception e) {
			e.printStackTrace();
			return -1;
		}
	}

}
