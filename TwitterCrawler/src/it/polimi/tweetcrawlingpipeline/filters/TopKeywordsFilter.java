package it.polimi.tweetcrawlingpipeline.filters;

import java.io.File;
import java.util.List;

import it.polimi.tweetcrawlingpipeline.utils.Config;
import it.polimi.tweetcrawlingpipeline.utils.TextProcessor;
import it.polimi.tweetcrawlingpipeline.utils.Utils;

public class TopKeywordsFilter {
	
	private List<String> allowedStemmedKeywords;
	private TextProcessor processor;
	
	public TopKeywordsFilter() {
		this(Config.cookingTaxonomy);
	}
	
	public TopKeywordsFilter(String taxonomyFile) {
		processor = new TextProcessor();
		
		allowedStemmedKeywords = Utils.readFileLines(new File(taxonomyFile));
		
		for (int i = 0; i < allowedStemmedKeywords.size(); i++)
			allowedStemmedKeywords.set(i, processor.stem(allowedStemmedKeywords.get(i)));
	}
	
	public boolean isRelevant(String word){
		return allowedStemmedKeywords.contains(processor.stem(word));
	}
}
