package it.polimi.tweetcrawlingpipeline.filters;

import it.polimi.tweetcrawlingpipeline.utils.Config;
import it.polimi.tweetcrawlingpipeline.utils.Utils;

import java.io.File;
import java.util.List;

public class StopwordsFilter {
	private List<String> stopwords;
	
	public StopwordsFilter() {
		this(Config.stopwordsFolder + "/" + Config.englishStopwordsFile);
	}
	
	public StopwordsFilter(String stopwordsFile) {
		stopwords = Utils.readFileLines(new File(stopwordsFile));
	}
	
	public boolean isStopword(String word){
		return stopwords.contains(word);
	}
}
