package it.polimi.tweetcrawlingpipeline.db;

import it.polimi.tweetcrawlingpipeline.utils.Config;
import it.polimi.tweetcrawlingpipeline.utils.Tuple;
import it.polimi.tweetcrawlingpipeline.crawler.objects.User;
import it.polimi.tweetcrawlingpipeline.db.objects.CollectionStatistics;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import com.mongodb.AggregationOutput;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.util.JSON;

public class TweetRepository {

	private MongoHandler mongoHandler;

	/**
	 * Constructor.
	 */
	public TweetRepository() {
		List<String> collectionNames = new ArrayList<String>();
		collectionNames.add(Config.rawTweets);

		collectionNames.add(Config.keywordsCollection);
		collectionNames.add(Config.hashtagsCollection);
		collectionNames.add(Config.collectionStatistics);
		collectionNames.add(Config.tweetCollection);
		collectionNames.add(Config.userCollection);
		collectionNames.add(Config.influencersByTweetCountCollection);
		collectionNames.add(Config.influencersByNumFollowersCollection);
		collectionNames.add(Config.influencersByCustomMetricsCollection);

		mongoHandler = new MongoHandler();
		mongoHandler.initializeCollections(collectionNames);
	}

	/**
	 * Saves tweet in the database (as unprocessed tweet)
	 * 
	 * @param tweet
	 */
	public void saveTweet(JSONObject tweet) {
		if (!isTweetInDB(tweet))
			writeTweet(tweet);
	}

	/**
	 * Checks whether the object is in the database
	 * 
	 * @param tweet
	 * @return status
	 */
	public boolean isTweetInDB(JSONObject tweet) {
		try {
			return mongoHandler.isObjectInDatabase(Config.rawTweets, "id", tweet.getString("id_str"));
		} catch (JSONException e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Writes the objects in the database
	 * 
	 * @param tweet
	 */
	private void writeTweet(JSONObject tweet) {
		DBObject tweetObject = (DBObject) JSON.parse(tweet.toString());
		mongoHandler.insert(Config.rawTweets, tweetObject);
	}
	
	/**
	 * Inserts a bunch of tweets in the database
	 * @param tweets
	 */
	public void insert(final List<String> tweets) {
		List<DBObject> objects = new ArrayList<DBObject>();

		for (String tweet : tweets)
			objects.add((DBObject) JSON.parse(tweet));

		mongoHandler.insert(Config.rawTweets, objects);
		System.out.println("[INFO] Wrote " + tweets.size() + " instances in DB.");
	}
	
	/**
	 * Returns tweet in a specified period
	 * @param startDate
	 * @param endDate
	 * @return tweetCursor
	 */
	private DBCursor getTweetsInPeriod(Date startDate, Date endDate) {
		BasicDBObject queryObject = new BasicDBObject("creation_date", new BasicDBObject("$gte", startDate).append("$lte", endDate));
		return mongoHandler.retrieveObjectByQueryObject(Config.tweetCollection, queryObject);
	}
	
	private DBCursor getUserTweetsInPeriod(Long userId, Date startDate, Date endDate) {
		DBObject query = new BasicDBObject("user_id", userId);
		query.put("creation_date", new BasicDBObject("$gte", startDate).append("$lte", endDate));
		return mongoHandler.retrieveObjectByQueryObject(Config.tweetCollection, query);
	}
	
	public int getNumTweetsInPeriod(Long userId, Date startDate, Date endDate) {
		return getUserTweetsInPeriod(userId, startDate, endDate).count();
	}
	
	/**
	 * Returns the users that were collected in a specified period
	 * @param startDate
	 * @param endDate
	 * @return userIDs
	 */
	public List<Long> getUserIdsInPeriod(Date startDate, Date endDate) {
		DBCursor tweets = getTweetsInPeriod(startDate, endDate);

		List<Long> userIDs = new ArrayList<Long>();
		while (tweets.hasNext()) {
			DBObject tweet = tweets.next();
			userIDs.add((Long) tweet.get("user_id"));
		}
		
		return userIDs;
	}
	
	/**
	 * Returns the number of followers for the specified user
	 * @param userId
	 * @return number of followers
	 */
	public int getNumFollowers(Long userId) {
		return getNumEdges(userId, "followers_count");
	}
	
	/**
	 * Returns the number of friends for the specified user
	 * @param userId
	 * @return number of friends
	 */
	public int getNumFriends(Long userId) {
		return getNumEdges(userId, "friends_count");
	}
	
	private int getNumEdges(Long userId, String edgesType) {
		DBObject queryObject = new BasicDBObject("id", userId);
		DBCursor results = mongoHandler.retrieveObjectByQueryObject(Config.userCollection, queryObject);
		if (!results.hasNext())
			return 0;
		else
			return (int) results.next().get(edgesType);
	}
	
	/**
	 * Orders users by number of followers
	 * @param userIDs
	 * @return ordered users
	 */
	public List<User> getOrderedUsersByNumFollowers(Date startDate, Date endDate) {
		List<Long> userIDs = getUserIdsInPeriod(startDate, endDate);
		
		DBObject match = new BasicDBObject("id", new BasicDBObject("$in", userIDs));
		DBObject orderBy = new BasicDBObject("followers_count", -1);
		DBCursor cursor = mongoHandler.retrieveObjectByQueryObject(Config.userCollection, match)
									  .sort(orderBy).limit(Config.numberOfInfluencers);
		
		List<User> users = new ArrayList<User>();
		while (cursor.hasNext()) {
			DBObject retrievedUser = cursor.next();
			
			User user = new User();
			user.setId((long) retrievedUser.get("id"));
			user.setScreenName((String) retrievedUser.get("screen_name"));
			user.setScore((int) retrievedUser.get("followers_count"));
			
			users.add(user);
		}
		
		return users;	
	}
	
	/**
	 * Orders users by tweet count
	 * @param startDate
	 * @param endDate
	 * @return ordered users
	 */
	public List<User> getOrderedUsersByTweetCount(Date startDate, Date endDate) {
		Iterable<DBObject> results = findDistinctUsersByTweetCount(startDate, endDate);
		
		List<User> users = new ArrayList<User>();
		for (DBObject object : results) {
			User user = new User();
			user.setId((Long) object.get("_id"));
			user.setScore((Integer) object.get("tweetCount"));
			
			DBObject queryObject = new BasicDBObject("id", user.getId());
			DBCursor cursor = mongoHandler.retrieveObjectByQueryObject(Config.userCollection, queryObject);
			if (cursor.hasNext()) 
				user.setScreenName((String) cursor.next().get("screen_name"));
			else
				user.setScreenName("");
			
			users.add(user);
		}
		
		return users;
	}
	
	/**
	 * Returns users by tweet count
	 * @param from
	 * @param to
	 * @return users by tweet count
	 */
	public Iterable<DBObject> findDistinctUsersByTweetCount(Date from,Date to){
		DBObject match = new BasicDBObject("$match",new BasicDBObject("creation_date",new BasicDBObject("$gte",from).append("$lte", to)));
		DBObject groupFields = new BasicDBObject("_id","$user_id");
		
		groupFields.put("tweetCount",new BasicDBObject("$sum",1));
		DBObject group = new BasicDBObject("$group",groupFields);
		DBObject fields = new BasicDBObject("_id",1);
		
		fields.put("tweetCount", 1);
		
		DBObject project = new BasicDBObject("$project",fields);
		DBObject sort = new BasicDBObject("$sort",new BasicDBObject("tweetCount",-1));
		DBObject limit = new BasicDBObject("$limit",Config.topK);
		
		List<DBObject> pipeline = Arrays.asList(match, group, project, sort, limit);
		DBCollection coll = mongoHandler.getCollection(Config.tweetCollection);
		AggregationOutput output = coll.aggregate(pipeline);
		
		Iterable<DBObject> mappedResults = output.results();
		return mappedResults;
	}
	
	/**
	 * Returns the screen name of a given user id
	 * @param userId
	 * @return screenName
	 */
	public String getScreenName(long userId) {
		DBObject userDbObject = mongoHandler.retrieveObjectByQueryObject(Config.userCollection, new BasicDBObject("id",userId)).next();
		return (String) userDbObject.get("screen_name");
	}
	
	/**
	 * Returns the number of original tweets for a given user
	 * @param userId
	 * @return number of tweets
	 */
	private DBCursor getOriginalTweetsInPeriod(long userId, Date startDate, Date endDate) {
		DBObject query = new BasicDBObject("user_id", userId);
		query.put("original_author_id", new BasicDBObject("$exists","false"));
		query.put("creation_date", new BasicDBObject("$gte", startDate).append("$lte", endDate));
		return mongoHandler.retrieveObjectByQueryObject(Config.tweetCollection, query);
	}
	
	/**
	 * Returns the number of original tweets by a specific author
	 * @param userId
	 * @param startDate
	 * @param endDate
	 * @return number of tweets
	 */
	public int getNumOriginalTweetsInPeriod(long userId, Date startDate, Date endDate) {
		return getOriginalTweetsInPeriod(userId, startDate, endDate).count();
	}
	
	/**
	 * Returns the number of tweets containing external content
	 * @param userId
	 * @param contentType
	 * @return number of tweets
	 */
	public int getNumTweetsContainingExtraContent(Long userId, Date startDate, Date endDate, String contentType) {
		DBObject query = new BasicDBObject("user_id", userId);
		query.put("original_author_id", new BasicDBObject("$exists","false"));
		query.put(contentType, new BasicDBObject("$not", new BasicDBObject("$size",0)));
		query.put("creation_date", new BasicDBObject("$gte", startDate).append("$lte", endDate));
		return mongoHandler.retrieveObjectByQueryObject(Config.tweetCollection, query).count();
	}
	
	/**
	 * Returns the most recent date in which a user published something
	 * @param userId
	 * @param startDate
	 * @param endDate
	 * @return userActivity
	 */
	public Date getMostRecentDate(Long userId, Date startDate, Date endDate) {
		DBCursor tweets = getUserTweetsInPeriod(userId, startDate, endDate);
		tweets.sort(new BasicDBObject("creation_date",-1)).limit(1);
		
		if (!tweets.hasNext())
			return null;	// non-active user in the selected period
		
		
		DBObject mostRecentTweet = tweets.next();
		Date mostRecentDate = (Date) mostRecentTweet.get("creation_date");
		return mostRecentDate;
	}
	
	/**
	 * Returns the number of tweets that are produced by the user and retweeted by others
	 * @param userId
	 * @param startDate
	 * @param endDate
	 * @return number of tweets
	 */
	public int getNumberOfTweetsRetweetedByOthers(Long userId, Date startDate, Date endDate) {
		DBObject match = createRetweetMatchObjectUserSide("original_author_id", String.valueOf(userId), startDate, endDate);
		DBObject group = createGroupObject("$original_id");
		DBObject project = createProjectObject();
		
		return computeAggregatePipelineResultsCount(Arrays.asList(match, group, project));
	}
	
	/**
	 * Returns the number of users that retweeted at least one time the specified user
	 * @param userId
	 * @param startDate
	 * @param endDate
	 * @return number of users
	 */
	public int getNumberOfUsersRetweeting(Long userId, Date startDate, Date endDate) {
		DBObject match = createRetweetMatchObjectUserSide("original_author_id", String.valueOf(userId), startDate, endDate);
		DBObject group = createGroupObject("$user_id");
		DBObject project = createProjectObject();
		
		return computeAggregatePipelineResultsCount(Arrays.asList(match, group, project));
	}
	
	/**
	 * Returns the number of tweets produced by others and retweeted by the specified user
	 * @param userId
	 * @param startDate
	 * @param endDate
	 * @return number of tweets
	 */
	public int getNumberOfTweetsUserRetweeted(Long userId, Date startDate, Date endDate) {
		DBObject match = createRetweetMatchObjectOtherUsersSide(userId, startDate, endDate, "original_id");
		DBObject group = createGroupObject("$original_id");
		DBObject project = createProjectObject();
		
		return computeAggregatePipelineResultsCount(Arrays.asList(match, group, project));
	}
	
	/**
	 * Returns the number of users that were retweeted at least once by the specified user
	 * @param userId
	 * @param startDate
	 * @param endDate
	 * @return number of users
	 */
	public int getNumberOfUsersRetweeted(Long userId, Date startDate, Date endDate) {
		DBObject match = createRetweetMatchObjectOtherUsersSide(userId, startDate, endDate, "original_author_id");
		DBObject group = createGroupObject("$original_author_id");
		DBObject project = createProjectObject();
		
		return computeAggregatePipelineResultsCount(Arrays.asList(match, group, project));
	}
	
	/**
	 * Returns the number of times the specified user was mentioned by others
	 * @param userId
	 * @param startDate
	 * @param endDate
	 * @return number of mentions
	 */
	public int getNumberOfMentionsByOthers(Long userId, Date startDate, Date endDate) {
		BasicDBObject query = new BasicDBObject("user_mentions", userId);
		return mongoHandler.retrieveObjectByQueryObject(Config.tweetCollection, query).count();
	}
	
	/**
	 * Returns the number of users that mentioned the specified user
	 * @param userId
	 * @param startDate
	 * @param endDate
	 * @return number of users
	 */
	public int getNumberOfUsersMentioning(Long userId, Date startDate, Date endDate) {
		DBObject match = createMentionMatchObject("user_mentions", userId, startDate, endDate);
		DBObject group = createGroupObject("$user_id");
		DBObject project = createProjectObject();
		
		return computeAggregatePipelineResultsCount(Arrays.asList(match, group, project));
	}
	
	/**
	 * Returns the number of times the specified user mentioned other users
	 * @param userId
	 * @param startDate
	 * @param endDate
	 * @return number of mentions
	 */
	public int getNumberOfMentionsByUser(Long userId, Date startDate, Date endDate) {
		DBObject match = createMentionMatchObject("user_id", userId, startDate, endDate);
		DBObject unwind = createUnwindObject();
		DBObject group = createGroupObject("$user_mentions");
		DBObject project = createProjectObject();
		
		return computeAggregatePipelineResultsExtractedCount(Arrays.asList(match, unwind, group, project));
	}
	
	/**
	 * Returns the number of users that were mentioned by the specified user
	 * @param userId
	 * @param startDate
	 * @param endDate
	 * @return number of users
	 */
	public int getNumberOfUsersMentioned(Long userId, Date startDate, Date endDate) {
		DBObject match = createMentionMatchObject("user_id", userId, startDate, endDate);
		DBObject unwind = createUnwindObject();
		DBObject group = createGroupObject("$user_mentions");
		DBObject project = createProjectObject();
		
		return computeAggregatePipelineResultsCount(Arrays.asList(match, unwind, group, project));
	}
	
	private DBObject createRetweetMatchObjectUserSide(String field, String value, Date startDate, Date endDate) {
		DBObject matchQuery = new BasicDBObject(field, value);
		matchQuery.put("creation_date", new BasicDBObject("$gte",startDate).append("$lte", endDate));
		DBObject match = new BasicDBObject("$match", matchQuery);
		
		return match;
	}
	
	private DBObject createRetweetMatchObjectOtherUsersSide(Long userId, Date startDate, Date endDate, String field) {
		DBObject matchQuery = new BasicDBObject();
		matchQuery.put("user_id", userId);
		matchQuery.put(field, new BasicDBObject("$exists",true));
		matchQuery.put("creation_date", new BasicDBObject("$gte",startDate).append("$lte", endDate));
		DBObject match = new BasicDBObject("$match", matchQuery);
		
		return match;
	}
	
	private DBObject createMentionMatchObject(String field, Long value, Date startDate, Date endDate) {
		DBObject matchQuery = new BasicDBObject(field, value);
		matchQuery.put("creation_date", new BasicDBObject("$gte",startDate).append("$lte", endDate));
		DBObject match = new BasicDBObject("$match", matchQuery);
		
		return match;
	}
	
	private DBObject createGroupObject(String groupField) {
		DBObject groupQuery = new BasicDBObject("_id",groupField);
		groupQuery.put("count",new BasicDBObject("$sum",1));
		DBObject group = new BasicDBObject("$group", groupQuery);
		
		return group;
	}
	
	private DBObject createProjectObject() {
		DBObject fields = new BasicDBObject("count", 1);
		DBObject project = new BasicDBObject("$project",fields);
		return project;
	}
	
	private DBObject createUnwindObject() {
		DBObject unwind = new BasicDBObject("$unwind", "$user_mentions");
		return unwind;
	}
	
	private int computeAggregatePipelineResultsCount(List<DBObject> pipeline) {
		Iterable<DBObject> mappedResults = getPipelineResults(pipeline);
		int numTweets = 0;
		for (Iterator<DBObject> iterator = mappedResults.iterator(); iterator.hasNext(); iterator.next())
			numTweets++;
		
		return numTweets;
	}
	
	private int computeAggregatePipelineResultsExtractedCount(List<DBObject> pipeline) {
		Iterable<DBObject> mappedResults = getPipelineResults(pipeline);
		int numTweets = 0;
		for (DBObject object:mappedResults)
			numTweets += (Integer)object.get("count");
		
		return numTweets;
	}
	
	private Iterable<DBObject> getPipelineResults(List<DBObject> pipeline) {
		AggregationOutput output = mongoHandler.aggregateResults(Config.tweetCollection, pipeline);		
		return output.results();
	}

	/**
	 * Updates the influencers index for the specified collection (i.e., score metrics)
	 * @param users
	 * @param collection
	 * @param startDate
	 */
	public void updateInfluencersIndex(List<User> users, String collection, Date startDate) {
		BasicDBObject influencersIndexBin = new BasicDBObject();
		influencersIndexBin.append("time", startDate.getTime());
		
		BasicDBList influencersList = new BasicDBList();
		for (User user:users) {
			BasicDBObject dbUserObject = new BasicDBObject();
			dbUserObject.append("id", user.getId());
			dbUserObject.append("screen_name", user.getScreenName());
			dbUserObject.append("score", user.getScore());
			influencersList.add(dbUserObject);
		}
		influencersIndexBin.append("influencers", influencersList);

		mongoHandler.insert(collection,	influencersIndexBin);	
	}

	/**
	 * Computes the scores for the keywords
	 * @param collection
	 * @param PrRelevant
	 * @param PrNonRelevant
	 * @param wordCount
	 * @return keywords scores
	 */
	public Map<String, Tuple<Double, Double>> getKeywordScores(String collection, double PrRelevant, double PrNonRelevant, double wordCount) {
		DBCursor resultSet = mongoHandler.getCollectionObjects(collection);
		Map<String, Tuple<Double, Double>> scores = new HashMap<String, Tuple<Double, Double>>();

		while (resultSet.hasNext()) {
			DBObject object = resultSet.next();
			double frequency = (Integer) object.get("frequency");
			double positiveDocuments = (Integer) object.get("positiveDocuments");
			double negativeDocuments = (Integer) object.get("negativeDocuments");
			
			Double wordScore = computeWordScore(frequency, positiveDocuments, negativeDocuments, PrRelevant, PrNonRelevant, wordCount);
			scores.put((String) object.get("keyword"),
					new Tuple<Double, Double>(wordScore, frequency));
		}

		return scores;

	}
	
	/**
	 * Computes the score for a specific keyword
	 * @param frequency
	 * @param positiveDocuments
	 * @param negativeDocuments
	 * @param PrRelevant
	 * @param PrNonRelevant
	 * @param wordCount
	 * @return score
	 */
	private Double computeWordScore(double frequency, double positiveDocuments, double negativeDocuments, double PrRelevant, double PrNonRelevant, double wordCount) {
		double PrKeyword = frequency / wordCount;

		double PrRelevantGivenKeyword = positiveDocuments / (positiveDocuments + negativeDocuments);
		double PrNonRelevantGivenKeyword = negativeDocuments / (positiveDocuments + negativeDocuments);

		double p = (PrRelevantGivenKeyword * PrKeyword) / PrRelevant;
		double r = (PrNonRelevantGivenKeyword * PrKeyword) / PrNonRelevant;
		
		if (r == 0)
			return Double.POSITIVE_INFINITY;
		else
			return Math.log((p * (1 - r)) / ((1 - p) * r));
	}

	/**
	 * Returns the collection statistics (positive docs, negative docs, word count, hashtag count)
	 * @return statistics
	 */
	public CollectionStatistics getCollectionStatistics() {
		DBObject object = mongoHandler.getFirstObject(Config.collectionStatistics);

		CollectionStatistics collectionStatistics = new CollectionStatistics();
		collectionStatistics.setNumNegativeDocuments((int) object.get("negative"));
		collectionStatistics.setNumPositiveDocuments((int) object.get("positive"));
		collectionStatistics.setWordCount((int) object.get("wordCount"));
		collectionStatistics.setHashtagCount((int) object.get("hashtagCount"));

		return collectionStatistics;
	}
}
