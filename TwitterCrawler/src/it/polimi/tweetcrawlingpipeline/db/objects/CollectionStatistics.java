package it.polimi.tweetcrawlingpipeline.db.objects;

public class CollectionStatistics {
	private int numPositiveDocuments;
	private int numNegativeDocuments;
	private int wordCount;
	private int hashtagCount;
	
	public int getNumNegativeDocuments() {
		return numNegativeDocuments;
	}
	public void setNumNegativeDocuments(int numNegativeDocuments) {
		this.numNegativeDocuments = numNegativeDocuments;
	}
	public int getWordCount() {
		return wordCount;
	}
	public void setWordCount(int wordCount) {
		this.wordCount = wordCount;
	}
	public int getNumPositiveDocuments() {
		return numPositiveDocuments;
	}
	public void setNumPositiveDocuments(int numPositiveDocuments) {
		this.numPositiveDocuments = numPositiveDocuments;
	}
	public int getHashtagCount() {
		return hashtagCount;
	}
	public void setHashtagCount(int hashtagCount) {
		this.hashtagCount = hashtagCount;
	}
}
