package it.polimi.tweetcrawlingpipeline.db.objects;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class TweetChunk {
	
	private JSONObject json;
	
	/**
	 * Constructor.
	 * @param json
	 */
	public TweetChunk(JSONObject json) {
		this.json = json;
	}
	
	/**
	 * Returns the string value of an attribute
	 * @param field
	 * @return string
	 */
	public String getFieldValue(String field) {
		try {
			return json.getString(field);
		} 
		catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * Returns the boolean value of an attribute
	 * @param field
	 * @return boolean
	 */
	public boolean getBooleanFieldValue(String field) {
		try {
			return json.getBoolean(field);
		} catch (JSONException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * Returns the long value of an attribute
	 * @param field
	 * @return long
	 */
	public Long getLongFieldValue(String field) {
		try {
			return Long.parseLong(json.getString(field));
		} 
		catch (NumberFormatException | JSONException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * Returns the int value of an attribute
	 * @param field
	 * @return int
	 */
	public int getIntFieldValue(String field) {
		try {
			return json.getInt(field);
		} 
		catch (JSONException e) {
			e.printStackTrace();
			return -1;
		}
	}
	
	/**
	 * Returns a sub object
	 * @param child
	 * @return object
	 */
	public JSONObject getOptionalSubObject(String child) {
		return json.optJSONObject(child);
	}
	
	/**
	 * Returns a sub object
	 * @param child
	 * @return object
	 */
	public JSONObject getSubObject(String child) {
		try {
			return json.getJSONObject(child);
		} 
		catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * Returns a sub array
	 * @param child
	 * @return array
	 */
	public JSONArray getSubArray(String child) {
		try {
			return json.getJSONArray(child);
		} 
		catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * Checks whether the object has a specified field
	 * @param field
	 * @return status
	 */
	public boolean has(String field) {
		return json.has(field);
	}
	
	/**
	 * Returns an optional field
	 * @param field
	 * @return object
	 */
	public String optString(String field) {
		return json.optString(field);
	}
	
	/** Checks whether the object is null
	 * @param field
	 * @return status
	 */
	public boolean isNull(String field) {
		return json.isNull(field);
	}
}
