package it.polimi.tweetcrawlingpipeline.db;

import it.polimi.tweetcrawlingpipeline.utils.Config;



import java.net.UnknownHostException;
import java.util.Date;
import java.util.List;

import com.mongodb.AggregationOutput;
import com.mongodb.BasicDBObject;
import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.MongoClientOptions;

public class MongoHandler {

	private static DB db;
	
	/**
	 * Constructor: connect to the database
	 */
	public MongoHandler() {	
		try {
			System.out.println("Connecting to Mongo DB..");
			
			MongoClient mongo = new MongoClient();
			//MongoCredential credential = MongoCredential.createMongoCRCredential(Config.user, Config.database, Config.password.toCharArray());
			//MongoClientOptions options = MongoClientOptions.builder().connectTimeout(30000).socketTimeout(60000).connectionsPerHost(40).autoConnectRetry(true).build();
            //MongoClient mongo = new MongoClient(new ServerAddress(), Arrays.asList(credential), options);

            db = mongo.getDB(Config.database);
			System.out.println("Connected.");
		} 
		catch (UnknownHostException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Initialize the connection with the database
	 * @param collectionNames
	 */
	public void initializeCollections(List<String> collectionNames){
		DBObject options = BasicDBObjectBuilder.start().add("capped", false).add("size", 2000000000l).get();
		
		for (int i = 0; i < collectionNames.size(); i++)
			if (!db.collectionExists(collectionNames.get(i)))
				db.createCollection(collectionNames.get(i), options);
			else
				db.getCollection(collectionNames.get(i));
	}
	
	/**
	 * Retrieve the reference to a database collection
	 * @param collectionName
	 * @return collection
	 */
	public DBCollection getCollection(String collectionName) {
		return db.getCollection(collectionName);
	}
	
	/**
	 * Retrieve the tweet count for a specific collection
	 * @param collectionName
	 * @return count
	 */
	public long getCollectionCount(String collectionName){
		DBCollection items = getCollection(collectionName);
		return items.count();
	}

	/**
	 * Returns the set of objects contained in a collection.
	 * @param collectionName
	 * @return objects
	 */
	public DBCursor getCollectionObjects(String collectionName){
		DBCollection items = getCollection(collectionName);
		DBCursor cursor = items.find();

		return cursor;
	}

	/**
	 * Return the object answering to the specified query
	 * @param collection
	 * @param field
	 * @param value
	 * @return object
	 */
	public DBCursor retrieveObjectByValue(DBCollection collection, String field, String value){
		BasicDBObject query = new BasicDBObject(field, value);
		DBCursor cursor = collection.find(query);
		
		return cursor;
	}
	
	/**
	 * Return the object answering to the specified query
	 * @param collection
	 * @param field
	 * @param value
	 * @return object
	 */
	public DBCursor retrieveObjectByLongValue(DBCollection collection, String field, long value){
		BasicDBObject query = new BasicDBObject(field, value);
		DBCursor cursor = collection.find(query);
		
		return cursor;
	}
	
	/**
	 * Return the object answering to the specified query
	 * @param collectionName
	 * @param queryObject
	 * @return object
	 */
	public DBCursor retrieveObjectByQueryObject(String collectionName, DBObject queryObject) {
		return retrieveObjectByQueryObject(getCollection(collectionName), queryObject);
	}
	
	/**
	 * Return the object answering to the specified query
	 * @param collection
	 * @param queryObject
	 * @return object
	 */
	public DBCursor retrieveObjectByQueryObject(DBCollection collection, DBObject queryObject) {
		DBCursor cursor = collection.find(queryObject);
		
		return cursor;
	}
	
	/**
	 * Check whether the object is in the database
	 * @param collectionName
	 * @param field
	 * @param value
	 * @return status
	 */
	public boolean isObjectInDatabase(String collectionName, String field, String value){
		DBCollection collection = getCollection(collectionName);
		
		BasicDBObject tweetQueryIdObject = new BasicDBObject();
		try {
			tweetQueryIdObject.put(field, value);
			if (collection.find(tweetQueryIdObject).hasNext())
				return true;
			else
				return false;
		
		} 
		catch (NumberFormatException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * Check whether the objects is in the database
	 * @param collectionName
	 * @param object
	 * @return status
	 */
	public boolean isObjectInDatabase(String collectionName, DBObject object) {
		DBCollection collection = getCollection(collectionName);
		return collection.find(object).hasNext();
	}
	
	public boolean isDatabaseEmpty(String collectionName){
		DBCollection collection = getCollection(collectionName);
		return collection.find().hasNext();
	}
	
	/**
	 * Update a specified object
	 * @param collectionName
	 * @param object
	 * @param chunk
	 */
	public void updateObject(String collectionName, DBObject object, DBObject chunk) {
		DBCollection collection = getCollection(collectionName);
		collection.update(chunk, object);
	}
	
	/**
	 * Insert a new object
	 * @param collectionName
	 * @param object
	 */
	public void insert(String collectionName, DBObject object) {
		DBCollection collection = getCollection(collectionName);
		collection.insert(object);
	}
	
	/**
	 * Insert a new object
	 * @param collectionName
	 * @param object
	 */
	public void insert(String collectionName, List<DBObject> objects) {
		DBCollection collection = getCollection(collectionName);
		collection.insert(objects);
	}
	
	/**
	 * Get the first object of a collection
	 * @param collectionName
	 * @return object
	 */
	public DBObject getFirstObject(String collectionName) {
		return getCollection(collectionName).findOne();
	}
	
	/**
	 * Delete the specified object(s)
	 * @param collectionName
	 * @param field
	 * @param value
	 */
	public void delete(String collectionName, String field, String value) {
		getCollection(collectionName).remove(retrieveObjectByValue(getCollection(collectionName), field, value).next());
	}
	
	/**
	 * Returns objects before a specified date
	 * @param collectionName
	 * @param field
	 * @param date
	 * @return objects
	 */
	public DBCursor getObjectsBeforeDate(String collectionName, String field, Date date) {
		return getObjectsOnDate(getCollection(collectionName), field, date, "$lte");
	}
	
	/**
	 * Returns objects after a specified date
	 * @param collectionName
	 * @param field
	 * @param date
	 * @return objects
	 */
	public DBCursor getObjectsAfterDate(String collectionName, String field, Date date) {
		return getObjectsOnDate(getCollection(collectionName), field, date, "$gte");
	}
	
	/**
	 * Returns objects that are compliant with the combination <date, operator>
	 * @param collection
	 * @param field
	 * @param date
	 * @param operator
	 * @return objects
	 */
	public DBCursor getObjectsOnDate(DBCollection collection, String field, Date date, String operator) {
		BasicDBObject query = new BasicDBObject();
		query.put(field, BasicDBObjectBuilder.start(operator, date).get());
		
		return collection.find(query);
	}
	
	/**
	 * Return objects in a specified date interval
	 * @param collectionName
	 * @param field
	 * @param startDate
	 * @param endDate
	 * @return objects
	 */
	public DBCursor getObjectsInInterval(String collectionName, String field, Date startDate, Date endDate) {
		BasicDBObject query = new BasicDBObject();
		query.put(field, BasicDBObjectBuilder.start("$lte", endDate).add("$gte", startDate).get());
		
		return getCollection(collectionName).find(query);
	}
	
	/**
	 * Return the object having the max value on a field
	 * @param collectionName
	 * @param field
	 * @return object
	 */
	public DBObject getMaxValuedObject(String collectionName, String field) {
		DBObject sort = new BasicDBObject(); 
		sort.put(field, -1);
		
		DBCursor cursor = getCollection(collectionName).find().sort(sort).limit(1);
		if (cursor.hasNext())
			return cursor.next();
		else
			return null;
	}
	
	public AggregationOutput aggregateResults(String collection, List<DBObject> pipeline) {
		DBCollection coll = getCollection(collection);
		AggregationOutput output = coll.aggregate(pipeline);
		
		return output;
	}
	
	public int computeCountQuery(long objectValue, String objectKey, String collectionName){
		BasicDBObject query=new BasicDBObject(objectKey,objectValue);

		int count=retrieveObjectByQueryObject(collectionName, query).count();
		return count;
	}
}

