package it.polimi.tweetcrawlingpipeline.dataset;

import it.polimi.tweetcrawlingpipeline.utils.Utils;

import java.util.List;

public class TweetsFilteringParameters {
	private HashtagList hashtagList;
	private KeywordList keywordList;
	private UserList 	userList;
	
	/**
	 * Constructor.
	 */
	public TweetsFilteringParameters() {
		hashtagList = new HashtagList();
		keywordList = new KeywordList();
		userList = new UserList();
	}
	
	/**
	 * Adds a set of users to the seed
	 * @param users
	 */
	public void addUsers(List<Long> users) {
		userList.append(users);
	}
	
	/**
	 * Adds a set of hashtags to the seed
	 * @param hashtags
	 */
	public void addHashtags(List<String> hashtags) {
		hashtagList.append(hashtags);
	}
	
	/**
	 * Adds a set of keywords to the seed
	 * @param keywords
	 */
	public void addKeywords(List<String> keywords) {
		keywordList.append(keywords);
	}
	
	/**
	 * Add a single user to the seed
	 * @param user
	 */
	public void addUser(Long user) {
		userList.add(user);
	}
	
	/**
	 * Add a single hashtag to the seed
	 * @param hashtag
	 */
	public void addHashtag(String hashtag) {
		hashtagList.add(hashtag);
	}
	
	/**
	 * Add a single keyword to the seed
	 * @param keyword
	 */
	public void addKeyword(String keyword) {
		keywordList.add(keyword);
	}
	
	/**
	 * Get all the seed users
	 * @return users
	 */
	public UserList getUsers() {
		return userList;
	}
	
	/**
	 * Returns the set of keywords and hashtags that are used to filter the tweets
	 * @return keywords
	 */
	public String[] getFilteringKeywords() {
		String[] keywords = keywordList.toStringArray();
		String[] hashtags = hashtagList.toStringArray();
		
		String[] filteringKeywords = Utils.concatenate(keywords, hashtags);		
		return filteringKeywords;
	}
	
	public KeywordList getKeywordList() {
		return keywordList;
	}
	
	public HashtagList getHashtagList() {
		return hashtagList;
	}
	
	/**
	 * Return number of keywords
	 * @return numKeywords
	 */
	public int getNumRequiredKeywords() {
		return keywordList.size();
	}
	
	/**
	 * Return number of hashtags
	 * @return hashtag
	 */
	public int getNumRequiredHashtags() {
		return hashtagList.size();
	}
}
