package it.polimi.tweetcrawlingpipeline.dataset;

import java.util.List;

public class UserList extends LongList {
	
	/**
	 * Converts the content to long values
	 * @return longValues
	 */
	public long[] toLongArray() {
		long[] result = new long[list.size()];
		
		for (int i = 0; i < list.size(); i++)
			result[i] = list.get(i);
		
		return result;
	}
	
	public List<Long> toLongList() {
		return list;
	}
	
	/**
	 * Returns the list of users
	 * @return list
	 */
	public List<Long> getList() {
		return list;
	}
}
