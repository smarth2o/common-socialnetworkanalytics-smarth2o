package it.polimi.tweetcrawlingpipeline.dataset;

import java.util.ArrayList;
import java.util.List;

public abstract class GenericList<T> {
	List<T> list;
	
	/**
	 * Constructor.
	 */
	public GenericList() {
		list = new ArrayList<T>();
	}
	
	/**
	 * Adds an element to the list
	 * @param element
	 */
	public void add(T element) {
		list.add(element);
	}
	
	/**
	 * Appends a set of elements to the list
	 * @param elements
	 */
	public void append(List<T> elements) {
		for (int i = 0; i < elements.size(); i++)
			list.add(elements.get(i));
	}
	
	/**
	 * Returns the top-K elements of the list (i.e., the first K objects)
	 * @param K
	 * @return top-K
	 */
	public List<T> getTopKRelevantElements(int K) {
		return list.subList(0, K);
	}
	
	/**
	 * Returns the list size
	 * @return size
	 */
	public int size() {
		return list.size();
	}
	
	/**
	 * Returns a single element from the list
	 * @param index
	 * @return element
	 */
	public T get(int index) {
		return list.get(index);
	}
}
