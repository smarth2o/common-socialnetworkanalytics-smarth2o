package it.polimi.tweetcrawlingpipeline.dataset;

import java.util.List;

public class StringList extends GenericList<String> {
	public String[] toStringArray() {
		String[] result = new String[list.size()];
		
		for (int i = 0; i < list.size(); i++)
			result[i] = list.get(i);
		
		return result;
	}
	
	public List<String> getWords() {
		return list;
	}
}
