package it.polimi.tweetcrawlingpipeline.utils;

import it.polimi.tweetcrawlingpipeline.dataset.StringList;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class TextFileReader {
	
	/**
	 * Reads the lines from a text file
	 * @param fileName
	 * @return lines
	 */
	public static StringList readWordListFromFile(String fileName){
		File textFile = new File(fileName);
		StringList words = new StringList();
		try {
			BufferedReader br = new BufferedReader(new FileReader(textFile));
			String line;
			while ((line = br.readLine()) != null)
				words.add(line);

			br.close();
		} 
		catch (FileNotFoundException e) {
			e.printStackTrace();
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
		
		return words;
	}
	
	/**
	 * Reads number from a text file
	 * @param fileName
	 * @return lines
	 */
	public static List<Long> readNumberListFromFile(String fileName) {
		StringList stringList = readWordListFromFile(fileName);
		
		List<Long> longList = new ArrayList<Long>();
		for (int i = 0; i < stringList.size(); i++)
			longList.add(Long.parseLong(stringList.get(i)));
		
		return longList;
	}
	
}
