package it.polimi.tweetcrawlingpipeline.utils;

import java.io.Serializable;

import weka.core.stemmers.SnowballStemmer;

public class TextProcessor implements Serializable {
	
	private static final long serialVersionUID = -7374621015538799209L;
	private SnowballStemmer stemmer;
	
	public TextProcessor() {
		stemmer = new SnowballStemmer();
		System.out.println("Test stemmer: from lighting to " + stemmer.stem("lighting"));
	}	
	/**
	 * Divides a text in words
	 * @param text
	 * @return words
	 */
	public static String[] divideInWords(String text) {
		return text.replaceAll("[^a-zA-Z ]", "").toLowerCase().split("\\s+");
	}
	
	public String stem(String word){
		return stemmer.stem(word);
	}
}
