package it.polimi.tweetcrawlingpipeline.utils;

public class Config {
	public static final String 	language = "lang";
	public static final String 	english = "en";
	public static final String 	italian = "it";
	
	private static final int	numMinutesBeforeUpdate = 10;
	public static final int 	timeBeforeUpdate = numMinutesBeforeUpdate*60*1000; //in ms
	
	public static final double 	activityConstant = (double)1/10000;
	public static final int 	numberOfInfluencers = 25;
	public static final int 	topK = 1000;
	
	/************************** Database **************************/
	public static final String database = "classify_twitter";
	public static final String user="twitterUser";
	public static final String password="c3wMk1v";
	public static final String rawTweets = "rawTweets";
	public static final String tweetCollection = "Tweet";
	public static final String userCollection ="User";
	public static final String influencersByNumFollowersCollection = "influencersByNumFollowers";
	public static final String influencersByTweetCountCollection ="influencersByTweetCount";
	public static final String influencersByCustomMetricsCollection = "influencersByCustomMetrics";
	
	public static final String collectionStatistics = "collectionStatistics";
	public static final String keywordsCollection = "keywordsCollection";
	public static final String hashtagsCollection = "hashtagsCollection";

	/************************** Seeds file **************************/
	public static final String keywordsFile = "relevantFiles/relevantKeywords.txt";
	public static final String hashtagsFile = "relevantFiles/relevantHashtags.txt";
	public static final String userIdFile = "relevantFiles/relevantUsers.txt";
	
	/************************** Taxonomy **************************/
	public static final String cookingTaxonomy = "filters/cookingKeywords.txt";
	
	/************************** Crawler **************************/
	public static final String fileSystemCrawlerString 	= "fileSystemCrawler";
	public static final String twitterCrawlerString 	= "twitterCrawler";
	public static final String twitterUserCrawlerString = "twitterUserCrawler";
	public static final String twitter4Jcrawler 		= "twitter4Jcrawler";
	public static final String twitterHBCcrawler 		= "twitterHBCcrawler";
	
	public static final String TwitterConsumerKey 	 	= "nBS9QFkRmF4GBT5gM1PQcRtid";//"0AtJn1RHvAIAD7jQfoVnVAVSf";
	public static final String TwitterConsumerSecret 	= "icivONkNVjwpExNuv6BQIuyxm6uDTwrD8n2GxCHzcy0DiI2t7d";//"PFhE4OO2p3CQ7CT6qs2lMxuVYqNetb8u8sH8hAtH8OVAOCd5DK";
	public static final String OAuthAccessToken		 	= "218851571-Ea5thhdwuHI6oCJ89KbYYevtlOuUNuEk7JlVB97c";//"2464836726-sujroB6ZreK47HrYPO4UnT3AQqhHWU5PyEK8AjP";
	public static final String OAuthAccessTokenSecret 	= "eGCVhEW5zTVKzwoQGijd9pVAcjaWIvIc4bteA5VNKsiP7";//"XnLOV9xkfaMSserxEX0HzeIWCknrkoUhw8OQ4z3Mt001j";
	public static final String TwitterStreamURI		 	= "https://stream.twitter.com/1.1/statuses/filter.json";
	
	public static final String fileSystemCrawlingFolder = "";
			
	public static final String tweetText = "text";
	public static final String tweetUserObjectName = "user";
	public static final String tweetUserScreenName = "screen_name";
	
	/************************** Stopwords **************************/
	public static final String stopwordsFolder 		= "stopwords/";
	public static final String englishStopwordsFile = "en.txt";
	
	/************************** Patterns **************************/
	public static final String URLpattern = "(@)?(href=')?(HREF=')?(HREF=\")?(href=\")?(http://)?(https://)?" +
			"[a-zA-Z_0-9\\-]+(\\.\\w[a-zA-Z_0-9\\-]+)+(/[#&\\n\\-=?\\+\\%/\\.\\w]+)?";
	public static final String mentionPattern 	= "^@\\w+:*|\\s@\\w+:*";
	public static final String hashtagPattern 	= "^#\\w+|\\s#\\w+";
	public static final String retweetPattern 	= "^RT ";
}
