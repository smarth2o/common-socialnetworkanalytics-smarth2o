package it.polimi.tweetcrawlingpipeline.utils;

import it.polimi.tweetcrawlingpipeline.dataset.StringList;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;

public class Utils {
	
	/**
	 * Reads the text content from a file
	 * @param file
	 * @return content
	 */
	public static String readFileContent(File file) {
		BufferedReader br = null;
		 
		try {
			String sCurrentLine;
			String text = "";
			br = new BufferedReader(new FileReader(file));
 
			while ((sCurrentLine = br.readLine()) != null)
				text += sCurrentLine;
			
			return text;
 
		} 
		catch (IOException e) {
			e.printStackTrace();
			return null;
		} 
		finally {
			try {
				if (br != null)
					br.close();
			} 
			catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}
	
	/**
	 * Reads the lines of a text file and returns a list of lines
	 * @param file
	 * @return lines
	 */
	public static List<String> readFileLines(File file) {
		BufferedReader br = null;
		List<String> lines = new ArrayList<String>();
		 
		try {
			String sCurrentLine;
			br = new BufferedReader(new FileReader(file));
 
			while ((sCurrentLine = br.readLine()) != null)
				lines.add(sCurrentLine);
			
			return lines;
 
		} 
		catch (IOException e) {
			e.printStackTrace();
			return null;
		} 
		finally {
			try {
				if (br != null)
					br.close();
			} 
			catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}
	
	/**
	 * Concatenates two arrays
	 * @param A
	 * @param B
	 * @return concatenation
	 */
	public static <T> T[] concatenate (T[] A, T[] B) {
		int aLen = A.length;
		int bLen = B.length;

		@SuppressWarnings("unchecked")
		T[] C = (T[]) Array.newInstance(A.getClass().getComponentType(), aLen+bLen);
		System.arraycopy(A, 0, C, 0, aLen);
		System.arraycopy(B, 0, C, aLen, bLen);

		return C;
	}
	
	/**
	 * Put the process to sleep for a while
	 * @param delay
	 */
	public static void sleep(long delay) {
		try {
			Thread.sleep(delay);
		} 
	    catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public static <K, V extends Comparable<? super V>> Map<K, V> sortByValue( Map<K, V> map ){
	    List<Map.Entry<K, V>> list = new LinkedList<>( map.entrySet() );
	    Collections.sort( list, new Comparator<Map.Entry<K, V>>() {
	        @Override
	        public int compare(Map.Entry<K, V> o1, Map.Entry<K, V> o2) {
	            return (o1.getValue()).compareTo( o2.getValue() );
	        }
	    } );
	
	    Map<K, V> result = new LinkedHashMap<>();
	    for (Map.Entry<K, V> entry : list)
	        result.put( entry.getKey(), entry.getValue() );
	    return result;
	}
	
    public static Document convertStringToDocument(String xmlStr) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();  
        DocumentBuilder builder;  
        try {  
            builder = factory.newDocumentBuilder();  
            Document doc = builder.parse(new InputSource(new StringReader(xmlStr))); 
            return doc;
        } 
        catch (Exception e) {  
            e.printStackTrace();  
        } 
        return null;
    }
    
	/**
	 * Populate the stopword list
	 * @return list
	 */
	public static List<String> populateStopwordsList(){
		String fileName = getStopwordsListFile(Config.english);
		StringList stopwords = new StringList();
		
		stopwords = TextFileReader.readWordListFromFile(fileName);
		return stopwords.getWords();
	}
	
	/**
	 * Gets the stopword file according to the language
	 * @param language
	 * @return stopwordsFile
	 */
	public static String getStopwordsListFile(String language) {
		switch(language) {
			case Config.english:
				return Config.stopwordsFolder + Config.englishStopwordsFile;
			default:
				return null;
		}
	}
	
	/**
	 * Remove the URLs from the text
	 * @param text
	 * @return text
	 */
	public static String removeURLs(String text) {
		text = removeSubstring(text, Config.URLpattern);	
		
		return text;
	}
	
	/**
	 * Removes hashtags from a text
	 * @param text
	 * @return
	 */
	public static String removeHashtags(String text) {
		text = removeSubstring(text, Config.hashtagPattern);	
		
		return text;
	}
	
	/**
	 * Remove retweets from a text
	 * @param text
	 * @return
	 */
	public static String removeRetweets(String text) {
		text = removeSubstring(text, Config.retweetPattern);	
		
		return text;
	}
	
	/**
	 * Removes stopwords from a text
	 * @param tweet
	 * @param stopwords
	 * @return
	 */
	public static List<String> removeStopwords(String tweet, List<String>stopwords){
		tweet = tweet.trim();
		String[] keywords = TextProcessor.divideInWords(tweet);
		ArrayList<String> keywordList = new ArrayList<String>();
		
		for(int i = 0; i < keywords.length; i++)
			if (!stopwords.contains(keywords[i]))
				keywordList.add(keywords[i]);

		return keywordList;
	}
	
	/**
	 * Remove the user mentions from the text (in case of tweet)
	 * @param text
	 * @return text
	 */
	public static String removeUserMentions(String text) {
		return removeSubstring(text, Config.mentionPattern);
	}
	
	/**
	 * Remove the substrings that answer to the given pattern
	 * @param text
	 * @param patternString
	 * @return text
	 */
	private static String removeSubstring(String text, String patternString) {
		Pattern pattern = Pattern.compile(patternString,Pattern.CASE_INSENSITIVE);
		Matcher m = pattern.matcher(text);
		
	    if ( m.find() ) {
	        do {
	            String substring = m.group().replace(" ", "");
	            text = text.replace(substring,"");
	        } while(m.find());
	    }
		
		return text;
	}
	
	public static long formatTime(long time) {
		Calendar date = Calendar.getInstance();
		date.setTimeInMillis(time);
		date.set(Calendar.MILLISECOND, 0);
		date.set(Calendar.SECOND,0);
		date.set(Calendar.MINUTE,0);
		date.set(Calendar.HOUR,0);
		time = date.getTimeInMillis();

		return time;
	}
	
	public static Date parseString(String date){
		Date result = null;
		try { 
		DateFormat df = new SimpleDateFormat("EEE MMM dd kk:mm:ss z yyyy", Locale.ENGLISH);
		
		 result =  df.parse(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 return result;

	}
}
