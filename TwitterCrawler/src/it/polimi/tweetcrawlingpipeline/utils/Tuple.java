package it.polimi.tweetcrawlingpipeline.utils;

public class Tuple<X, Y> { 
	
	public final X firstElement; 
	public final Y secondElement; 
  
	public Tuple(X x, Y y) { 
		this.firstElement = x; 
		this.secondElement = y; 
	} 
} 
