package it.polimi.tweetcrawlingpipeline.crawler.filters;

import java.util.List;

import com.google.common.primitives.Longs;

public class Filters {
	private String[] keywords;
	private List<Long> userIds;
	
	public String[] getKeywords() {
		return keywords;
	}
	public void setKeywords(String[] keywords) {
		this.keywords = keywords;
	}
	public long[] getUserIds() {
		return Longs.toArray(userIds);
	}
	public void setUserIds(List<Long> userIds) {
		this.userIds = userIds;
	}
	
	
}
