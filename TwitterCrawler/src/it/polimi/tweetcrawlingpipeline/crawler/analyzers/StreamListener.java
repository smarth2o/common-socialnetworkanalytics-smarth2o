package it.polimi.tweetcrawlingpipeline.crawler.analyzers;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

import it.polimi.tweetcrawlingpipeline.db.TweetRepository;
import twitter4j.StallWarning;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;
import twitter4j.TwitterObjectFactory;

public class StreamListener implements StatusListener {
	
	private TweetRepository tweetRepository;
	private LinkedBlockingQueue<String> tweets;
	
	private int TWEETS_SAVING_TIME = 5000;
	private int THREADS_DELAY = 2000;
	private int NUM_THREADS = 4;
	
	private int MAX_QUEUE_LENGTH = 150;
	
	/**
	 * Constructor.
	 * @param tweetRepository
	 */
	public StreamListener(TweetRepository tweetRepository) {
		this.tweetRepository = tweetRepository;
		tweets = new LinkedBlockingQueue<String>();
		
		final ExecutorService executor = Executors.newFixedThreadPool(NUM_THREADS);
		Runnable tweetAnalyzer = defineMonitoringRunnable(tweetRepository);
	    for (int i = 0; i < NUM_THREADS; i++) {
	    	executor.execute(tweetAnalyzer);
	    	try {
				Thread.sleep(THREADS_DELAY);
			} 
	    	catch (InterruptedException e) {
				e.printStackTrace();
			}
	    }
	}
	
	private Runnable defineMonitoringRunnable(final TweetRepository tweetRepository) {
		return new Runnable() {

            @Override
            public void run() {
                List<String> tempTweets = new ArrayList<String>();

                while (true) {
                	if (tweets.size() > 0) {
	                    tempTweets.clear();
	                    tweets.drainTo(tempTweets);
	
	                    tweetRepository.insert(tempTweets);   
                	}
                    
                    try {
                        Thread.sleep(TWEETS_SAVING_TIME);
                    } 
                    catch (InterruptedException ex) {
                        ex.printStackTrace();
                    }

                }
            }
        };
	}
	
	private void startSavingThread() {
		Thread savingThread = new Thread(defineSavingRunnable(tweetRepository));
		savingThread.start();
	}
	
	private Runnable defineSavingRunnable(final TweetRepository tweetRepository) {
		return new Runnable() {

			@Override
			public void run() {
                List<String> tempTweets = new ArrayList<String>();
                tempTweets.clear();
                tweets.drainTo(tempTweets);

                tweetRepository.insert(tempTweets);   		
			}};
	}
	
	@Override
    public void onTrackLimitationNotice(int numberOfLimitedStatuses) {
        System.out.println("Got track limitation notice:" + numberOfLimitedStatuses);
    }

	@Override
    public void onScrubGeo(long userId, long upToStatusId) {
        System.out.println("Got scrub_geo event userId:" + userId + " upToStatusId:" + upToStatusId);
    }

	@Override
    public void onException(Exception ex) {
        ex.printStackTrace();
    }

	@Override
	public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {
		System.out.println("Got a status deletion notice id:" + statusDeletionNotice.getStatusId());
	}

	@Override
	public void onStallWarning(StallWarning stallWarning) {		
	}

	@Override
	public void onStatus(Status status) {	
		tweets.add(TwitterObjectFactory.getRawJSON(status));
		if (tweets.size() > MAX_QUEUE_LENGTH)
			startSavingThread();
	}
	
}
