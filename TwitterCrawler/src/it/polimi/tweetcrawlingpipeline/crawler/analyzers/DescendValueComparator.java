package it.polimi.tweetcrawlingpipeline.crawler.analyzers;

import it.polimi.tweetcrawlingpipeline.utils.Tuple;

import java.util.Comparator;
import java.util.Map;

public class DescendValueComparator implements Comparator<String> {

    Map<String, Tuple<Double,Double>> base;
    public DescendValueComparator(Map<String, Tuple<Double,Double>> base) {
        this.base = base;
    }

    public int compare(String a, String b) {
    	double scoreA = base.get(a).firstElement;
    	double frequencyA = base.get(a).secondElement;
    	double scoreB = base.get(b).firstElement;
    	double frequencyB = base.get(b).secondElement;
    	
        if (scoreA > scoreB)
            return -1;
        else if (scoreA < scoreB)
            return 1;
        else
        	if (frequencyA > frequencyB)
        		return -1;
        	else
        		return 1;
    }
}