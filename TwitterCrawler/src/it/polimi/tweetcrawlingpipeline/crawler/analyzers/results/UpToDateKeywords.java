package it.polimi.tweetcrawlingpipeline.crawler.analyzers.results;

import java.util.Date;
import java.util.Map;

public class UpToDateKeywords {
	private Map<String,Integer> keywords;
	private Date endDate;
	
	public void setKeywords(Map<String,Integer> keywords) {
		this.keywords = keywords;
	}
	
	public void setDate(Date endDate) {
		this.endDate = endDate;
	}
	
	public Map<String,Integer> getKeywords() {
		return keywords;
	}
	
	public Date getDate() {
		return endDate;
	}
	
}
