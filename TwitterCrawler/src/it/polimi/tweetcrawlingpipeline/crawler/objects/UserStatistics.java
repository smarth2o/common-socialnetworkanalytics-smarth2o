package it.polimi.tweetcrawlingpipeline.crawler.objects;

import java.util.Date;
import java.util.List;

public class UserStatistics {
	private long 	id;
	
	private int		tweetCount;
	
	private int		numTweetsWithMultimediaContent;
	private int		numTweetsWithExternalContent;
	
	private Date	mostRecentTweetDate;
	
	private int			numberOfRetweetsByOtherUsers;
	private List<Long>	retweetingUsers;
	private int			numberOfReweetsByUser;
	private List<Long>	retweetedUsers;
	
	private int			numberOfMentionsByOtherUsers;
	private List<Long>	mentioningUsers;
	private int			numberOfMentionsByUser;
	private List<Long>	mentionedUsers;
	
	public long getId() {
		return id;
	}
	
	public int getTweetCount() {
		return tweetCount;
	}
	
	public int getNumTweetsWithMultimediaContent() {
		return numTweetsWithMultimediaContent;
	}
	
	public int getNumTweetsWithExternalContent() {
		return numTweetsWithExternalContent;
	}
	
	public Date getMostRecentTweetDate() {
		return mostRecentTweetDate;
	}
	
	public int getNumberOfRetweetsByOtherUsers() {
		return numberOfRetweetsByOtherUsers;
	}
	
	public List<Long> getRetweetingUsers() {
		return retweetingUsers;
	}
	
	public int getNumberOfReweetsByUser() {
		return numberOfReweetsByUser;
	}
	
	public List<Long> getRetweetedUsers() {
		return retweetedUsers;
	}
	
	public int getNumberOfMentionsByOtherUsers() {
		return numberOfMentionsByOtherUsers;
	}
	
	public List<Long> getMentioningUsers() {
		return mentioningUsers;
	}
	
	public int getNumberOfMentionsByUser() {
		return numberOfMentionsByUser;
	}
	
	public List<Long> getMentionedUsers() {
		return mentionedUsers;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public void setTweetCount(int tweetCount) {
		this.tweetCount = tweetCount;
	}
	
	public void setNumTweetsWithMultimediaContent(int numTweetsWithMultimediaContent) {
		this.numTweetsWithMultimediaContent = numTweetsWithMultimediaContent;
	}
	
	public void setNumTweetsWithExternalContent(int numTweetsWithExternalContent) {
		this.numTweetsWithExternalContent = numTweetsWithExternalContent;
	}
	
	public void setMostRecentTweetDate(Date mostRecentTweetDate) {
		this.mostRecentTweetDate = mostRecentTweetDate;
	}
	
	public void setNumberOfRetweetsByOtherUsers(int numberOfRetweetsByOtherUsers) {
		this.numberOfRetweetsByOtherUsers = numberOfRetweetsByOtherUsers;
	}
	
	public void setRetweetingUsers(List<Long> retweetingUsers) {
		this.retweetingUsers = retweetingUsers;
	}
	
	public void setNumberOfReweetsByUser(int numberOfReweetsByUser) {
		this.numberOfReweetsByUser = numberOfReweetsByUser;
	}
	
	public void setRetweetedUsers(List<Long> retweetedUsers) {
		this.retweetedUsers = retweetedUsers;
	}
	
	public void setNumberOfMentionsByOtherUsers(int numberOfMentionsByOtherUsers) {
		this.numberOfMentionsByOtherUsers = numberOfMentionsByOtherUsers;
	}

	public void setMentioningUsers(List<Long> mentioningUsers) {
		this.mentioningUsers = mentioningUsers;
	}

	public void setNumberOfMentionsByUser(int numberOfMentionsByUser) {
		this.numberOfMentionsByUser = numberOfMentionsByUser;
	}

	public void setMentionedUsers(List<Long> mentionedUsers) {
		this.mentionedUsers = mentionedUsers;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserStatistics other = (UserStatistics) obj;
		if (id != other.id)
			return false;
		return true;
	}


}
