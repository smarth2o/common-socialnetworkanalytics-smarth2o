package it.polimi.tweetcrawlingpipeline.crawler.crawlers;

import java.util.Calendar;
import java.util.Date;

import twitter4j.FilterQuery;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.conf.ConfigurationBuilder;
import it.polimi.tweetcrawlingpipeline.crawler.analyzers.StreamListener;
import it.polimi.tweetcrawlingpipeline.crawler.filters.Filters;
import it.polimi.tweetcrawlingpipeline.dataset.TweetsFilteringParameters;
import it.polimi.tweetcrawlingpipeline.pipeline.TweetCrawlingPipeline;
import it.polimi.tweetcrawlingpipeline.utils.Config;
import it.polimi.tweetcrawlingpipeline.utils.Utils;

public class Twitter4JCrawler extends TwitterCrawler {
	
	private TwitterStream twitterStream;
	/**
	 * Constructor.
	 * @param seed
	 * @param pipeline
	 */
	public Twitter4JCrawler(TweetsFilteringParameters seed, TweetCrawlingPipeline pipeline, boolean isWithFeedback) {
		super(seed, pipeline, isWithFeedback);
		
	}
	
	@Override
	public void crawl() {
		ConfigurationBuilder configurationBuilder = prepareConfiguration();

        twitterStream = new TwitterStreamFactory(configurationBuilder.build()).getInstance();
        twitterStream.addListener(new StreamListener(pipeline.getTweetRepository()));
        
        boolean isSeed = true;
                
        while (true) {   
        	Filters filters = new Filters();
        	filters.setUserIds(seed.getUsers().toLongList());
            if ((isWithFeedback & isSeed) || !isWithFeedback) {
        		filters.setKeywords(seed.getFilteringKeywords());
        		isSeed = false;
        	}
        	else
        		filters.setKeywords(getKeywordsFromDatabase());
        	
        	twitterStream.cleanUp();
        	
        	FilterQuery filterQuery = new FilterQuery();
            filterQuery.follow(filters.getUserIds());
            filterQuery.track(filters.getKeywords());
            
            twitterStream.filter(filterQuery);
            
        	Calendar now = Calendar.getInstance();
           	Calendar stop = Calendar.getInstance();
        	stop.add(Calendar.MILLISECOND, Config.timeBeforeUpdate);
        	long delay = stop.getTimeInMillis() - now.getTimeInMillis();
        	if (delay > 0) 
        	    Utils.sleep(delay);
        	
        	updateInfluencers(now, delay);
        }
	}
	
	private void updateInfluencers(Calendar endTime, long delay) {
		Date endDate = endTime.getTime();
		Date startDate = computeStartDate(endTime, delay);
		
		updateInfluencersByNumberOfFollowers(startDate, endDate);
		updateInfluencersByTweetCount(startDate, endDate);
		updateCustomInfluenceMetrics(startDate, endDate);
	}
	
	private ConfigurationBuilder prepareConfiguration() {
		ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
        configurationBuilder.setJSONStoreEnabled(true);
        configurationBuilder.setOAuthConsumerKey(Config.TwitterConsumerKey);
        configurationBuilder.setOAuthConsumerSecret(Config.TwitterConsumerSecret);
        configurationBuilder.setOAuthAccessToken(Config.OAuthAccessToken);
        configurationBuilder.setOAuthAccessTokenSecret(Config.OAuthAccessTokenSecret);
        
        return configurationBuilder;
	}
	
	private Date computeStartDate(Calendar endTime, long delay) {
		Calendar startCalendar = Calendar.getInstance();
		startCalendar.setTimeInMillis(endTime.getTimeInMillis() - delay);
		return startCalendar.getTime();
	}
	
}
