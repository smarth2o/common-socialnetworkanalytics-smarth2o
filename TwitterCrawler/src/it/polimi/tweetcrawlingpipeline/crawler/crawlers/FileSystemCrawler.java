package it.polimi.tweetcrawlingpipeline.crawler.crawlers;

import java.io.File;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import it.polimi.tweetcrawlingpipeline.dataset.TweetsFilteringParameters;
import it.polimi.tweetcrawlingpipeline.pipeline.TweetCrawlingPipeline;
import it.polimi.tweetcrawlingpipeline.utils.Config;
import it.polimi.tweetcrawlingpipeline.utils.Utils;

public class FileSystemCrawler extends GenericCrawler {
	
	/**
	 * Constructor.
	 * @param seed
	 * @param pipeline
	 */
	public FileSystemCrawler(TweetsFilteringParameters seed, TweetCrawlingPipeline pipeline, boolean isWithFeedback) {
		super(seed, pipeline, isWithFeedback);
	}
	
	@Override
	public void crawl() {
		File dir = new File(Config.fileSystemCrawlingFolder);
		
		File[] directoryListing = dir.listFiles();
		if (directoryListing != null)
			for (File child : directoryListing) {
				String extension = child.getName().substring(child.getName().lastIndexOf(".") + 1, child.getName().length());
				if (extension.equals("json"))
					readTweetsFromFile(child);
			}
	}
	
	/**
	 * Reads tweets from the specified file
	 * @param tweetsFile
	 */
	private void readTweetsFromFile(File tweetsFile) {
		String fileContent = Utils.readFileContent(tweetsFile);
		
		try {
			JSONArray jsonArray = new JSONArray(fileContent);
			for (int i = 0; i < jsonArray.length(); ++i)
			    processTweet(jsonArray.getJSONObject(i));
		} 
		catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Processes the extracted tweet
	 * @param jsonArray
	 */
	private void processTweet(JSONObject tweet) {
	}

}
