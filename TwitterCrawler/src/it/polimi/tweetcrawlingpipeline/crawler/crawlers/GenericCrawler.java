package it.polimi.tweetcrawlingpipeline.crawler.crawlers;

import it.polimi.tweetcrawlingpipeline.dataset.TweetsFilteringParameters;
import it.polimi.tweetcrawlingpipeline.filters.StopwordsFilter;
import it.polimi.tweetcrawlingpipeline.filters.TopKeywordsFilter;
import it.polimi.tweetcrawlingpipeline.pipeline.TweetCrawlingPipeline;

import it.polimi.tweetcrawlingpipeline.utils.Config;

public abstract class GenericCrawler {
	
	protected TweetCrawlingPipeline 	pipeline;
	protected TweetsFilteringParameters seed;
	protected boolean 					isWithFeedback;
	
	protected TopKeywordsFilter 		keywordsFilter;
	protected StopwordsFilter			stopwordsFilter;
	
	public GenericCrawler(TweetsFilteringParameters seed, TweetCrawlingPipeline pipeline, boolean isWithFeedback) {
		this.pipeline = pipeline;
		this.seed = seed;
		this.isWithFeedback = isWithFeedback;
		
		this.keywordsFilter = new TopKeywordsFilter();
		this.stopwordsFilter = new StopwordsFilter();
	}
	
	/**
	 * Factory for crawlers
	 * @param crawlerType
	 * @param seed
	 * @param pipeline
	 * @return crawler
	 */
	public static GenericCrawler getFactory(String crawlerType, TweetsFilteringParameters seed, TweetCrawlingPipeline pipeline, boolean isWithFeedback) {		
		switch (crawlerType) {
			case Config.fileSystemCrawlerString:
				return new FileSystemCrawler(seed, pipeline, isWithFeedback);
			case Config.twitterHBCcrawler:
				return new HBCTwitterCrawler(seed, pipeline, isWithFeedback);
			case Config.twitter4Jcrawler:
				return new Twitter4JCrawler(seed, pipeline, isWithFeedback);
			default:
				return null;
		}
	}
	
	/**
	 * Crawls the tweets
	 */
	public abstract void crawl();

}
