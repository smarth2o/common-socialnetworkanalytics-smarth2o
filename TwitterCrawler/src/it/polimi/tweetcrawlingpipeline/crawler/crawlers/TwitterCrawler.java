package it.polimi.tweetcrawlingpipeline.crawler.crawlers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import it.polimi.tweetcrawlingpipeline.crawler.analyzers.DescendValueComparator;
import it.polimi.tweetcrawlingpipeline.crawler.influencers.CustomInfluenceMetricsComputer;
import it.polimi.tweetcrawlingpipeline.crawler.influencers.NumFollowersInfluenceMetricsComputer;
import it.polimi.tweetcrawlingpipeline.crawler.influencers.RelevanceComputer;
import it.polimi.tweetcrawlingpipeline.crawler.influencers.TweetCountMetricsComputer;
import it.polimi.tweetcrawlingpipeline.dataset.TweetsFilteringParameters;
import it.polimi.tweetcrawlingpipeline.db.objects.CollectionStatistics;
import it.polimi.tweetcrawlingpipeline.pipeline.TweetCrawlingPipeline;
import it.polimi.tweetcrawlingpipeline.utils.Config;
import it.polimi.tweetcrawlingpipeline.utils.Tuple;
import it.polimi.tweetcrawlingpipeline.utils.Utils;

public abstract class TwitterCrawler extends GenericCrawler {

	/**
	 * Constructor.
	 * 
	 * @param seed
	 * @param pipeline
	 */
	public TwitterCrawler(TweetsFilteringParameters seed,
			TweetCrawlingPipeline pipeline, boolean isWithFeedback) {
		super(seed, pipeline, isWithFeedback);
	}

	@Override
	public abstract void crawl();

	protected void updateInfluencersByNumberOfFollowers(Date startDate, Date endDate) {
		RelevanceComputer influenceMetricsComputer = new NumFollowersInfluenceMetricsComputer();
		influenceMetricsComputer.computeRelevance(pipeline.getTweetRepository(), startDate, endDate);
	}

	protected void updateCustomInfluenceMetrics(Date startDate, Date endDate) {
		RelevanceComputer influenceMetricsComputer = new CustomInfluenceMetricsComputer();
		influenceMetricsComputer.computeRelevance(pipeline.getTweetRepository(), startDate, endDate);
	}

	protected void updateInfluencersByTweetCount(Date startDate, Date endDate) {
		RelevanceComputer influenceMetricsComputer = new TweetCountMetricsComputer();
		influenceMetricsComputer.computeRelevance(pipeline.getTweetRepository(), startDate, endDate);
	}

	protected String[] getKeywordsFromDatabase() {
		CollectionStatistics collectionStatistics = pipeline.getTweetRepository().getCollectionStatistics();

		List<String> topKKeywords = getTopKWords(
				collectionStatistics.getNumPositiveDocuments(),
				collectionStatistics.getNumNegativeDocuments(),
				collectionStatistics.getWordCount(), Config.keywordsCollection,
				seed.getNumRequiredKeywords());
		List<String> topKHashtags = getTopKWords(
				collectionStatistics.getNumPositiveDocuments(),
				collectionStatistics.getNumNegativeDocuments(),
				collectionStatistics.getHashtagCount(),
				Config.hashtagsCollection, seed.getNumRequiredHashtags());

		String[] keywords = new String[topKKeywords.size()];
		topKKeywords.toArray(keywords);
		String[] hashtags = new String[topKHashtags.size()];
		topKHashtags.toArray(hashtags);
		return Utils.concatenate(keywords, hashtags);
	}

	private List<String> getTopKWords(double numPositiveDocuments,
			double numNegativeDocuments, double wordCount,
			String wordsCollection, int K) {
		double PrRelevant = numPositiveDocuments
				/ (numPositiveDocuments + numNegativeDocuments);
		double PrNonRelevant = numNegativeDocuments
				/ (numPositiveDocuments + numNegativeDocuments);

		Map<String, Tuple<Double, Double>> scores = pipeline
				.getTweetRepository().getKeywordScores(wordsCollection,
						PrRelevant, PrNonRelevant, wordCount);
		Map<String, Tuple<Double, Double>> orderedScores = orderByFrequency(scores);

		List<String> topKWords = new ArrayList<String>();

		int k = 0;
		for (Map.Entry<String, Tuple<Double, Double>> entry : orderedScores
				.entrySet()) {
			if (!entry.getKey().startsWith("#")
					&& !keywordsFilter.isRelevant(entry.getKey()))
				continue;

			topKWords.add(entry.getKey());
			k++;

			if (k == K)
				break;
		}

		return topKWords;
	}

	private Map<String, Tuple<Double, Double>> orderByFrequency(
			Map<String, Tuple<Double, Double>> elements) {
		DescendValueComparator comparator = new DescendValueComparator(elements);
		TreeMap<String, Tuple<Double, Double>> sortedMap = new TreeMap<String, Tuple<Double, Double>>(
				comparator);
		sortedMap.putAll(elements);

		return sortedMap;
	}
}
