package it.polimi.tweetcrawlingpipeline.crawler.crawlers;

import it.polimi.tweetcrawlingpipeline.dataset.TweetsFilteringParameters;
import it.polimi.tweetcrawlingpipeline.pipeline.TweetCrawlingPipeline;
import it.polimi.tweetcrawlingpipeline.utils.Config;

import java.util.Arrays;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

import com.twitter.hbc.ClientBuilder;
import com.twitter.hbc.core.Client;
import com.twitter.hbc.core.Constants;
import com.twitter.hbc.core.endpoint.StatusesFilterEndpoint;
import com.twitter.hbc.core.processor.StringDelimitedProcessor;
import com.twitter.hbc.httpclient.auth.Authentication;
import com.twitter.hbc.httpclient.auth.OAuth1;

public class HBCTwitterCrawler extends TwitterCrawler {
	private static final int NUM_THREADS = 4;
	public static int num = 0;
	
	/**
	 * Constructor.
	 * @param seed
	 * @param pipeline
	 */
	public HBCTwitterCrawler(TweetsFilteringParameters seed, TweetCrawlingPipeline pipeline, boolean isWithFeedback) {
		super(seed, pipeline, isWithFeedback);
	}

	@Override
	public void crawl() {
		System.out.println("Crawling...");
		
	    final BlockingQueue<String> queue = new LinkedBlockingQueue<String>(10000);
	    
	    StatusesFilterEndpoint filterQuery = new StatusesFilterEndpoint();
	    filterQuery.followings(seed.getUsers().getList());
	    filterQuery.trackTerms(Arrays.asList(seed.getFilteringKeywords()));

	    initializeConnection(queue, filterQuery);
	    startThreadPoolCrawling(queue);
	}
	
	/**
	 * Initializes the connection with Twitter API (using the provided access keys)
	 * @param queue
	 * @param filterQuery
	 */
	private void initializeConnection(final BlockingQueue<String> queue, StatusesFilterEndpoint filterQuery) {
		System.out.println("Connecting to Twitter...");
		
		Authentication auth = new OAuth1(Config.TwitterConsumerKey, 
				 Config.TwitterConsumerSecret, 
				 Config.OAuthAccessToken, 
				 Config.OAuthAccessTokenSecret);
		
		System.out.println("Twitter stream API version: " + Constants.CURRENT_API_VERSION);

		Client client = new ClientBuilder()
								.hosts(Constants.STREAM_HOST)
								.endpoint(filterQuery)
								.authentication(auth)
								.processor(new StringDelimitedProcessor(queue))
								.build();
		client.connect();
		
		System.out.println("Connected.");
	}
	
	/**
	 * Starts the crawling in multithreading
	 * @param queue
	 */
	private void startThreadPoolCrawling(final BlockingQueue<String> queue) {
		final ExecutorService executor = Executors.newFixedThreadPool(NUM_THREADS);
	    
		Runnable tweetAnalyzer = defineRunnable(queue);
	    for (int i = 0; i < NUM_THREADS; i++)
	    	executor.execute(tweetAnalyzer);
	}
	
	/**
	 * Defines the tweets analysis process (handled by a single thread)
	 * @param queue
	 * @return runnableAnalysisProcedure
	 */
	private Runnable defineRunnable(final BlockingQueue<String> queue) {
		return new Runnable() {

			@Override
			public void run() {
				while (true)
					try {
						String spilato = queue.take();
						System.out.println(spilato);
						System.out.println(num);
						num++;
					} 
					catch (InterruptedException e) {
						e.printStackTrace();
					}
			}
	    	
	    };
	}
}
