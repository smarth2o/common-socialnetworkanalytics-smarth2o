package it.polimi.tweetcrawlingpipeline.crawler.influencers.rankaggregators;

public class Tuple<T> {
	private T value;
	private double median;
	
	public Tuple(T value, double median) {
		this.value = value;
		this.median = median;
	}
	
	public double getMedian() {
		return median;
	}
	
	public T getValue() {
		return value;
	}
}
