package it.polimi.tweetcrawlingpipeline.crawler.influencers.rankaggregators;

import java.util.List;

public interface RankAggregator<T> {
	public List<T> aggregateRanks(List<List<T>> ranks);
}
