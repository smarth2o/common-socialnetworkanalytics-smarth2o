package it.polimi.tweetcrawlingpipeline.crawler.influencers.rankaggregators;

import it.polimi.tweetcrawlingpipeline.utils.Config;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MedianRankAggregator<T> implements RankAggregator<T> {

	@Override
	public List<T> aggregateRanks(List<List<T>> ranks) {
		List<T> allObjects = getAllObjects(ranks);
		List<Tuple<T>> tuples = new ArrayList<Tuple<T>>();
		
		for (T object:allObjects) {
			List<Integer> positions = new ArrayList<Integer>();
			
			for (List<T> rank:ranks) {
				int position = rank.indexOf(object);
				if (position == -1)
					position = rank.size() + 1;
				positions.add(position);
			}
			tuples.add(new Tuple<T>(object, computeMedian(positions)));
		}
		
		List<T> orderedObjects = new ArrayList<T>();
		Collections.sort(tuples, new TupleComparator<T>());
		
		for (Tuple<T> tuple:tuples) 
			orderedObjects.add(tuple.getValue());
		
		return orderedObjects.subList(0, Math.min(orderedObjects.size(), Config.numberOfInfluencers));
	}
	
	/**
	 * Returns the full set of objects contained in the ranks set
	 * @param ranks
	 * @return objects
	 */
	private List<T> getAllObjects(List<List<T>> ranks) {
		List<T> allObjects = new ArrayList<T>();
		for (List<T> rank:ranks)
			for (int j = 0; j < rank.size(); j++)
				if (!allObjects.contains(rank.get(j)))
					allObjects.add(rank.get(j));
		
		return allObjects;
	}
	
	/**
	 * Computes the median of a set of positions
	 * @param positions
	 * @return median
	 */
	private double computeMedian(List<Integer> positions) {
		Collections.sort(positions);
		
		double median;
		if (positions.size() % 2 == 0)
		    median = ((double)positions.get(positions.size()/2) + (double)positions.get(positions.size()/2 - 1))/2;
		else
		    median = (double) positions.get(positions.size()/2);
		
		return median;
	}

}
