package it.polimi.tweetcrawlingpipeline.crawler.influencers.rankaggregators;

import java.util.Comparator;

public class TupleComparator<T> implements Comparator<Tuple<T>> {

	@Override
	public int compare(Tuple<T> o1, Tuple<T> o2) {
		if (o1.getMedian() > o2.getMedian())
			return 1;
		else if (o1.getMedian() < o2.getMedian())
			return -1;
		else
			return 0;
	}

}
