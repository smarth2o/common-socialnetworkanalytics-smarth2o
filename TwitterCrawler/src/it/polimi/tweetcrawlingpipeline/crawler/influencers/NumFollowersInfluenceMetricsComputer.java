package it.polimi.tweetcrawlingpipeline.crawler.influencers;

import java.util.Date;
import java.util.List;

import it.polimi.tweetcrawlingpipeline.crawler.objects.User;
import it.polimi.tweetcrawlingpipeline.db.TweetRepository;
import it.polimi.tweetcrawlingpipeline.utils.Config;

public class NumFollowersInfluenceMetricsComputer extends InfluenceMetricsComputer{

	@Override
	public void computeRelevance(TweetRepository tweetRepository, Date startDate, Date endDate) {
		List<User> orderedUsers = tweetRepository.getOrderedUsersByNumFollowers(startDate, endDate);
		tweetRepository.updateInfluencersIndex(orderedUsers, Config.influencersByNumFollowersCollection, endDate);
	}

}
