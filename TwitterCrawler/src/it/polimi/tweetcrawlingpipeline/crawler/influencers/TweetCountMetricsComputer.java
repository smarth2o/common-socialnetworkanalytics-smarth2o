package it.polimi.tweetcrawlingpipeline.crawler.influencers;

import it.polimi.tweetcrawlingpipeline.crawler.objects.User;
import it.polimi.tweetcrawlingpipeline.db.TweetRepository;
import it.polimi.tweetcrawlingpipeline.utils.Config;

import java.util.Date;
import java.util.List;

public class TweetCountMetricsComputer extends InfluenceMetricsComputer {

	@Override
	public void computeRelevance(TweetRepository tweetRepository, Date startDate, Date endDate) {
		List<User> orderedUsers = tweetRepository.getOrderedUsersByTweetCount(startDate, endDate);
		orderedUsers = orderedUsers.subList(0, Math.min(orderedUsers.size(), Config.numberOfInfluencers));
		tweetRepository.updateInfluencersIndex(orderedUsers, Config.influencersByTweetCountCollection, endDate);
	}

}
