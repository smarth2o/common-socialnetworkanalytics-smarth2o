package it.polimi.tweetcrawlingpipeline.crawler.influencers;

import it.polimi.tweetcrawlingpipeline.db.TweetRepository;

import java.util.Date;

public abstract class RelevanceComputer {
	public abstract void computeRelevance(TweetRepository tweetRepository, Date startDate, Date endDate);
}
