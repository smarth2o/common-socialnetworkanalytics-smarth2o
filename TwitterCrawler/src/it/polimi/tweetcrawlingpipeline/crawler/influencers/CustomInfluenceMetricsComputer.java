package it.polimi.tweetcrawlingpipeline.crawler.influencers;

import it.polimi.tweetcrawlingpipeline.crawler.influencers.metricscomponents.ActivityMetricsComponent;
import it.polimi.tweetcrawlingpipeline.crawler.influencers.metricscomponents.CreativityMetricsComponent;
import it.polimi.tweetcrawlingpipeline.crawler.influencers.metricscomponents.DegreeCentralityMetricsComponent;
import it.polimi.tweetcrawlingpipeline.crawler.influencers.metricscomponents.ExternalContentEnrichmentMetricsComponent;
import it.polimi.tweetcrawlingpipeline.crawler.influencers.metricscomponents.MentionImpactMetricsComponent;
import it.polimi.tweetcrawlingpipeline.crawler.influencers.metricscomponents.MultimediaEnrichmentMetricsComponent;
import it.polimi.tweetcrawlingpipeline.crawler.influencers.metricscomponents.RankCreator;
import it.polimi.tweetcrawlingpipeline.crawler.influencers.metricscomponents.RetweetImpactMetricsComponent;
import it.polimi.tweetcrawlingpipeline.crawler.influencers.rankaggregators.MedianRankAggregator;
import it.polimi.tweetcrawlingpipeline.crawler.influencers.rankaggregators.RankAggregator;
import it.polimi.tweetcrawlingpipeline.crawler.objects.User;
import it.polimi.tweetcrawlingpipeline.db.TweetRepository;
import it.polimi.tweetcrawlingpipeline.utils.Config;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CustomInfluenceMetricsComputer extends InfluenceMetricsComputer {

	@Override
	public void computeRelevance(TweetRepository tweetRepository, Date startDate, Date endDate) {
		List<List<User>> ranks = new ArrayList<List<User>>();
		RankCreator rankCreator = new RankCreator(tweetRepository);

		ranks.add(rankCreator.rank(new DegreeCentralityMetricsComponent(), startDate, endDate));
		ranks.add(rankCreator.rank(new CreativityMetricsComponent(), startDate, endDate));
		ranks.add(rankCreator.rank(new MultimediaEnrichmentMetricsComponent(), startDate, endDate));
		ranks.add(rankCreator.rank(new ExternalContentEnrichmentMetricsComponent(), startDate, endDate));
		ranks.add(rankCreator.rank(new ActivityMetricsComponent(), startDate, endDate));
		ranks.add(rankCreator.rank(new RetweetImpactMetricsComponent(), startDate, endDate));
		ranks.add(rankCreator.rank(new MentionImpactMetricsComponent(), startDate, endDate));
		
		RankAggregator<User> rankAggregator = new MedianRankAggregator<User>();
		List<User> orderedUsers = rankAggregator.aggregateRanks(ranks);
		saveScores(orderedUsers);
		
		tweetRepository.updateInfluencersIndex(orderedUsers, Config.influencersByCustomMetricsCollection, endDate);
	}
	
	/**
	 * Sets decreasing scores to the users
	 * @param orderedUsers
	 */
	private void saveScores(List<User> orderedUsers) {
		for (int i = 0; i < orderedUsers.size(); i++)
			orderedUsers.get(i).setScore(orderedUsers.size()-i);
	}

}
