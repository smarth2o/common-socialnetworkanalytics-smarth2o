package it.polimi.tweetcrawlingpipeline.crawler.influencers.metricscomponents;

import it.polimi.tweetcrawlingpipeline.db.TweetRepository;

import java.util.Date;

public class MentionImpactMetricsComponent extends ImpactMetricsComponent {

	@Override
	public double computeScore(Long userId, Date startDate, Date endDate, TweetRepository tweetRepository) {
		int numberOfMentionsByOtherUsers = tweetRepository.getNumberOfMentionsByOthers(userId, startDate, endDate);
		int numberOfUsersMentioning = tweetRepository.getNumberOfUsersMentioning(userId, startDate, endDate);
		
		int numberOfMentionsByUser = tweetRepository.getNumberOfMentionsByUser(userId, startDate, endDate);
		int numberOfUsersMentioned = tweetRepository.getNumberOfUsersMentioned(userId, startDate, endDate);
		return computeImpact(numberOfMentionsByOtherUsers, numberOfUsersMentioning, numberOfMentionsByUser, numberOfUsersMentioned);
	}
	
}
