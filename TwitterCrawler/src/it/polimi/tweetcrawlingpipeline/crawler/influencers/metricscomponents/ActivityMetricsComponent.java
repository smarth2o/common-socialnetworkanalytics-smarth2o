package it.polimi.tweetcrawlingpipeline.crawler.influencers.metricscomponents;

import it.polimi.tweetcrawlingpipeline.db.TweetRepository;
import it.polimi.tweetcrawlingpipeline.utils.Config;

import java.util.Date;

public class ActivityMetricsComponent implements MetricsComponent {

	@Override
	public double computeScore(Long userId, Date startDate, Date endDate, TweetRepository tweetRepository) {
		Date mostRecentDate = tweetRepository.getMostRecentDate(userId, startDate, endDate);
		if (mostRecentDate == null)
			return 0;
		
		Date now = new Date(System.currentTimeMillis());
		
		double passedMilliseconds = (double)now.getTime() - (double)mostRecentDate.getTime();
		double exponent = -(double)Config.activityConstant*passedMilliseconds/1000;
		return Math.exp(exponent);
	}

}
