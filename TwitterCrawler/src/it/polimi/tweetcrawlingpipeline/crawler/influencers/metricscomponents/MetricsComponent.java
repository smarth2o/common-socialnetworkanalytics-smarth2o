package it.polimi.tweetcrawlingpipeline.crawler.influencers.metricscomponents;

import java.util.Date;

import it.polimi.tweetcrawlingpipeline.db.TweetRepository;

public interface MetricsComponent {
	public double computeScore(Long userId, Date startDate, Date endDate, TweetRepository tweetRepository);
}
