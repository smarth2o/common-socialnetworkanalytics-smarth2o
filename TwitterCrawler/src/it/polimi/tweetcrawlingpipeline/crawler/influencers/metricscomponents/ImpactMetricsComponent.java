package it.polimi.tweetcrawlingpipeline.crawler.influencers.metricscomponents;

import it.polimi.tweetcrawlingpipeline.db.TweetRepository;

import java.util.Date;

import com.sun.xml.internal.txw2.IllegalSignatureException;

public class ImpactMetricsComponent implements MetricsComponent {

	@Override
	public double computeScore(Long userId, Date startDate, Date endDate, TweetRepository tweetRepository) {
		throw new IllegalSignatureException("Cannot instantiate class ImpactMetricsComponent");
	}
	
	/**
	 * Computes the communication impact of a user
	 * @param numCitationByOthers
	 * @param numCitingUsers
	 * @param numCitationsByUser
	 * @param numCitedUsers
	 * @return impact
	 */
	protected double computeImpact(int numCitationByOthers, int numCitingUsers, int numCitationsByUser, int numCitedUsers){
		double citedImpact = 0;
		double citingImpact = 0;
		if (numCitingUsers != 0)
			citedImpact = numCitationByOthers * Math.log(1 + numCitingUsers);
		if (numCitedUsers != 0)
			citingImpact = numCitationsByUser * Math.log(1 + numCitedUsers);
		
		return citedImpact - citingImpact;
	}

}
