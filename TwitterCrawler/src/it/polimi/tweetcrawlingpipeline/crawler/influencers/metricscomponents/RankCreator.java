package it.polimi.tweetcrawlingpipeline.crawler.influencers.metricscomponents;

import it.polimi.tweetcrawlingpipeline.crawler.objects.User;
import it.polimi.tweetcrawlingpipeline.db.TweetRepository;
import it.polimi.tweetcrawlingpipeline.utils.Config;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class RankCreator {
	private TweetRepository tweetRepository;
	
	/**
	 * Constructor
	 * @param tweetRepository
	 */
	public RankCreator(TweetRepository tweetRepository) {
		this.tweetRepository = tweetRepository;
	}
	
	/**
	 * Returns the rank of users according to the specified ranking criterion
	 * @param criterion
	 * @param startDate
	 * @param endDate
	 * @return rank
	 */
	public List<User> rank(MetricsComponent metricsComponent, Date startDate, Date endDate){
		List<Long> userIds = tweetRepository.getUserIdsInPeriod(startDate, endDate);
		List<User> users = new ArrayList<User>();
		
		for (Long userId:userIds)
			users.add(buildUserObject(userId, metricsComponent.computeScore(userId, startDate, endDate, tweetRepository)));
		
		orderUsersByScore(users);
		if (users.size() > Config.numberOfInfluencers)
			users = users.subList(0, Config.numberOfInfluencers);
		
		return users;
	}
	
	/**
	 * Builds the user object given its parameters
	 * @param userId
	 * @param score
	 * @return user
	 */
	private User buildUserObject(long userId, double score) {
		User user = new User();
		user.setId(userId);
		user.setScreenName(tweetRepository.getScreenName(userId));
		user.setScore(score);
		return user;
	}
	
	/**
	 * Orders users by descending scores
	 * @param users
	 */
	private void orderUsersByScore(List<User> users) {
		Collections.sort(users, new Comparator<User>() {
	        @Override
			public int compare(final User user1, final User user2) {
			    if (user1.getScore() == user2.getScore())
			        return 0;
			    return user1.getScore() > user2.getScore() ? -1 : 1;
			}
		} );
	}
}
