package it.polimi.tweetcrawlingpipeline.crawler.influencers.metricscomponents;

import it.polimi.tweetcrawlingpipeline.db.TweetRepository;

import java.util.Date;

import com.sun.xml.internal.txw2.IllegalSignatureException;

public class EnrichmentMetricsComponent implements MetricsComponent {

	@Override
	public double computeScore(Long userId, Date startDate, Date endDate, TweetRepository tweetRepository) {
		throw new IllegalSignatureException("Cannot instantiate class EnrichmentMetricsComponent");
	}
	
	/**
	 * Computes the enrichment degree for the specified characteristics
	 * @param userId
	 * @param enrichmentType
	 * @param startDate
	 * @param endDate
	 * @return enrichment
	 */
	protected double computeEnrichment(Long userId, String enrichmentType, Date startDate, Date endDate, TweetRepository tweetRepository) {
		int numOriginalTweets = tweetRepository.getNumOriginalTweetsInPeriod(userId, startDate, endDate);
		int originalTweetsWithExtraContent = tweetRepository.getNumTweetsContainingExtraContent(userId, startDate, endDate, enrichmentType);

		if (numOriginalTweets == 0)
			return 0;
		else
			return ((double) originalTweetsWithExtraContent) / numOriginalTweets;
	}

}
