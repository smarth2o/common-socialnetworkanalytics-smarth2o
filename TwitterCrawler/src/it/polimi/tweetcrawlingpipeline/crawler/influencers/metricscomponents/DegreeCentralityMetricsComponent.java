package it.polimi.tweetcrawlingpipeline.crawler.influencers.metricscomponents;

import java.util.Date;

import it.polimi.tweetcrawlingpipeline.db.TweetRepository;

public class DegreeCentralityMetricsComponent implements MetricsComponent {

	@Override
	public double computeScore(Long userId, Date startDate, Date endDate, TweetRepository tweetRepository) {
		int numFollowers = tweetRepository.getNumFollowers(userId);
		int numFriends = tweetRepository.getNumFriends(userId);
		
		return numFollowers + numFriends;
	}

}
