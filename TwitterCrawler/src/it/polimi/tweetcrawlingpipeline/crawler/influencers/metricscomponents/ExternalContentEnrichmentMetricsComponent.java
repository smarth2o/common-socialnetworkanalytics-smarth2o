package it.polimi.tweetcrawlingpipeline.crawler.influencers.metricscomponents;

import it.polimi.tweetcrawlingpipeline.db.TweetRepository;

import java.util.Date;

public class ExternalContentEnrichmentMetricsComponent extends EnrichmentMetricsComponent {
	
	@Override
	public double computeScore(Long userId, Date startDate, Date endDate, TweetRepository tweetRepository) {
		return computeEnrichment(userId, "url", startDate, endDate, tweetRepository);
	}

}
