package it.polimi.tweetcrawlingpipeline.crawler.influencers.metricscomponents;

import java.util.Date;

import it.polimi.tweetcrawlingpipeline.db.TweetRepository;

public class CreativityMetricsComponent implements MetricsComponent {

	@Override
	public double computeScore(Long userId, Date startDate, Date endDate, TweetRepository tweetRepository) {
		int publishedTweets = tweetRepository.getNumTweetsInPeriod(userId, startDate, endDate);
		int numOriginalTweets = tweetRepository.getNumOriginalTweetsInPeriod(userId, startDate, endDate);
		
		return ((double)numOriginalTweets) / publishedTweets;
	}

}
