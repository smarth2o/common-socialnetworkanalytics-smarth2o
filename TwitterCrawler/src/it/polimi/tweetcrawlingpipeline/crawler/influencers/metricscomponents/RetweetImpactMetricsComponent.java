package it.polimi.tweetcrawlingpipeline.crawler.influencers.metricscomponents;

import it.polimi.tweetcrawlingpipeline.db.TweetRepository;

import java.util.Date;

public class RetweetImpactMetricsComponent extends ImpactMetricsComponent {

	@Override
	public double computeScore(Long userId, Date startDate, Date endDate, TweetRepository tweetRepository) {
		int numberOfRetweetsByOtherUsers = tweetRepository.getNumberOfTweetsRetweetedByOthers(userId, startDate, endDate);
		int numberOfUsersRetweeting = tweetRepository.getNumberOfUsersRetweeting(userId, startDate, endDate);
		
		int numberOfReweetsByUser = tweetRepository.getNumberOfTweetsUserRetweeted(userId, startDate, endDate);
		int numberOfUsersRetweeted = tweetRepository.getNumberOfUsersRetweeted(userId, startDate, endDate);
		
		return computeImpact(numberOfRetweetsByOtherUsers, numberOfUsersRetweeting, numberOfReweetsByUser, numberOfUsersRetweeted);
	}

}
