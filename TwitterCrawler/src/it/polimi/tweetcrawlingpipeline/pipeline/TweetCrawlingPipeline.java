package it.polimi.tweetcrawlingpipeline.pipeline;

import it.polimi.tweetcrawlingpipeline.crawler.crawlers.GenericCrawler;
import it.polimi.tweetcrawlingpipeline.dataset.TweetsFilteringParameters;
import it.polimi.tweetcrawlingpipeline.db.TweetRepository;
import it.polimi.tweetcrawlingpipeline.utils.Config;
import it.polimi.tweetcrawlingpipeline.utils.TextFileReader;

public class TweetCrawlingPipeline {
	
	private GenericCrawler 	crawler;
	private TweetRepository tweetRepository;

	/**
	 * Constructor.
	 * @param crawlerType
	 * @param seed
	 */
	public TweetCrawlingPipeline(String crawlerType, TweetsFilteringParameters seed, boolean isWithFeedback) {
		this(Config.english, crawlerType, seed, isWithFeedback);
	}
	
	/**
	 * Constructor.
	 * @param language
	 * @param crawlerType
	 * @param seed
	 */
	public TweetCrawlingPipeline(String language, String crawlerType, TweetsFilteringParameters seed, boolean isWithFeedback){
		 crawler = GenericCrawler.getFactory(crawlerType, seed, this, isWithFeedback);
		 tweetRepository = new TweetRepository();
	}
	 
	/**
	 * Crawl tweets
	 */
	public void startCrawling() {
		crawler.crawl();
	}
	
	public TweetRepository getTweetRepository() {
		return tweetRepository;
	}
	
	
	/**
	 * Main.
	 * @param args
	 */
	public static void main(String[] args) {
		int K = 50;
		
		String language = Config.english;
		String crawlerType = Config.twitter4Jcrawler;
		
		boolean isWithFeedback = true;
		
		TweetsFilteringParameters seed = new TweetsFilteringParameters();
		seed.addUsers(TextFileReader.readNumberListFromFile(Config.userIdFile));
		seed.addHashtags(TextFileReader.readWordListFromFile(Config.hashtagsFile).getTopKRelevantElements(K));
		seed.addKeywords(TextFileReader.readWordListFromFile(Config.keywordsFile).getTopKRelevantElements(K));
		
		TweetCrawlingPipeline crawlingPipeline = new TweetCrawlingPipeline(language, crawlerType, seed, isWithFeedback);
		crawlingPipeline.startCrawling();
	}

}
